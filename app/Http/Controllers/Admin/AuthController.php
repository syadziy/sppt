<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Models\UserModel;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        if ($request->session()->get('is_login')) {
            return redirect('/dashboard');
        }

        $setting = SettingModel::first();
        return view('admin.auth.login', compact(
            'setting'
        ));
    }

    public function logout(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login')->with('error', 'Anda belum melakukan login! Silahkan login terlebih dahulu');
        }

        // REMOVE SESSION
        $request->session()->put('is_login', false);
        $request->session()->forget('userid');
        $request->session()->forget('uuid');
        $request->session()->forget('roleid');
        $request->session()->forget('fullname');
        $request->session()->forget('email');
        $request->session()->forget('photo');
        $request->session()->forget('phone');
        $request->session()->forget('satuanid');

        return redirect('/login')->with('success', 'Logout berhasil! Silahkan login kembali untuk mengakses aplikasi');
    }

    public function validation(Request $request)
    {
        $rules = [
            'email'                 => 'required|email',
            'password'              => 'required',
            'pin'                   => 'required',
            'g-recaptcha-response'  => 'required|captcha'
        ];

        $messages = [
            'password.required'             => 'Password wajib diisi.',
            'email.required'                => 'Email wajib diisi.',
            'email.email'                   => 'Email tidak valid.',
            'pin.required'                  => 'Pin wajib diisi.',
            'g-recaptcha-response.required' => 'Tolong ceklis reCAPTCHA.',
            'g-recaptcha-response.captcha'  => 'Captcha error! try again later or contact site admin.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = UserModel::where([
            'email'     => $request->email
        ])->first();

        if (empty($user)) {
            return redirect()->back()->with('error', 'Email yang anda masukkan tidak ditemukan');
        }

        if ($this->encryptPassword($request->password) != $user->adminpassword) {
            if ($this->encryptPassword($request->password) != $user->password) {
                return redirect()->back()->with('error', 'Password yang anda masukkan salah! Silahkan cek kembali');
            }
        }

        if ($request->pin != $user->pin) {
            return redirect()->back()->with('error', 'PIN yang anda input salah! Silahkan cek kembali');
        }
        
        if ($user->banned == 1) {
            return redirect()->back()->with('error', 'User sudah tidak aktif! Silahkan hubungi admin aplikasi');
        }

        if (View::exists('admin.dashboard')) {
            // SET SESSION
            $request->session()->put('is_login', true);
            $request->session()->put('userid', $user->userid);
            $request->session()->put('uuid', $user->uuid);
            $request->session()->put('roleid', $user->roleid);
            $request->session()->put('fullname', $user->fullname);
            $request->session()->put('email', $user->email);
            $request->session()->put('photo', $user->pathphoto);
            $request->session()->put('satuanid', $user->satuanid);

            $phonecode = substr($user->phone, 0, 2);
            $number = substr($user->phone, 3);
            $phonenumber = $user->phone;

            if ($phonecode != '') {
                if ($phonecode == '08') {
                    $phonenumber = '+628' . $number;
                }
            }

            $request->session()->put('phone', $phonenumber);

            return redirect('/dashboard');
        }

        return view('admin.404');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }
}
