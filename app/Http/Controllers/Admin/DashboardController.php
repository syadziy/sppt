<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Models\GeledahModel;
use App\Models\IzinBesukModel;
use App\Models\SitaModel;
use App\Models\PenahananModel;
use App\Models\TilangModel;
use App\Models\TipiringModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DashboardController extends Controller
{

    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        if (View::exists('admin.dashboardv2')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $geledah = GeledahModel::where('status', 1)->count();
            $sita = SitaModel::where('status', 1)->count();
            $penahanan = PenahananModel::where('status', 1)->count();
            $user = UserModel::where('status', 1)->count();

            $userPN = $this->getUserByRole(2);
            $userKJ = $this->getUserByRole(4);
            $userKP = $this->getUserByRole(3);
            $userRT = $this->getUserByRole(5);

            $allPenahananDewasa = PenahananModel::where('tipepenahanan', 1)->where('status', 1)->get();
            $penahananDewasa = $this->getDataPerYear($allPenahananDewasa);

            $allPenahananAnak = PenahananModel::where('tipepenahanan', 2)->where('status', 1)->get();            
            $penahananAnak = $this->getDataPerYear($allPenahananAnak);

            $allTilang = TilangModel::where('status', 1)->get();
            $tilang = $this->getDataPerYear($allTilang);

            $allTipiring = TipiringModel::where('status', 1)->get();
            $tipiring = $this->getDataPerYear($allTipiring);

            $allGeledah = GeledahModel::where('status', 1)->get();
            $geledahChart = $this->getDataPerYear($allGeledah);

            $allSita = SitaModel::where('status', 1)->get();
            $sitaChart = $this->getDataPerYear($allSita);
            
            $allIzinBesuk = IzinBesukModel::where('status', 1)->get();
            $izinBesuk = $this->getDataPerYear($allIzinBesuk);

            return view('admin.dashboardv2', compact(
                'session',
                'setting',
                'geledah',
                'sita',
                'penahanan',
                'user',
                'userPN',
                'userKJ',
                'userKP',
                'userRT',
                'penahananDewasa',
                'penahananAnak',
                'tilang',
                'tipiring',
                'geledahChart',
                'sitaChart',
                'izinBesuk',
            ));
        }

        return view('admin.404');
    }

    public function getDataPerYear($allData) {
        $januari = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-01-01' && data_get($value, 'registerdate') <= $year . '-01-31';
        });

        $februari = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-02-01' && data_get($value, 'registerdate') <= $year . '-02-31';
        });

        $maret = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-03-01' && data_get($value, 'registerdate') <= $year . '-03-31';
        });

        $april = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-04-01' && data_get($value, 'registerdate') <= $year . '-04-31';
        });

        $mei = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-05-01' && data_get($value, 'registerdate') <= $year . '-05-31';
        });

        $juni = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-06-01' && data_get($value, 'registerdate') <= $year . '-06-31';
        });

        $juli = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-07-01' && data_get($value, 'registerdate') <= $year . '-07-31';
        });

        $agustus = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-08-01' && data_get($value, 'registerdate') <= $year . '-08-31';
        });

        $september = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-09-01' && data_get($value, 'registerdate') <= $year . '-09-31';
        });

        $oktober = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-10-01' && data_get($value, 'registerdate') <= $year . '-10-31';
        });

        $november = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-11-01' && data_get($value, 'registerdate') <= $year . '-11-31';
        });

        $desember = $allData->filter(function ($value, $key) {
            $year = Carbon::now()->format('Y');
            return data_get($value, 'registerdate') >= $year . '-12-01' && data_get($value, 'registerdate') <= $year . '-12-31';
        });

        return array(
            count($januari), 
            count($februari), 
            count($maret),
            count($april),
            count($mei),
            count($juni),
            count($juli),
            count($agustus),
            count($september),
            count($oktober),
            count($november),
            count($desember),
        );
    }

    public function getUserByRole($roleid)
    {
        $allUser = UserModel::where('status', 1)->get();
        switch ($roleid) {
            case 2:
                $userByRole = $allUser->filter(function ($value, $key) {
                    return data_get($value, 'roleid') == 2;
                });
                break;
            case 3:
                $userByRole = $allUser->filter(function ($value, $key) {
                    return data_get($value, 'roleid') == 3;
                });
                break;
            case 4:
                $userByRole = $allUser->filter(function ($value, $key) {
                    return data_get($value, 'roleid') == 4;
                });
                break;
            case 5:
                $userByRole = $allUser->filter(function ($value, $key) {
                    return data_get($value, 'roleid') == 5;
                });
                break;
            
            default:
                break;
        }

        return count($userByRole);
    }
}
