<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SatuanModel;
use App\Models\SettingModel;
use App\Models\DiversiModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class DiversiController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        if (View::exists('admin.diversi.index')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $satuan = SatuanModel::where('status', 1);
            $diversi = DiversiModel::select('trx_diversi.*')
                ->with('pengaju')
                ->with('satuan')
                ->join('ref_satuan', 'trx_diversi.satuanid', '=', 'ref_satuan.satuanid')
                ->where('trx_diversi.status', 1);

            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
                $diversi = $diversi->where('ref_satuan.roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            } else {
                $satuan = $satuan->whereIn('roleid', [3, 4]);
                $diversi = $diversi->whereIn('roleid', [3, 4]);
            }

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                // $diversi = $diversi->where('ref_satuan.satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();
            $diversi = $diversi->paginate('10');

            return view('admin.diversi.index', compact(
                'session',
                'setting',
                'satuan',
                'diversi',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'no_register'                 => 'required',
            'nama_tersangka'              => 'required',
            'berkas'                      => 'required|mimes:doc,docx,pdf',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'no_register.required'              => 'Nomor register wajib diisi.',
            'nama_tersangka.required'           => 'Nama terdakwa / tersangka wajib diisi.',
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file doc, docx atau pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $diversi = new DiversiModel();
        $diversi->uuid = (string) Str::uuid();
        $diversi->registerdate = $request->tanggal_register;
        $diversi->satuanid = $request->satuan_kerja;
        $diversi->nomorregister = $request->no_register;
        $diversi->namatersangka = $request->nama_tersangka;

        $file = $request->file('berkas');
        $path = 'berkas/diversi';
        $filename = 'data_' . $request->no_register . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $diversi->berkas = $filename;
        $diversi->statusdiversi = 'pending';
        $diversi->createdby = $request->session()->get('userid');
        $diversi->save();

        return redirect('/' . $menu . '/diversi')->with('success', 'Diversi berhasil ditambahkan! Silahkan melakukan proses lebih lanjut');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $diversi = DiversiModel::where('uuid', $request->uuid)->first();

        if (empty($diversi)) {
            return redirect()->back()->with('error', 'Data diversi tidak ditemukan! Silahkan cek kembali');
        }

        $diversi->status = 0;
        $diversi->save();

        return redirect('/' . $menu . '/diversi')->with('success', 'Data diversi berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.diversi.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'kejaksaan')) {
                $menu = 'kejaksaan';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            }

            $diversi = DiversiModel::with('pengaju')->with('satuan')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('status', 1);
            
            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            } else {
                $satuan = $satuan->whereIn('roleid', [3, 4]);
            }

            $statusdiversi = $this->statusDiversi();

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.diversi.detail', compact(
                'session',
                'diversi',
                'setting',
                'satuan',
                'statusdiversi',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'no_register'                 => 'required',
            'nama_tersangka'              => 'required',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'no_register.required'              => 'Nomor register wajib diisi.',
            'nama_tersangka.required'           => 'Nama terdakwa / tersangka wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $diversi = DiversiModel::where('uuid', $uuid)->first();
        if ($diversi == null) {
            return redirect()->back()->with('error', 'Data diversi tidak ditemukan! Silahkan cek kembali');
        }

        $diversi->registerdate = $request->tanggal_register;
        $diversi->satuanid = $request->satuan_kerja;
        $diversi->nomorregister = $request->no_register;
        $diversi->namatersangka = $request->nama_tersangka;
        $diversi->modifiedby = $request->session()->get('userid');
        $diversi->modifiedat = Carbon::now();
        $diversi->save();

        return redirect('/' . $menu . '/diversi/detail/' . $uuid)->with('success', 'Diversi berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:doc,docx,pdf',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file doc, docx atau pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file DOC, DOCX, atau PDF.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $diversi = DiversiModel::where('uuid', $uuid)->first();
        if ($diversi == null) {
            return redirect()->back()->with('error', 'Data diversi tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/diversi';
        $filename = 'data_' . $diversi->nomorregister . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $diversi->berkas = $filename;
        $diversi->save();

        return redirect('/' . $menu . '/diversi/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $diversi = DiversiModel::where('uuid', $uuid)->first();
        if ($diversi == null) {
            return redirect()->back()->with('error', 'Data diversi tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/diversi/" . $diversi->berkas;
        $extension = substr($file, -4);

        if ($extension == '.pdf') {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } else {
            $headers = [
                'Content-Type' => 'application/vnd.ms-word',
            ];
        }

        return response()->download($file, $diversi->berkas, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_diversi'            => 'required',
        ];

        $messages = [
            'status_diversi.required'         => 'Status diversi wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $diversi = DiversiModel::where('uuid', $uuid)->first();
        if ($diversi == null) {
            return redirect()->back()->with('error', 'Data diversi tidak ditemukan! Silahkan cek kembali');
        }

        $diversi->statusdiversi = $request->status_diversi;
        $diversi->modifiedby = $request->session()->get('userid');
        $diversi->modifiedat = Carbon::now();
        $diversi->save();

        return redirect('/' . $menu . '/diversi/detail/' . $uuid)->with('success', 'Status diversi berhasil di update! Silahkan melakukan proses lebih lanjut');
    }
}
