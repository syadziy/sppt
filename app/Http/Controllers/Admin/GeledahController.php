<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Models\SatuanModel;
use App\Models\GeledahModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class GeledahController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        if (View::exists('admin.geledah.index')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $geledah = GeledahModel::select('trx_geledah.*')
                ->with('pemohon')
                ->with('satuan')
                ->join('ref_satuan', 'trx_geledah.satuanid', '=', 'ref_satuan.satuanid')
                ->where('trx_geledah.status', 1);

            if ($menu == 'kepolisian') {
                // $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? 3 : $session->get('roleid'));
                $geledah = $geledah->where('ref_satuan.roleid', $session->get('roleid') == 1 ? 3 : $session->get('roleid'));
            } else {
                // $satuan = $satuan->whereIn('roleid', [3, 4]);
                $geledah = $geledah->whereIn('roleid', [3, 4]);
            }

            $agama = $this->religion();
            $gender = $this->gender();
            $geledah_type = $this->jenisGeledah();
            $target_geledah_type = $this->targetGeledah();
            $pihak_type = $this->jenisPihak();
            $pemohon = GeledahModel::with('pemohon')->where('status', 1);
            $func = $this;

            // Cek role Superadmin
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                $geledah = $geledah->where('trx_geledah.createdby', $session->get('userid'));
            }

            $satuan = $satuan->get();
            $geledah = $geledah->paginate('10');

            return view('admin.geledah.index', compact(
                'session',
                'setting',
                'satuan',
                'agama',
                'gender',
                'geledah_type',
                'target_geledah_type',
                'pihak_type',
                'geledah',
                'func',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.geledah.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            }

            $geledah = GeledahModel::with('pemohon')->with('satuan')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $statusgeledah = $this->statusGeledah();
            $geledah_type = $this->jenisGeledah();
            $target_geledah_type = $this->targetGeledah();
            $pihak_type = $this->jenisPihak();
            $gender = $this->gender();
            $agama = $this->religion();

            // Cek role Superadmin
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.geledah.detail', compact(
                'session',
                'setting',
                'geledah',
                'statusgeledah',
                'satuan',
                'geledah_type',
                'target_geledah_type',
                'pihak_type',
                'gender',
                'agama',
                'menu',
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {

        $rules = [
            'register_date'                 => 'required',
            'geledah_type'                  => 'required',
            'satuan_kerja'                  => 'required',
            'permohonan_penyidik_date'      => 'required',
            'perintah_geledah_date'         => 'required',
            'target_geledah'                => 'required',
            // 'laporan_penyidik_date'         => 'required',
            // 'ba_geledah_date'               => 'required',
            'no_permohonan_penyidik'        => 'required',
            'no_perintah_geledah'           => 'required',
            'lokasi'                        => 'required',
            // 'no_laporan_penyidik'           => 'required',
            // 'no_ba_geledah'                 => 'required',
            'jenis_pihak'                   => 'required',
            'nama_pihak'                    => 'required',
            'alamat'                        => 'required',
            'birthplace'                    => 'required',
            'birthdate'                     => 'required',
            'sex_type'                      => 'required',
            'nation'                        => 'required',
            'religion'                      => 'required',
            'occupation'                    => 'required',
            'berkas'                        => 'required|mimes:pdf,doc,docx',
        ];

        $messages = [
            'geledah_type.required'                  => 'Jenis Penggeledahan wajib diisi.',
            'register_date.required'                 => 'Tanggal Register wajib diisi.',
            'satuan_kerja.required'                  => 'Satuan Kerja wajib diisi.',
            'permohonan_penyidik_date.required'      => 'Tanggal Permohonan Penyidik wajib diisi.',
            'perintah_geledah_date.required'         => 'Tanggal Perintah Penggeledahan wajib diisi.',
            'target_geledah.required'                => 'Izin Penggeledahan wajib diisi.',
            // 'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
            // 'ba_geledah_date.required'               => 'Tanggal Berita Acara Penggeledahan wajib diisi.',
            'no_permohonan_penyidik.required'        => 'No Surat Permohonan Penyidik wajib diisi.',
            'no_perintah_geledah.required'           => 'No Surat Perintah Penggeledahan wajib diisi.',
            'lokasi.required'                        => 'Penggeledahan wajib diisi.',
            // 'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
            // 'no_ba_geledah.required'                 => 'No Berita Acara Penggeledahan wajib diisi.',
            'jenis_pihak.required'                   => 'Jenis Pihak wajib diisi.',
            'nama_pihak.required'                    => 'Nama Pihak wajib diisi.',
            'alamat.required'                        => 'Alamat wajib diisi.',
            'birthplace.required'                    => 'Tanggal lahir wajib diisi.',
            'birthdate.required'                     => 'Tempat lahir wajib diisi.',
            'sex_type.required'                      => 'Jenis kelamin wajib diisi.',
            'nation.required'                        => 'Kebangsaan wajib diisi.',
            'religion.required'                      => 'Agama wajib diisi.',
            'occupation.required'                    => 'Pekerjaan wajib diisi.',
            'berkas.required'                        => 'Berkas wajib diisi.',
            'berkas.mimes'                           => 'Berkas harus berformat PDF atau DOCX.',
        ];

        if ($request->geledah_type == 'penetapan_persetujuan_penggeledahan' || $request->geledah_type == 'penetapan_penolakan_izin_penggeledahan') {
            $addrules = [
                'laporan_penyidik_date'         => 'required',
                'ba_geledah_date'               => 'required',
                'no_laporan_penyidik'           => 'required',
                'no_ba_geledah'                 => 'required',
            ];

            $addmessages = [
                'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
                'ba_geledah_date.required'               => 'Tanggal Berita Acara Penggeledahan wajib diisi.',
                'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
                'no_ba_geledah.required'                 => 'No Berita Acara Penggeledahan wajib diisi.',
            ];
            $rules = array_merge($rules, $addrules);
            $messages = array_merge($messages, $addmessages);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $geledah = new GeledahModel();
        $geledah->uuid = (string) Str::uuid();
        $geledah->registerdate = $request->register_date;
        $geledah->geledahtype = $request->geledah_type;
        $geledah->satuanid = $request->satuan_kerja;
        $geledah->permohonanpenyidikdate = $request->permohonan_penyidik_date;
        $geledah->nopermohonanpenyidik = $request->no_permohonan_penyidik;
        $geledah->perintahgeledahdate = $request->perintah_geledah_date;
        $geledah->noperintahgeledah = $request->no_perintah_geledah;
        $geledah->targetgeledah = $request->target_geledah;
        $geledah->lokasi = $request->lokasi;
        $geledah->laporanpenyidikdate = $request->laporan_penyidik_date;
        $geledah->nolaporanpenyidik = $request->no_laporan_penyidik;
        $geledah->bageledahdate = $request->ba_geledah_date;
        $geledah->nobageledah = $request->no_ba_geledah;
        $geledah->pihakgeledah = $request->jenis_pihak;
        $geledah->namapihak = $request->nama_pihak;
        $geledah->alamatpihak = $request->alamat;
        $geledah->pihakbirthplace = $request->birthplace;
        $geledah->pihakbirthdate = $request->birthdate;
        $geledah->sextype = $request->sex_type;
        $geledah->religion = $request->religion;
        $geledah->nation = $request->nation;
        $geledah->occupation = $request->occupation;

        $file = $request->file('berkas');
        $path = 'berkas/geledah';
        $filename = 'data_' . $request->no_perintah_geledah . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $geledah->berkasurl = $filename;
        $geledah->statusgeledah = 'proses';
        $geledah->createdby = $request->session()->get('userid');
        $geledah->save();

        return redirect('/' . $menu . '/geledah')->with('success', 'Data Penggeledahan berhasil ditambahkan. Silahkan melakukan proses selanjutnya.');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $geledah = GeledahModel::where('uuid', $request->uuid)->first();

        if (empty($geledah)) {
            return redirect()->back()->with('error', 'Data geledah tidak ditemukan! Silahkan cek kembali');
        }

        $geledah->status = 0;
        $geledah->save();

        return redirect('/' . $menu . '/geledah')->with('success', 'Data geledah berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'register_date'                 => 'required',
            'geledah_type'                  => 'required',
            'satuan_kerja'                  => 'required',
            'permohonan_penyidik_date'      => 'required',
            'perintah_geledah_date'         => 'required',
            'target_geledah'                => 'required',
            // 'laporan_penyidik_date'         => 'required',
            // 'ba_geledah_date'               => 'required',
            'no_permohonan_penyidik'        => 'required',
            'no_perintah_geledah'           => 'required',
            'lokasi'                        => 'required',
            // 'no_laporan_penyidik'           => 'required',
            // 'no_ba_geledah'                 => 'required',
            'jenis_pihak'                   => 'required',
            'nama_pihak'                    => 'required',
            'alamat'                        => 'required',
            'birthplace'                    => 'required',
            'birthdate'                     => 'required',
            'sex_type'                      => 'required',
            'nation'                        => 'required',
            'religion'                      => 'required',
            'occupation'                    => 'required',
        ];

        $messages = [
            'geledah_type.required'                  => 'Jenis Penggeledahan wajib diisi.',
            'register_date.required'                 => 'Tanggal Register wajib diisi.',
            'satuan_kerja.required'                  => 'Satuan Kerja wajib diisi.',
            'permohonan_penyidik_date.required'      => 'Tanggal Permohonan Penyidik wajib diisi.',
            'perintah_geledah_date.required'         => 'Tanggal Perintah Penggeledahan wajib diisi.',
            'target_geledah.required'                => 'Target Penggeledahan wajib diisi.',
            // 'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
            // 'ba_geledah_date.required'               => 'Tanggal Berita Acara Penggeledahan wajib diisi.',
            'no_permohonan_penyidik.required'        => 'No Surat Permohonan Penyidik wajib diisi.',
            'no_perintah_geledah.required'           => 'No Surat Perintah Penggeledahan wajib diisi.',
            'lokasi.required'                        => 'Lokasi wajib diisi.',
            // 'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
            // 'no_ba_geledah.required'                 => 'No Berita Acara Penggeledahan wajib diisi.',
            'jenis_pihak.required'                   => 'Jenis Pihak wajib diisi.',
            'nama_pihak.required'                    => 'Nama Pihak wajib diisi.',
            'alamat.required'                        => 'Alamat wajib diisi.',
            'birthplace.required'                    => 'Tanggal lahir wajib diisi.',
            'birthdate.required'                     => 'Tempat lahir wajib diisi.',
            'sex_type.required'                      => 'Jenis kelamin wajib diisi.',
            'nation.required'                        => 'Kebangsaan wajib diisi.',
            'religion.required'                      => 'Agama wajib diisi.',
            'occupation.required'                    => 'Pekerjaan wajib diisi.',
            'berkas.required'                        => 'Berkas wajib diisi.',
        ];

        if ($request->geledah_type == 'penetapan_persetujuan_penggeledahan' || $request->geledah_type == 'penetapan_penolakan_izin_penggeledahan') {
            $addrules = [
                'laporan_penyidik_date'         => 'required',
                'ba_geledah_date'               => 'required',
                'no_laporan_penyidik'           => 'required',
                'no_ba_geledah'                 => 'required',
            ];

            $addmessages = [
                'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
                'ba_geledah_date.required'               => 'Tanggal Berita Acara Penggeledahan wajib diisi.',
                'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
                'no_ba_geledah.required'                 => 'No Berita Acara Penggeledahan wajib diisi.',
            ];
            $rules = array_merge($rules, $addrules);
            $messages = array_merge($messages, $addmessages);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $geledah = GeledahModel::where('uuid', $uuid)->first();
        if ($geledah == null) {
            return redirect()->back()->with('error', 'Data geledah tidak ditemukan! Silahkan cek kembali');
        }

        $geledah->registerdate = $request->register_date;
        $geledah->geledahtype = $request->geledah_type;
        $geledah->satuanid = $request->satuan_kerja;
        $geledah->permohonanpenyidikdate = $request->permohonan_penyidik_date;
        $geledah->nopermohonanpenyidik = $request->no_permohonan_penyidik;
        $geledah->perintahgeledahdate = $request->perintah_geledah_date;
        $geledah->noperintahgeledah = $request->no_perintah_geledah;
        $geledah->targetgeledah = $request->target_geledah;
        $geledah->lokasi = $request->lokasi;
        $geledah->laporanpenyidikdate = $request->laporan_penyidik_date;
        $geledah->nolaporanpenyidik = $request->no_laporan_penyidik;
        $geledah->bageledahdate = $request->ba_geledah_date;
        $geledah->nobageledah = $request->no_ba_geledah;
        $geledah->pihakgeledah = $request->jenis_pihak;
        $geledah->namapihak = $request->nama_pihak;
        $geledah->alamatpihak = $request->alamat;
        $geledah->pihakbirthplace = $request->birthplace;
        $geledah->pihakbirthdate = $request->birthdate;
        $geledah->sextype = $request->sex_type;
        $geledah->religion = $request->religion;
        $geledah->nation = $request->nation;
        $geledah->occupation = $request->occupation;
        $geledah->modifiedby = $request->session()->get('userid');
        $geledah->modifiedat = Carbon::now();
        $geledah->save();

        return redirect('/' . $menu . '/geledah/detail/' . $uuid)->with('success', 'Geledah berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:pdf,docx,doc',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file pdf/doc/docx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file PDF/DOCX.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $geledah = GeledahModel::where('uuid', $uuid)->first();
        if ($geledah == null) {
            return redirect()->back()->with('error', 'Data geledah tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/geledah';
        $filename = 'data_' . $geledah->noperintahgeledah . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $geledah->berkasurl = $filename;
        $geledah->save();

        return redirect('/' . $menu . '/geledah/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $geledah = GeledahModel::where('uuid', $uuid)->first();
        if ($geledah == null) {
            return redirect()->back()->with('error', 'Data geledah tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/geledah/" . $geledah->berkasurl;
        $extension = substr($file, -4);

        if ($extension == '.pdf') {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } else {
            $headers = [
                'Content-Type' => 'application/vnd.ms-word',
            ];
        }

        return response()->download($file, $geledah->berkasurl, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_geledah'            => 'required',
        ];

        $messages = [
            'status_geledah.required'         => 'Status geledah wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $geledah = GeledahModel::where('uuid', $uuid)->first();
        if ($geledah == null) {
            return redirect()->back()->with('error', 'Data geledah tidak ditemukan! Silahkan cek kembali');
        }

        $geledah->statusgeledah = $request->status_geledah;
        $geledah->modifiedby = $request->session()->get('userid');
        $geledah->modifiedat = Carbon::now();
        $geledah->save();

        return redirect('/' . $menu . '/geledah/detail/' . $uuid)->with('success', 'Status geledah berhasil di update! Silahkan melakukan proses lebih lanjut');
    }
}
