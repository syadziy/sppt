<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\InboxModel;
use App\Models\SettingModel;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class InboxController extends Controller
{

    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        if (View::exists('admin.inbox.index')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $inbox = InboxModel::with('userSender')->with('userReceiver')->where('receiver', $session->get('userid'))->where('statusreceiver', 1)->paginate(100);
            $sent = InboxModel::with('userSender')->with('userReceiver')->where('sender', $session->get('userid'))->where('statussender', 1)->paginate(100);
            $trash = InboxModel::with('userSender')->with('userReceiver')->where('sender', $session->get('userid'))->orWhere('receiver', $session->get('userid'))->paginate(100);
            $user = UserModel::with('role')->where('status', 1)->get();
            $notread = InboxModel::with('userSender')->where('receiver', $session->get('userid'))->where('isread', 1)->where('statusreceiver', 1)->paginate(10);

            return view('admin.inbox.index', compact(
                'session',
                'setting',
                'inbox',
                'sent',
                'trash',
                'notread',
                'user'
            ));
        }

        return view('admin.404');
    }

    public function send(Request $request)
    {
        $rules = [
            'pengirim'      => 'required',
            'penerima'      => 'required',
            'judul'         => 'required',
            'isi'           => 'required',
        ];

        $messages = [
            'pengirim.required'         => 'Jenis satuan wajib diisi.',
            'penerima.required'         => 'Nama satuan wajib diisi.',
            'judul.required'            => 'Nama petugas wajib diisi.',
            'isi.required'              => 'Email wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $message = new InboxModel();
        $message->uuid = (string) Str::uuid();
        $message->sender = $request->session()->get('userid');
        $message->receiver = $request->penerima;
        $message->judul = $request->judul;
        $message->isi = $request->isi;
        $message->createdby = $request->session()->get('userid');
        $message->save();

        return redirect('/inbox')->with('success', 'Pesan berhasil dikirim ke email ' . $request->email);
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $message = InboxModel::where('uuid', $request->uuid)->first();

        if (empty($message)) {
            return redirect()->back()->with('error', 'Pesan tidak ditemukan! Silahkan cek kembali');
        }

        if ($request->type == 'sent') {
            $message->statussender = 0;
        } else {
            $message->statusreceiver = 0;
        }

        $message->save();

        return redirect('/inbox')->with('success', 'Pesan berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function read($uuid)
    {
        $message = InboxModel::where('uuid', $uuid)->first();

        if (empty($message)) {
            return redirect()->back()->with('error', 'Pesan tidak ditemukan! Silahkan cek kembali');
        }

        $message->isread = 0;
        $message->save();

        return "success";
    }
}
