<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Models\IzinBesukModel;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class IzinBesukController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        if (View::exists('admin.izinbesuk.index')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $gender = $this->gender();
            $religion = $this->religion();

            $izinbesuk = IzinBesukModel::with('pengaju')->where('status', 1);

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                // $izinbesuk = $izinbesuk->where('ref_satuan.satuanid', $session->get('satuanid'));
            }

            $izinbesuk = $izinbesuk->paginate('10');

            return view('admin.izinbesuk.index', compact(
                'session',
                'setting',
                'gender',
                'religion',
                'izinbesuk',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'nama_pemohon'              => 'required',
            'jenis_kelamin'             => 'required',
            'agama'                     => 'required',
            'tempat_lahir'              => 'required',
            'tanggal_lahir'             => 'required',
            'kebangsaan'                => 'required',
            'pekerjaan'                 => 'required',
            'hubungan_keluarga'         => 'required',
            'alamat_pemohon'            => 'required',
            'pengikut_besuk'            => 'required',
            'tanggal_surat_izin_besuk'  => 'required',
            'hakim'                     => 'required',
            'no_perkara'                => 'required',
            'nama_tersangka'            => 'required',
            'tanggal_berlaku'           => 'required',
        ];

        $messages = [
            'nama_pemohon.required'                     => 'Tanggal register wajib diisi.',
            'jenis_kelamin.required'                    => 'Satuan kerja wajib diisi.',
            'agama.required'                            => 'Nomor register wajib diisi.',
            'tempat_lahir.required'                     => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_lahir.required'                    => 'Nama terdakwa / tersangka wajib diisi.',
            'kebangsaan.required'                       => 'Nama terdakwa / tersangka wajib diisi.',
            'pekerjaan.required'                        => 'Nama terdakwa / tersangka wajib diisi.',
            'hubungan_kerja.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'alamat_pemohon.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'pengikut_besuk.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_surat_izin_besuk.required'         => 'Nama terdakwa / tersangka wajib diisi.',
            'hakim.required'                            => 'Nama terdakwa / tersangka wajib diisi.',
            'no_perkara.required'                       => 'Nama terdakwa / tersangka wajib diisi.',
            'nama_tersangka.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_berlaku.required'                  => 'Nama terdakwa / tersangka wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $izinbesuk = new IzinBesukModel();
        $izinbesuk->uuid = (string) Str::uuid();
        $izinbesuk->registerdate = Carbon::now();
        $izinbesuk->nomorperkara = $request->no_perkara;
        $izinbesuk->namatersangka = $request->nama_tersangka;
        $izinbesuk->namapemohon = $request->nama_pemohon;
        $izinbesuk->jeniskelamin = $request->jenis_kelamin;
        $izinbesuk->tanggallahir = $request->tanggal_lahir;
        $izinbesuk->tempatlahir = $request->tempat_lahir;
        $izinbesuk->agama = $request->agama;
        $izinbesuk->kebangsaan = $request->kebangsaan;
        $izinbesuk->pekerjaan = $request->pekerjaan;
        $izinbesuk->alamatpemohon = $request->alamat_pemohon;
        $izinbesuk->hubungankeluarga = $request->hubungan_keluarga;
        $izinbesuk->pengikutbesuk = $request->pengikut_besuk;
        $izinbesuk->tanggalsuratizinbesuk = $request->tanggal_surat_izin_besuk;
        $izinbesuk->hakim = $request->hakim;
        $izinbesuk->tanggalberlaku = $request->tanggal_berlaku;
        $izinbesuk->createdby = $request->session()->get('userid');
        $izinbesuk->save();

        return redirect('/' . $menu . '/izin_besuk')->with('success', 'Izin besuk berhasil ditambahkan! Silahkan melakukan proses lebih lanjut');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $izinbesuk = IzinBesukModel::where('uuid', $request->uuid)->first();

        if (empty($izinbesuk)) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        $izinbesuk->status = 0;
        $izinbesuk->save();

        return redirect('/' . $menu . '/izin_besuk')->with('success', 'Data izin besuk berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.izinbesuk.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $gender = $this->gender();
            $religion = $this->religion();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            } else if (strpos($url, 'lapas')) {
                $menu = 'lapas';
            }

            $izinbesuk = IzinBesukModel::with('pengaju')->where('uuid', $uuid)->first();

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            return view('admin.izinbesuk.detail', compact(
                'session',
                'izinbesuk',
                'setting',
                'gender',
                'religion',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'nama_pemohon'              => 'required',
            'jenis_kelamin'             => 'required',
            'agama'                     => 'required',
            'tempat_lahir'              => 'required',
            'tanggal_lahir'             => 'required',
            'kebangsaan'                => 'required',
            'pekerjaan'                 => 'required',
            'hubungan_keluarga'         => 'required',
            'alamat_pemohon'            => 'required',
            'pengikut_besuk'            => 'required',
            'tanggal_surat_izin_besuk'  => 'required',
            'hakim'                     => 'required',
            'no_perkara'                => 'required',
            'nama_tersangka'            => 'required',
            'tanggal_berlaku'           => 'required',
        ];

        $messages = [
            'nama_pemohon.required'                     => 'Tanggal register wajib diisi.',
            'jenis_kelamin.required'                    => 'Satuan kerja wajib diisi.',
            'agama.required'                            => 'Nomor register wajib diisi.',
            'tempat_lahir.required'                     => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_lahir.required'                    => 'Nama terdakwa / tersangka wajib diisi.',
            'kebangsaan.required'                       => 'Nama terdakwa / tersangka wajib diisi.',
            'pekerjaan.required'                        => 'Nama terdakwa / tersangka wajib diisi.',
            'hubungan_kerja.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'alamat_pemohon.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'pengikut_besuk.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_surat_izin_besuk.required'         => 'Nama terdakwa / tersangka wajib diisi.',
            'hakim.required'                            => 'Nama terdakwa / tersangka wajib diisi.',
            'no_perkara.required'                       => 'Nama terdakwa / tersangka wajib diisi.',
            'nama_tersangka.required'                   => 'Nama terdakwa / tersangka wajib diisi.',
            'tanggal_berlaku.required'                  => 'Nama terdakwa / tersangka wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        $izinbesuk->nomorperkara = $request->no_perkara;
        $izinbesuk->namatersangka = $request->nama_tersangka;
        $izinbesuk->namapemohon = $request->nama_pemohon;
        $izinbesuk->jeniskelamin = $request->jenis_kelamin;
        $izinbesuk->tanggallahir = $request->tanggal_lahir;
        $izinbesuk->tempatlahir = $request->tempat_lahir;
        $izinbesuk->agama = $request->agama;
        $izinbesuk->kebangsaan = $request->kebangsaan;
        $izinbesuk->pekerjaan = $request->pekerjaan;
        $izinbesuk->alamatpemohon = $request->alamat_pemohon;
        $izinbesuk->hubungankeluarga = $request->hubungan_keluarga;
        $izinbesuk->pengikutbesuk = $request->pengikut_besuk;
        $izinbesuk->tanggalsuratizinbesuk = $request->tanggal_surat_izin_besuk;
        $izinbesuk->hakim = $request->hakim;
        $izinbesuk->tanggalberlaku = $request->tanggal_berlaku;
        $izinbesuk->modifiedby = $request->session()->get('userid');
        $izinbesuk->modifiedat = Carbon::now();
        $izinbesuk->save();

        return redirect('/' . $menu . '/izin_besuk/detail/' . $uuid)->with('success', 'Izin besuk berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reuploadPermohonan(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:jpg,jpeg,png,pdf',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file jpg, png atau pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file JPG, PNG, atau PDF.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/permohonan';
        $filename = 'permohonan' . $izinbesuk->izinbesukid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $izinbesuk->berkaspermohonan = $filename;
        $izinbesuk->save();

        return redirect('/' . $menu . '/izin_besuk/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reuploadIzin(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:jpg,jpeg,png,pdf',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file jpg, png atau pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file JPG, PNG, atau PDF.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/izin';
        $filename = 'izin' . $izinbesuk->izinbesukid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $izinbesuk->berkasizin = $filename;
        $izinbesuk->save();

        return redirect('/' . $menu . '/izin_besuk/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function downloadPermohonan($uuid)
    {
        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        if ($izinbesuk->berkaspermohonan == null) {
            return redirect()->back()->with('error', 'Berkas belum di upload! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/permohonan/" . $izinbesuk->berkaspermohonan;
        $extension = substr($file, -4);

        if ($extension == '.pdf') {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } else {
            $headers = [
                'Content-Type' => 'application/image',
            ];
        }

        return response()->download($file, $izinbesuk->berkas, $headers);
    }

    public function downloadIzin($uuid)
    {
        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        if ($izinbesuk->berkasizin == null) {
            return redirect()->back()->with('error', 'Berkas belum di upload! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/izin/" . $izinbesuk->berkasizin;
        $extension = substr($file, -4);

        if ($extension == '.pdf') {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } else {
            $headers = [
                'Content-Type' => 'application/image',
            ];
        }

        return response()->download($file, $izinbesuk->berkas, $headers);
    }

    public function generateSurat($uuid)
    {
        $izinbesuk = IzinBesukModel::where('uuid', $uuid)->first();
        if ($izinbesuk == null) {
            return redirect()->back()->with('error', 'Data izin besuk tidak ditemukan! Silahkan cek kembali');
        }

        $today = Carbon::now();
        $pdf = PDF::loadview('admin.surat_izin', compact(
            'izinbesuk',
            'today'
        ))->setPaper('A4', 'potrait');

        return $pdf->download('surat_izin_mengunjungi_tahanan.pdf');
    }
}
