<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PenahananModel;
use App\Models\SatuanModel;
use App\Models\SettingModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class PenahananAnakController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        if (View::exists('admin.penahanan.anak.index')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $religion = $this->religion();
            $gender = $this->gender();
            $jenistahanan = $this->jenisTempatTahanan();
            $jenispermohonan = $this->jenisPermohonanPenahanan();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'kejaksaan')) {
                $menu = 'kejaksaan';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            } else if (strpos($url, 'lapas')) {
                $menu = 'lapas';
            }

            $satuan = SatuanModel::where('status', 1);
            $penahanan = PenahananModel::select('trx_penahanan.*')
                ->with('pengaju')
                ->with('satuan')
                ->join('ref_satuan', 'trx_penahanan.satuanid', '=', 'ref_satuan.satuanid')
                ->where('tipepenahanan', 2)
                ->where('trx_penahanan.status', 1);

            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
                $penahanan = $penahanan->where('ref_satuan.roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            }

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2 && $session->get('roleid') != 5) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                $penahanan = $penahanan->where('trx_penahanan.createdby', $session->get('userid'));
            }

            $satuan = $satuan->get();
            $penahanan = $penahanan->paginate('10');

            return view('admin.penahanan.anak.index', compact(
                'session',
                'setting',
                'religion',
                'gender',
                'jenistahanan',
                'jenispermohonan',
                'satuan',
                'penahanan',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'jenis_penahanan'             => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_pengajaun'           => 'required',
            'no_surat_pengajuan'          => 'required',
            'waktu_penahanan'             => 'required',
            'jenis_tempat_tahanan'        => 'required',
            'tindak_pidana'               => 'required',
            'no_surat_penahanan'          => 'required',
            'no_surat_perpanjangan'       => 'required',
            'nama_tersangka'              => 'required',
            'tempat_lahir'                => 'required',
            'tempat_tinggal'              => 'required',
            'tanggal_lahir'               => 'required',
            'jenis_kelamin'               => 'required',
            'agama'                       => 'required',
            'kebangsaan'                  => 'required',
            'pekerjaan'                   => 'required',
            'berkas'                      => 'required|mimes:pdf',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'jenis_penahanan.required'          => 'Jenis permohonan penahanan wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_pengajaun.required'        => 'Tanggal surat pengajuan wajib diisi.',
            'no_surat_pengajuan.required'       => 'No surat pengajuan wajib diisi.',
            'waktu_penahanan.required'          => 'Waktu penahanan habis wajib diisi.',
            'jenis_tempat_tahanan.required'     => 'Jenis tempat tahanan wajib diisi.',
            'tindak_pidana.required'            => 'Tindak pidanan yang dilakukan tersangka wajib diisi.',
            'no_surat_penahanan.required'       => 'No surat perintah penahanan wajib diisi.',
            'no_surat_perpanjangan.required'    => 'No surat perpanjangan kejaksaan negeri wajib diisi.',
            'nama_tersangka.required'           => 'Nama tersangka wajib diisi.',
            'tempat_lahir.required'             => 'Tempat lahir wajib diisi.',
            'tempat_tinggal.required'           => 'Tempat tinggal wajib diisi.',
            'tanggal_lahir.required'            => 'Tanggal lahir wajib diisi.',
            'jenis_kelamin.required'            => 'Jenis kelamin wajib diisi.',
            'agama.required'                    => 'Agama wajib diisi.',
            'kebangsaan.required'               => 'Kebangsaan wajib diisi.',
            'pekerjaan.required'                => 'Pekerjaan wajib diisi.',
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $penahanan = new PenahananModel();
        $penahanan->uuid = (string) Str::uuid();
        $penahanan->registerdate = $request->tanggal_register;
        $penahanan->jenispermohonan = $request->jenis_penahanan;
        $penahanan->satuanid = $request->satuan_kerja;
        $penahanan->tanggalpengajuan = $request->tanggal_pengajaun;
        $penahanan->nosuratpengajuan = $request->no_surat_pengajuan;
        $penahanan->waktupenahanan = $request->waktu_penahanan;
        $penahanan->jenistempattahanan = $request->jenis_tempat_tahanan;
        $penahanan->tindakpidana = $request->tindak_pidana;
        $penahanan->nosuratpenahanan = $request->no_surat_penahanan;
        $penahanan->nosuratkejaksaan = $request->no_surat_perpanjangan;
        $penahanan->namatersangka = $request->nama_tersangka;
        $penahanan->tempatlahir = $request->tempat_lahir;
        $penahanan->tempattinggal = $request->tempat_tinggal;
        $penahanan->tanggallahir = $request->tanggal_lahir;
        $penahanan->jeniskelamin = $request->jenis_kelamin;
        $penahanan->agama = $request->agama;
        $penahanan->kebangsaan = $request->kebangsaan;
        $penahanan->pekerjaan = $request->pekerjaan;
        $penahanan->tipepenahanan = 2; // 1 Dewasa, 2 Anak

        $file = $request->file('berkas');
        $path = 'berkas/penahanan/anak';
        $filename = 'data_' . $request->no_surat_pengajuan . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $penahanan->berkas = $filename;
        $penahanan->statuspenahanan = 'proses';
        $penahanan->createdby = $request->session()->get('userid');
        $penahanan->save();

        return redirect('/' . $menu . '/penahanan/anak')->with('success', 'Penahanan anak berhasil ditambahkan! Silahkan melakukan proses lebih lanjut');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $penahanan = PenahananModel::where('uuid', $request->uuid)->first();

        if (empty($penahanan)) {
            return redirect()->back()->with('error', 'Data tersangka tidak ditemukan! Silahkan cek kembali');
        }

        $penahanan->status = 0;
        $penahanan->save();

        return redirect('/' . $menu . '/penahanan/anak')->with('success', 'Data tersangka berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.penahanan.anak.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $religion = $this->religion();
            $gender = $this->gender();
            $jenistahanan = $this->jenisTempatTahanan();
            $jenispermohonan = $this->jenisPermohonanPenahanan();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'kejaksaan')) {
                $menu = 'kejaksaan';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            } else if (strpos($url, 'lapas')) {
                $menu = 'lapas';
            }

            $penahanan = PenahananModel::with('pengaju')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('status', 1);

            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            }

            $statuspenahanan = $this->statusPenahanan();

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2 && $session->get('roleid') != 5) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.penahanan.anak.detail', compact(
                'session',
                'penahanan',
                'setting',
                'religion',
                'gender',
                'jenistahanan',
                'jenispermohonan',
                'satuan',
                'statuspenahanan',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'jenis_penahanan'             => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_pengajaun'           => 'required',
            'no_surat_pengajuan'          => 'required',
            'waktu_penahanan'             => 'required',
            'jenis_tempat_tahanan'        => 'required',
            'tindak_pidana'               => 'required',
            'no_surat_penahanan'          => 'required',
            'no_surat_perpanjangan'       => 'required',
            'nama_tersangka'              => 'required',
            'tempat_lahir'                => 'required',
            'tempat_tinggal'              => 'required',
            'tanggal_lahir'               => 'required',
            'jenis_kelamin'               => 'required',
            'agama'                       => 'required',
            'kebangsaan'                  => 'required',
            'pekerjaan'                   => 'required',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'jenis_penahanan.required'          => 'Jenis permohonan penahanan wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_pengajaun.required'        => 'Tanggal surat pengajuan wajib diisi.',
            'no_surat_pengajuan.required'       => 'No surat pengajuan wajib diisi.',
            'waktu_penahanan.required'          => 'Waktu penahanan habis wajib diisi.',
            'jenis_tempat_tahanan.required'     => 'Jenis tempat tahanan wajib diisi.',
            'tindak_pidana.required'            => 'Tindak pidanan yang dilakukan tersangka wajib diisi.',
            'no_surat_penahanan.required'       => 'No surat perintah penahanan wajib diisi.',
            'no_surat_perpanjangan.required'    => 'No surat perpanjangan kejaksaan negeri wajib diisi.',
            'nama_tersangka.required'           => 'Nama tersangka wajib diisi.',
            'tempat_lahir.required'             => 'Tempat lahir wajib diisi.',
            'tempat_tinggal.required'           => 'Tempat tinggal wajib diisi.',
            'tanggal_lahir.required'            => 'Tanggal lahir wajib diisi.',
            'jenis_kelamin.required'            => 'Jenis kelamin wajib diisi.',
            'agama.required'                    => 'Agama wajib diisi.',
            'kebangsaan.required'               => 'Kebangsaan wajib diisi.',
            'pekerjaan.required'                => 'Pekerjaan wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $penahanan = PenahananModel::where('uuid', $uuid)->first();
        if ($penahanan == null) {
            return redirect()->back()->with('error', 'Data penahanan tidak ditemukan! Silahkan cek kembali');
        }

        $penahanan->registerdate = $request->tanggal_register;
        $penahanan->jenispermohonan = $request->jenis_penahanan;
        $penahanan->satuanid = $request->satuan_kerja;
        $penahanan->tanggalpengajuan = $request->tanggal_pengajaun;
        $penahanan->nosuratpengajuan = $request->no_surat_pengajuan;
        $penahanan->waktupenahanan = $request->waktu_penahanan;
        $penahanan->jenistempattahanan = $request->jenis_tempat_tahanan;
        $penahanan->tindakpidana = $request->tindak_pidana;
        $penahanan->nosuratpenahanan = $request->no_surat_penahanan;
        $penahanan->nosuratkejaksaan = $request->no_surat_perpanjangan;
        $penahanan->namatersangka = $request->nama_tersangka;
        $penahanan->tempatlahir = $request->tempat_lahir;
        $penahanan->tempattinggal = $request->tempat_tinggal;
        $penahanan->tanggallahir = $request->tanggal_lahir;
        $penahanan->jeniskelamin = $request->jenis_kelamin;
        $penahanan->agama = $request->agama;
        $penahanan->kebangsaan = $request->kebangsaan;
        $penahanan->pekerjaan = $request->pekerjaan;
        $penahanan->modifiedby = $request->session()->get('userid');
        $penahanan->modifiedat = Carbon::now();
        $penahanan->save();

        return redirect('/' . $menu . '/penahanan/anak/detail/' . $uuid)->with('success', 'Penahanan anak berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:pdf',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file pdf.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file PDF.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $penahanan = PenahananModel::where('uuid', $uuid)->first();
        if ($penahanan == null) {
            return redirect()->back()->with('error', 'Data penahanan tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/penahanan/anak';
        $filename = 'data_' . $penahanan->nosuratpengajuan . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $penahanan->berkas = $filename;
        $penahanan->save();

        return redirect('/' . $menu . '/penahanan/anak/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $penahanan = PenahananModel::where('uuid', $uuid)->first();
        if ($penahanan == null) {
            return redirect()->back()->with('error', 'Data penahanan tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/penahanan/anak/" . $penahanan->berkas;
        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, $penahanan->berkas, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_penahanan'            => 'required',
        ];

        $messages = [
            'status_penahanan.required'         => 'Status penahanan wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'lapas')) {
            $menu = 'lapas';
        }

        $penahanan = PenahananModel::where('uuid', $uuid)->first();
        if ($penahanan == null) {
            return redirect()->back()->with('error', 'Data penahanan tidak ditemukan! Silahkan cek kembali');
        }

        $penahanan->statuspenahanan = $request->status_penahanan;
        $penahanan->modifiedby = $request->session()->get('userid');
        $penahanan->modifiedat = Carbon::now();
        $penahanan->save();

        return redirect('/' . $menu . '/penahanan/anak/detail/' . $uuid)->with('success', 'Status penahanan anak berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

}
