<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class SettingController extends Controller
{

    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        if (View::exists('admin.setting')) {
            $setting = SettingModel::first();
            $session = $request->session();

            return view('admin.setting', compact(
                'setting',
                'session'
            ));
        }

        return view('admin.404');
    }

    public function uploadFoto(Request $request)
    {
        $rules = [
            'photo'      => 'required',
        ];

        $messages = [
            'photo.required'         => 'Foto wajib diupload.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $setting = SettingModel::first();

        $file = $request->file('photo');
        $path = 'logo';
        $filename = 'logoapp' . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        if ($setting == null) {
            $setting = new SettingModel();
            $setting->uuid = (string) Str::uuid();
            $setting->logo = $filename;
            $setting->save();
        } else {
            $setting->logo = $filename;
            $setting->save();
        }

        return redirect('/setting/website')->with('success', 'Update setting website berhasil! Silahkan melanjutkan pekerjaan anda');
    }

    public function update(Request $request)
    {
        $rules = [
            'nama_website'  => 'required',
            'regional'      => 'required',
        ];

        $messages = [
            'nama_website.required'     => 'Nama website wajib diisi.',
            'regional.required'         => 'Regional wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $setting = SettingModel::first();

        if ($setting == null) {
            $setting = new SettingModel();
            $setting->uuid = (string) Str::uuid();
            $setting->websitename = $request->nama_website;
            $setting->regional = $request->regional;
            $setting->save();
        } else {
            $setting->websitename = $request->nama_website;
            $setting->regional = $request->regional;
            $setting->save();
        }

        return redirect('/setting/website')->with('success', 'Update setting website berhasil! Silahkan melanjutkan pekerjaan anda');
    }

}
