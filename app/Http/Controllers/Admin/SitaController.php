<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingModel;
use App\Models\SatuanModel;
use App\Models\SitaModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;


class SitaController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        }

        if (View::exists('admin.sita.index')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $satuan = SatuanModel::where('status', 1);
            $sita = SitaModel::select('trx_sita.*')
                ->with('pemohon')
                ->with('satuan')
                ->join('ref_satuan', 'trx_sita.satuanid', '=', 'ref_satuan.satuanid')
                ->where('trx_sita.status', 1);

            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
                $sita = $sita->where('ref_satuan.roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            } else {
                $satuan = $satuan->whereIn('roleid', [3, 4]);
                $sita = $sita->whereIn('roleid', [3, 4]);
            }

            $agama = $this->religion();
            $gender = $this->gender();
            $sita_type = $this->jenisPenyitaan();
            $pihak_type = $this->jenisPihak();
            $pemohon = SitaModel::with('pemohon')->where('status', 1);
            $func = $this;

            // Cek role Superadmin
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                $sita = $sita->where('trx_sita.createdby', $session->get('userid'));
            }

            $satuan = $satuan->get();
            $sita = $sita->paginate('10');

            return view('admin.sita.index', compact(
                'session',
                'setting',
                'satuan',
                'agama',
                'gender',
                'sita_type',
                'pihak_type',
                'sita',
                'func',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.sita.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'kejaksaan')) {
                $menu = 'kejaksaan';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            }

            $sita = SitaModel::with('pemohon')->with('satuan')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('status', 1);
            $statussita = $this->statusSita();
            $sita_type = $this->jenisPenyitaan();
            $pihak_type = $this->jenisPihak();
            $gender = $this->gender();
            $agama = $this->religion();

            if ($menu == 'kepolisian' || $menu == 'kejaksaan') {
                $satuan = $satuan->where('roleid', $session->get('roleid') == 1 ? ($menu == 'kepolisian' ? 3 : 4) : $session->get('roleid'));
            } else {
                $satuan = $satuan->whereIn('roleid', [3, 4]);
            }

            // Cek role Superadmin
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.sita.detail', compact(
                'session',
                'setting',
                'sita',
                'statussita',
                'satuan',
                'sita_type',
                'pihak_type',
                'gender',
                'agama',
                'menu',
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {

        $rules = [
            'register_date'                 => 'required',
            'penyitaan_type'                => 'required',
            'satuan_kerja'                  => 'required',
            'permohonan_penyidik_date'      => 'required',
            'perintah_penyitaan_date'       => 'required',
            'izin_sita'                     => 'required',
            // 'laporan_penyidik_date'         => 'required',
            // 'ba_penyitaan_date'             => 'required',
            'no_permohonan_penyidik'        => 'required',
            'no_perintah_penyitaan'         => 'required',
            'penyitaan'                     => 'required',
            // 'no_laporan_penyidik'           => 'required',
            // 'no_ba_penyitaan'               => 'required',
            'jenis_pihak'                   => 'required',
            'nama_pihak'                    => 'required',
            'alamat'                        => 'required',
            'birthplace'                    => 'required',
            'birthdate'                     => 'required',
            'sex_type'                      => 'required',
            'nation'                        => 'required',
            'religion'                      => 'required',
            'occupation'                    => 'required',
            'berkas'                        => 'required|mimes:pdf,doc,docx',
        ];

        $messages = [
            'penyitaan_type.required'                => 'Jenis Penyitaan wajib diisi.',
            'register_date.required'                 => 'Tanggal Register wajib diisi.',
            'satuan_kerja.required'                  => 'Satuan Kerja wajib diisi.',
            'permohonan_penyidik_date.required'      => 'Tanggal Permohonan Penyidik wajib diisi.',
            'perintah_penyitaan_date.required'       => 'Tanggal Perintah Penyitaan wajib diisi.',
            'izin_sita.required'                     => 'Izin Penyitaan wajib diisi.',
            // 'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
            // 'ba_penyitaan_date.required'             => 'Tanggal Berita Acara Penyitaan wajib diisi.',
            'no_permohonan_penyidik.required'        => 'No Surat Permohonan Penyidik wajib diisi.',
            'no_perintah_penyitaan.required'         => 'No Surat Perintah Penyitaan wajib diisi.',
            'penyitaan.required'                     => 'Penyitaan wajib diisi.',
            // 'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
            // 'no_ba_penyitaan.required'               => 'No Berita Acara Penyitaan wajib diisi.',
            'jenis_pihak.required'                   => 'Jenis Pihak wajib diisi.',
            'nama_pihak.required'                    => 'Nama Pihak wajib diisi.',
            'alamat.required'                        => 'Alamat wajib diisi.',
            'birthplace.required'                    => 'Tanggal lahir wajib diisi.',
            'birthdate.required'                     => 'Tempat lahir wajib diisi.',
            'sex_type.required'                      => 'Jenis kelamin wajib diisi.',
            'nation.required'                        => 'Kebangsaan wajib diisi.',
            'religion.required'                      => 'Agama wajib diisi.',
            'occupation.required'                    => 'Pekerjaan wajib diisi.',
            'berkas.required'                        => 'Berkas wajib diisi.',
            'berkas.mimes'                           => 'Berkas harus berformat PDF atau DOCX.',
        ];

        if (
            $request->penyitaan_type == 'penetapan_persetujuan_penyitaan' || $request->penyitaan_type == 'penetapan_izin_penyitaan_khusus_pasal_43'
            || $request->penyitaan_type == 'penetapan_penolakan_izin_penyitaan'
        ) {

            $addrules = [
                'laporan_penyidik_date'         => 'required',
                'ba_penyitaan_date'             => 'required',
                'no_laporan_penyidik'           => 'required',
                'no_ba_penyitaan'               => 'required',
            ];

            $addmessages = [
                'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
                'ba_penyitaan_date.required'             => 'Tanggal Berita Acara Penyitaan wajib diisi.',
                'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
                'no_ba_penyitaan.required'               => 'No Berita Acara Penyitaan wajib diisi.',
            ];
            $rules = array_merge($rules, $addrules);
            $messages = array_merge($messages, $addmessages);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        }

        $sita = new SitaModel();
        $sita->uuid = (string) Str::uuid();
        $sita->registerdate = $request->register_date;
        $sita->sitatype = $request->penyitaan_type;
        $sita->satuanid = $request->satuan_kerja;
        $sita->permohonanpenyidikdate = $request->permohonan_penyidik_date;
        $sita->nopermohonanpenyidik = $request->no_permohonan_penyidik;
        $sita->perintahsitadate = $request->perintah_penyitaan_date;
        $sita->noperintahsita = $request->no_perintah_penyitaan;
        $sita->izinsita = $request->izin_sita;
        $sita->sitaan = $request->penyitaan;
        $sita->laporanpenyidikdate = $request->laporan_penyidik_date;
        $sita->nolaporanpenyidik = $request->no_laporan_penyidik;
        $sita->basitadate = $request->ba_penyitaan_date;
        $sita->nobasita = $request->no_ba_penyitaan;
        $sita->pihaksita = $request->jenis_pihak;
        $sita->namapihak = $request->nama_pihak;
        $sita->alamatpihak = $request->alamat;
        $sita->pihakbirthplace = $request->birthplace;
        $sita->pihakbirthdate = $request->birthdate;
        $sita->sextype = $request->sex_type;
        $sita->religion = $request->religion;
        $sita->nation = $request->nation;
        $sita->occupation = $request->occupation;

        $file = $request->file('berkas');
        $path = 'berkas/sita';
        $filename = 'data_' . $request->no_permohonan_penyidik . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $sita->berkasurl = $filename;
        $sita->statussita = 'proses';
        $sita->createdby = $request->session()->get('userid');
        $sita->save();

        return redirect('/' . $menu . '/sita')->with('success', 'Data Penyitaan berhasil ditambahkan. Silahkan melakukan proses selanjutnya.');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        }

        $sita = SitaModel::where('uuid', $request->uuid)->first();

        if (empty($sita)) {
            return redirect()->back()->with('error', 'Data penyitaan tidak ditemukan! Silahkan cek kembali');
        }

        $sita->status = 0;
        $sita->save();

        return redirect('/' . $menu . '/sita')->with('success', 'Data penyitaan berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'register_date'                 => 'required',
            'penyitaan_type'                => 'required',
            'satuan_kerja'                  => 'required',
            'permohonan_penyidik_date'      => 'required',
            'perintah_penyitaan_date'       => 'required',
            'izin_sita'                     => 'required',
            // 'laporan_penyidik_date'         => 'required',
            // 'ba_penyitaan_date'             => 'required',
            'no_permohonan_penyidik'        => 'required',
            'no_perintah_penyitaan'         => 'required',
            'penyitaan'                     => 'required',
            // 'no_laporan_penyidik'           => 'required',
            // 'no_ba_penyitaan'               => 'required',
            'jenis_pihak'                   => 'required',
            'nama_pihak'                    => 'required',
            'alamat'                        => 'required',
            'birthplace'                    => 'required',
            'birthdate'                     => 'required',
            'sex_type'                      => 'required',
            'nation'                        => 'required',
            'religion'                      => 'required',
            'occupation'                    => 'required',
        ];

        $messages = [
            'penyitaan_type.required'                => 'Jenis Penyitaan wajib diisi.',
            'register_date.required'                 => 'Tanggal Register wajib diisi.',
            'satuan_kerja.required'                  => 'Satuan Kerja wajib diisi.',
            'permohonan_penyidik_date.required'      => 'Tanggal Permohonan Penyidik wajib diisi.',
            'perintah_penyitaan_date.required'       => 'Tanggal Perintah Penyitaan wajib diisi.',
            'izin_sita.required'                     => 'Izin Penyitaan wajib diisi.',
            // 'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
            // 'ba_penyitaan_date.required'             => 'Tanggal Berita Acara Penyitaan wajib diisi.',
            'no_permohonan_penyidik.required'        => 'No Surat Permohonan Penyidik wajib diisi.',
            'no_perintah_penyitaan.required'         => 'No Surat Perintah Penyitaan wajib diisi.',
            'penyitaan.required'                     => 'Penyitaan wajib diisi.',
            // 'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
            // 'no_ba_penyitaan.required'               => 'No Berita Acara Penyitaan wajib diisi.',
            'jenis_pihak.required'                   => 'Jenis Pihak wajib diisi.',
            'nama_pihak.required'                    => 'Nama Pihak wajib diisi.',
            'alamat.required'                        => 'Alamat wajib diisi.',
            'birthplace.required'                    => 'Tanggal lahir wajib diisi.',
            'birthdate.required'                     => 'Tempat lahir wajib diisi.',
            'sex_type.required'                      => 'Jenis kelamin wajib diisi.',
            'nation.required'                        => 'Kebangsaan wajib diisi.',
            'religion.required'                      => 'Agama wajib diisi.',
            'occupation.required'                    => 'Pekerjaan wajib diisi.',
            'berkas.required'                        => 'Berkas wajib diisi.',
        ];

        if (
            $request->penyitaan_type == 'penetapan_persetujuan_penyitaan' || $request->penyitaan_type == 'penetapan_izin_penyitaan_khusus_pasal_43'
            || $request->penyitaan_type == 'penetapan_penolakan_izin_penyitaan'
        ) {

            $addrules = [
                'laporan_penyidik_date'         => 'required',
                'ba_penyitaan_date'             => 'required',
                'no_laporan_penyidik'           => 'required',
                'no_ba_penyitaan'               => 'required',
            ];

            $addmessages = [
                'laporan_penyidik_date.required'         => 'Tanggal Laporan Penyidik wajib diisi.',
                'ba_penyitaan_date.required'             => 'Tanggal Berita Acara Penyitaan wajib diisi.',
                'no_laporan_penyidik.required'           => 'No Surat Laporan Penyidik wajib diisi.',
                'no_ba_penyitaan.required'               => 'No Berita Acara Penyitaan wajib diisi.',
            ];
            $rules = array_merge($rules, $addrules);
            $messages = array_merge($messages, $addmessages);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        }

        $sita = SitaModel::where('uuid', $uuid)->first();
        if ($sita == null) {
            return redirect()->back()->with('error', 'Data sita tidak ditemukan! Silahkan cek kembali');
        }

        $sita->registerdate = $request->register_date;
        $sita->sitatype = $request->penyitaan_type;
        $sita->satuanid = $request->satuan_kerja;
        $sita->permohonanpenyidikdate = $request->permohonan_penyidik_date;
        $sita->nopermohonanpenyidik = $request->no_permohonan_penyidik;
        $sita->perintahsitadate = $request->perintah_penyitaan_date;
        $sita->noperintahsita = $request->no_perintah_penyitaan;
        $sita->izinsita = $request->izin_sita;
        $sita->sitaan = $request->penyitaan;
        $sita->laporanpenyidikdate = $request->laporan_penyidik_date;
        $sita->nolaporanpenyidik = $request->no_laporan_penyidik;
        $sita->basitadate = $request->ba_penyitaan_date;
        $sita->nobasita = $request->no_ba_penyitaan;
        $sita->pihaksita = $request->jenis_pihak;
        $sita->namapihak = $request->nama_pihak;
        $sita->alamatpihak = $request->alamat;
        $sita->pihakbirthplace = $request->birthplace;
        $sita->pihakbirthdate = $request->birthdate;
        $sita->sextype = $request->sex_type;
        $sita->religion = $request->religion;
        $sita->nation = $request->nation;
        $sita->occupation = $request->occupation;
        $sita->modifiedby = $request->session()->get('userid');
        $sita->modifiedat = Carbon::now();
        $sita->save();

        return redirect('/' . $menu . '/sita/detail/' . $uuid)->with('success', 'Sita berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:pdf,docx,doc',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file pdf/doc/docx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file PDF/DOCX.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        } else if (strpos($url, 'kejaksaan')) {
            $menu = 'kejaksaan';
        }

        $sita = SitaModel::where('uuid', $uuid)->first();
        if ($sita == null) {
            return redirect()->back()->with('error', 'Data sita tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/sita';
        $filename = 'data_' . $sita->nopermohonanpenyidik . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $sita->berkasurl = $filename;
        $sita->save();

        return redirect('/' . $menu . '/sita/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $sita = SitaModel::where('uuid', $uuid)->first();
        if ($sita == null) {
            return redirect()->back()->with('error', 'Data sita tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/sita/" . $sita->berkasurl;
        $extension = substr($file, -4);

        if ($extension == '.pdf') {
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
        } else {
            $headers = [
                'Content-Type' => 'application/vnd.ms-word',
            ];
        }

        return response()->download($file, $sita->berkasurl, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_sita'            => 'required',
        ];

        $messages = [
            'status_sita.required'         => 'Status sita wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $sita = SitaModel::where('uuid', $uuid)->first();
        if ($sita == null) {
            return redirect()->back()->with('error', 'Data sita tidak ditemukan! Silahkan cek kembali');
        }

        $sita->statussita = $request->status_sita;
        $sita->modifiedby = $request->session()->get('userid');
        $sita->modifiedat = Carbon::now();
        $sita->save();

        return redirect('/' . $menu . '/sita/detail/' . $uuid)->with('success', 'Status sita berhasil di update! Silahkan melakukan proses lebih lanjut');
    }
}
