<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SatuanModel;
use App\Models\SettingModel;
use App\Models\TilangModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class TilangController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        if (View::exists('admin.tilang.index')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $tilang = TilangModel::select('trx_tilang.*')->with('pengaju')->with('satuan')->where('trx_tilang.status', 1);

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                // $tilang = $tilang->join('ref_satuan', 'trx_tilang.satuanid', '=', 'ref_satuan.satuanid')->where('ref_satuan.satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();
            $tilang = $tilang->paginate('10');

            return view('admin.tilang.index', compact(
                'session',
                'setting',
                'satuan',
                'tilang',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_penindakan'          => 'required',
            'tanggal_sidang'              => 'required',
            'berkas'                      => 'required|mimes:xls,xlsx',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_penindakan.required'       => 'Tanggal penindakan wajib diisi.',
            'tanggal_sidang.required'           => 'Tanggal sidang wajib diisi.',
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file xls atau xlsx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tilang = new TilangModel();
        $tilang->uuid = (string) Str::uuid();
        $tilang->registerdate = $request->tanggal_register;
        $tilang->satuanid = $request->satuan_kerja;
        $tilang->tanggalpenindakan = $request->tanggal_penindakan;
        $tilang->tanggalsidang = $request->tanggal_sidang;

        $file = $request->file('berkas');
        $path = 'berkas/tilang';
        $filename = 'data_' . $request->uuid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $tilang->berkas = $filename;
        $tilang->statustilang = 'pending';
        $tilang->createdby = $request->session()->get('userid');
        $tilang->save();

        return redirect('/' . $menu . '/tilang')->with('success', 'Tilang berhasil ditambahkan! Silahkan melakukan proses lebih lanjut');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tilang = TilangModel::where('uuid', $request->uuid)->first();

        if (empty($tilang)) {
            return redirect()->back()->with('error', 'Data tilang tidak ditemukan! Silahkan cek kembali');
        }

        $tilang->status = 0;
        $tilang->save();

        return redirect('/' . $menu . '/tilang')->with('success', 'Data tilang berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.tilang.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            }

            $tilang = TilangModel::with('pengaju')->with('satuan')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $statustilang = $this->statusTilang();

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.tilang.detail', compact(
                'session',
                'tilang',
                'setting',
                'satuan',
                'statustilang',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_penindakan'          => 'required',
            'tanggal_sidang'              => 'required',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_penindakan.required'       => 'Tanggal penindakan wajib diisi.',
            'tanggal_sidang.required'           => 'Tanggal sidang wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tilang = TilangModel::where('uuid', $uuid)->first();
        if ($tilang == null) {
            return redirect()->back()->with('error', 'Data tilang tidak ditemukan! Silahkan cek kembali');
        }

        $tilang->registerdate = $request->tanggal_register;
        $tilang->satuanid = $request->satuan_kerja;
        $tilang->tanggalpenindakan = $request->tanggal_penindakan;
        $tilang->tanggalsidang = $request->tanggal_sidang;
        $tilang->modifiedby = $request->session()->get('userid');
        $tilang->modifiedat = Carbon::now();
        $tilang->save();

        return redirect('/' . $menu . '/tilang/detail/' . $uuid)->with('success', 'Tilang berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:xls,xlsx',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file xls atau xlsx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file XLS atau XLSX.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tilang = TilangModel::where('uuid', $uuid)->first();
        if ($tilang == null) {
            return redirect()->back()->with('error', 'Data tilang tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/tilang';
        $filename = 'data_' . $tilang->uuid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $tilang->berkas = $filename;
        $tilang->save();

        return redirect('/' . $menu . '/tilang/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $tilang = TilangModel::where('uuid', $uuid)->first();
        if ($tilang == null) {
            return redirect()->back()->with('error', 'Data tilang tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/tilang/" . $tilang->berkas;
        $headers = [
            'Content-Type' => 'application/vnd.ms-excel',
        ];

        return response()->download($file, $tilang->berkas, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_tilang'            => 'required',
        ];

        $messages = [
            'status_tilang.required'         => 'Status tilang wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tilang = TilangModel::where('uuid', $uuid)->first();
        if ($tilang == null) {
            return redirect()->back()->with('error', 'Data tilang tidak ditemukan! Silahkan cek kembali');
        }

        $tilang->statustilang = $request->status_tilang;
        $tilang->modifiedby = $request->session()->get('userid');
        $tilang->modifiedat = Carbon::now();
        $tilang->save();

        return redirect('/' . $menu . '/tilang/detail/' . $uuid)->with('success', 'Status tilang berhasil di update! Silahkan melakukan proses lebih lanjut');
    }
}
