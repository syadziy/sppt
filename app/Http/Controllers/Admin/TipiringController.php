<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SatuanModel;
use App\Models\SettingModel;
use App\Models\TipiringModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class TipiringController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        if (View::exists('admin.tipiring.index')) {
            $session = $request->session();
            $setting = SettingModel::first();

            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $tipiring = TipiringModel::select('trx_tipiring.*')->with('pengaju')->with('satuan')->where('trx_tipiring.status', 1);

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
                // $tipiring = $tipiring->join('ref_satuan', 'trx_tipiring.satuanid', '=', 'ref_satuan.satuanid')->where('ref_satuan.satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();
            $tipiring = $tipiring->paginate('10');

            return view('admin.tipiring.index', compact(
                'session',
                'setting',
                'satuan',
                'tipiring',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_pelimpahan'          => 'required',
            'nama_tersangka'              => 'required',
            'berkas'                      => 'required|mimes:doc,docx',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_pelimpahan.required'       => 'Tanggal pelimpahan wajib diisi.',
            'nama_tersangka.required'           => 'Nama tersangka wajib diisi.',
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file doc atau docx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tipiring = new TipiringModel();
        $tipiring->uuid = (string) Str::uuid();
        $tipiring->registerdate = $request->tanggal_register;
        $tipiring->satuanid = $request->satuan_kerja;
        $tipiring->tanggalpelimpahan = $request->tanggal_pelimpahan;
        $tipiring->namatersangka = $request->nama_tersangka;

        $file = $request->file('berkas');
        $path = 'berkas/tipiring';
        $filename = 'data_' . $request->uuid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $tipiring->berkas = $filename;
        $tipiring->statustipiring = 'pending';
        $tipiring->createdby = $request->session()->get('userid');
        $tipiring->save();

        return redirect('/' . $menu . '/tipiring')->with('success', 'BAPC tipiring berhasil ditambahkan! Silahkan melakukan proses lebih lanjut');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tipiring = TipiringModel::where('uuid', $request->uuid)->first();

        if (empty($tipiring)) {
            return redirect()->back()->with('error', 'Data tipiring tidak ditemukan! Silahkan cek kembali');
        }

        $tipiring->status = 0;
        $tipiring->save();

        return redirect('/' . $menu . '/tipiring')->with('success', 'Data tipiring berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.tipiring.detail')) {
            $session = $request->session();
            $setting = SettingModel::first();

            // Filter Satuan By Menu
            $url = $request->url();
            if (strpos($url, 'kepolisian')) {
                $menu = 'kepolisian';
            } else if (strpos($url, 'pengadilan_negeri')) {
                $menu = 'pengadilan_negeri';
            }

            $tipiring = TipiringModel::with('pengaju')->with('satuan')->where('uuid', $uuid)->first();
            $satuan = SatuanModel::where('roleid', 3)->where('status', 1);
            $statustipiring = $this->statusTipiring();

            // CEK ROLE SUPERADMIN
            if ($session->get('roleid') != 1 && $session->get('roleid') != 2) {
                // $satuan = $satuan->where('satuanid', $session->get('satuanid'));
            }

            $satuan = $satuan->get();

            return view('admin.tipiring.detail', compact(
                'session',
                'tipiring',
                'setting',
                'satuan',
                'statustipiring',
                'menu'
            ));
        }

        return view('admin.404');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'tanggal_register'            => 'required',
            'satuan_kerja'                => 'required',
            'tanggal_pelimpahan'          => 'required',
            'nama_tersangka'              => 'required',
        ];

        $messages = [
            'tanggal_register.required'         => 'Tanggal register wajib diisi.',
            'satuan_kerja.required'             => 'Satuan kerja wajib diisi.',
            'tanggal_pelimpahan.required'       => 'Tanggal pelimpahan wajib diisi.',
            'nama_tersangka.required'           => 'Nama tersangka wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tipiring = TipiringModel::where('uuid', $uuid)->first();
        if ($tipiring == null) {
            return redirect()->back()->with('error', 'Data tipiring tidak ditemukan! Silahkan cek kembali');
        }

        $tipiring->registerdate = $request->tanggal_register;
        $tipiring->satuanid = $request->satuan_kerja;
        $tipiring->tanggalpelimpahan = $request->tanggal_pelimpahan;
        $tipiring->namatersangka = $request->nama_tersangka;
        $tipiring->modifiedby = $request->session()->get('userid');
        $tipiring->modifiedat = Carbon::now();
        $tipiring->save();

        return redirect('/' . $menu . '/tipiring/detail/' . $uuid)->with('success', 'BAPC Tipiring berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function reupload(Request $request, $uuid)
    {
        $rules = [
            'berkas'                      => 'required|mimes:doc,docx',
        ];

        $messages = [
            'berkas.required'                   => 'Berkas wajib diisi.',
            'berkas.mimes'                      => 'Berkas harus berupa file doc atau docx.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Berkas wajib diisi dan berkas yang di upload harus berupa file DOC atau DOCX.');
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tipiring = TipiringModel::where('uuid', $uuid)->first();
        if ($tipiring == null) {
            return redirect()->back()->with('error', 'Data tipiring tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('berkas');
        $path = 'berkas/tipiring';
        $filename = 'data_' . $tipiring->uuid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $tipiring->berkas = $filename;
        $tipiring->save();

        return redirect('/' . $menu . '/tipiring/detail/' . $uuid)->with('success', 'Berkas berhasil di update! Silahkan melakukan proses lebih lanjut');
    }

    public function download($uuid)
    {
        $tipiring = TipiringModel::where('uuid', $uuid)->first();
        if ($tipiring == null) {
            return redirect()->back()->with('error', 'Data tipiring tidak ditemukan! Silahkan cek kembali');
        }

        $file = public_path() . "/berkas/tipiring/" . $tipiring->berkas;
        $headers = [
            'Content-Type' => 'application/vnd.ms-word',
        ];

        return response()->download($file, $tipiring->berkas, $headers);
    }

    public function updateStatus(Request $request, $uuid)
    {
        $rules = [
            'status_tipiring'            => 'required',
        ];

        $messages = [
            'status_tipiring.required'         => 'Status tipiring wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        // Filter Satuan By Menu
        $url = $request->url();
        if (strpos($url, 'kepolisian')) {
            $menu = 'kepolisian';
        } else if (strpos($url, 'pengadilan_negeri')) {
            $menu = 'pengadilan_negeri';
        }

        $tipiring = TipiringModel::where('uuid', $uuid)->first();
        if ($tipiring == null) {
            return redirect()->back()->with('error', 'Data tipiring tidak ditemukan! Silahkan cek kembali');
        }

        $tipiring->statustipiring = $request->status_tipiring;
        $tipiring->modifiedby = $request->session()->get('userid');
        $tipiring->modifiedat = Carbon::now();
        $tipiring->save();

        return redirect('/' . $menu . '/tipiring/detail/' . $uuid)->with('success', 'Status tipiring berhasil di update! Silahkan melakukan proses lebih lanjut');
    }
}
