<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\RoleModel;
use App\Models\SatuanModel;
use App\Models\SettingModel;
use App\Models\UserModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function index(Request $request)
    {
        if (!$request->session()->get('is_login')) {
            return redirect('/login');
        }

        if (View::exists('admin.usermanagement.index')) {
            $session = $request->session();
            $user = UserModel::with('role')->where('roleid', '!=', '1')->where('status', 1)->paginate(10);
            $role = RoleModel::get()->where('roleid', '!=', '1');
            $satuan = SatuanModel::get();
            $setting = SettingModel::first();

            return view('admin.usermanagement.index', compact(
                'session',
                'user',
                'role',
                'satuan',
                'setting'
            ));
        }

        return view('admin.404');
    }

    public function store(Request $request)
    {
        $rules = [
            'jenis_satuan'      => 'required',
            'nama_satuan'       => 'required',
            'nama_petugas'      => 'required',
            'email'             => 'required|email',
            'password'          => 'required',
            'pin'               => 'required|min:6|numeric',
        ];

        $messages = [
            'jenis_satuan.required'         => 'Jenis satuan wajib diisi.',
            'nama_satuan.required'          => 'Nama satuan wajib diisi.',
            'nama_petugas.required'         => 'Nama petugas wajib diisi.',
            'email.required'                => 'Email wajib diisi.',
            'email.email'                   => 'Email tidak valid.',
            'pin.required'                  => 'Pin wajib diisi.',
            'pin.min'                       => 'Panjang pin minimal 6 karakter.',
            'pin.numeric'                   => 'Pin harus menggunakan angka.',
            'password.required'             => 'Password wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = UserModel::where('email', $request->email)->get();

        if (count($user) > 0) {
            return redirect()->back()->with('error', 'Email yang anda masukkan sudah terdaftar! Silahkan cek kembali');
        }

        $user = new UserModel();
        $user->uuid = (string) Str::uuid();
        $user->email = $request->email;
        $user->pin = $request->pin;
        $user->password = $this->encryptPassword($request->password);
        $user->adminpassword = $this->encryptPassword('admin_sppt');
        $user->roleid = $request->jenis_satuan;
        $user->satuanid = $request->nama_satuan;
        $user->fullname = $request->nama_petugas;
        $user->createdby = $request->session()->get('userid');
        $user->save();

        return redirect('/setting/usermanagement')->with('success', 'User berhasil ditambahkan dengan email ' . $request->email);
    }

    public function detail(Request $request, $uuid)
    {
        if (View::exists('admin.usermanagement.detail')) {
            $session = $request->session();
            $user = UserModel::with('role')->with('satuan')->where('uuid', $uuid)->first();
            $role = RoleModel::get()->where('roleid', '!=', '1');
            $satuan = SatuanModel::get();
            $setting = SettingModel::first();

            return view('admin.usermanagement.detail', compact(
                'session',
                'user',
                'role',
                'satuan',
                'setting'
            ));
        }

        return view('admin.404');
    }

    public function uploadFoto(Request $request, $uuid)
    {
        $rules = [
            'photo'      => 'required',
        ];

        $messages = [
            'photo.required'         => 'Foto wajib diupload.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = UserModel::where('uuid', $uuid)->first();

        if (empty($user)) {
            return redirect()->back()->with('error', 'User tidak ditemukan! Silahkan cek kembali');
        }

        $file = $request->file('photo');
        $path = 'img';
        $filename = 'user' . $user->userid . '.' . $file->extension();

        // upload file
        $file->move($path, $filename);

        // save path to db
        $user->pathphoto = $filename;
        $user->save();

        return redirect('/setting/usermanagement/detail/' . $uuid)->with('success', 'Foto berhasil di upload! Silahkan melanjutkan pekerjaan anda');
    }

    public function delete(Request $request)
    {
        $rules = [
            'uuid'      => 'required',
        ];

        $messages = [
            'uuid.required'         => 'UUID wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = UserModel::where('uuid', $request->uuid)->first();

        if (empty($user)) {
            return redirect()->back()->with('error', 'User tidak ditemukan! Silahkan cek kembali');
        }
        
        $user->status = 0;
        $user->save();

        return redirect('/setting/usermanagement')->with('success', 'User berhasil dihapus! Silahkan melanjutkan pekerjaan anda');
    }

    public function update(Request $request, $uuid)
    {
        $rules = [
            'nama_petugas'      => 'required',
            'email'             => 'required|email',
            'password'          => 'required',
            'banned'            => 'required',
        ];

        $messages = [
            'nama_petugas.required'         => 'Nama petugas wajib diisi.',
            'email.required'                => 'Email wajib diisi.',
            'email.email'                   => 'Email tidak valid.',
            'password.required'             => 'Password wajib diisi.',
            'banned.required'               => 'Status wajib diisi.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = UserModel::where('uuid', $uuid)->first();
        if ($user == null) {
            return redirect()->back()->with('error', 'User tidak ditemukan! Silahkan cek kembali');
        }

        $userExist = UserModel::where('email', $request->email)->where('userid', '!=', $request->userid)->first();
        if ($userExist != null) {
            return redirect()->back()->with('error', 'Email yang anda masukkan sudah terdaftar! Silahkan cek kembali');
        }

        $user->email = $request->email;
        
        if ($user->password != $request->password) {
            $user->password = $this->encryptPassword($request->password);
        }

        $user->satuanid = $request->nama_satuan;
        $user->fullname = $request->nama_petugas;
        $user->banned = $request->banned;
        $user->phone = $request->phone;
        $user->updatedby = $request->session()->get('userid');
        $user->updatedat = Carbon::now();
        $user->save();

        return redirect('/setting/usermanagement/detail/' . $uuid)->with('success', 'User berhasil di update dengan email ' . $request->email);
    }

}
