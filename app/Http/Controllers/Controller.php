<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function encryptPassword($password)
    {
        return hash('sha256', $password . 'sttpv2');
    }

    public function religion() {
        return array(
            'islam' => 'Islam',
            'kristen' => 'Kristen',
            'protestan' => 'Protestan',
            'buddha' => 'Buddha',
            'kong_hu_cu' => 'Kong Hu Cu',
            'hindu' => 'Hindu'
        );
    }

    public function gender() {
        return array(
            'laki_laki' => 'Laki-laki',
            'perempuan' => 'Perempuan'
        );
    }

    public function jenisPermohonanPenahanan() {
        return array(
            'perpanjangan_penahanan' => 'Perpanjangan Penahanan'
        );
    }
    
    public function jenisTempatTahanan() {
        return array(
            'tahanan_rumah' => 'Tahanan Rumah',
            'tahanan_rutan' => 'Tahanan Rutan',
            'tahanan_kota' => 'Tahanan Kota'
        );
    }

    public function statusPenahanan() {
        return array(
            'proses' => 'Proses',
            'tolak' => 'Tolak',
            'terima' => 'Terima'
        );
    }
    
    public function statusTilang() {
        return array(
            'selesai' => 'Selesai',
            'batal' => 'Batal'
        );
    }
    
    public function statusTipiring() {
        return array(
            'selesai' => 'Selesai',
            'batal' => 'Batal'
        );
    }
    
    public function statusDiversi() {
        return array(
            'selesai' => 'Selesai',
            'batal' => 'Batal'
        );
    }

    public function statusSita() {
        return array(
            'proses' => 'Proses',
            'tolak' => 'Tolak',
            'terima' => 'Terima'
        );
    }

    public function statusGeledah() {
        return array(
            'proses' => 'Proses',
            'tolak' => 'Tolak',
            'terima' => 'Terima'
        );
    }

    public function jenisPenyitaan() {
        return array(
            'penetapan_izin_penyitaan' => 'Penetapan Izin Penyitaan',
            'penetapan_persetujuan_penyitaan' => 'Penetapan Persetujuan Penyitaan',
            'penetapan_izin_penyitaan_khusus_pasal_43' => 'Penetapan Izin penyitaan Khusus Pasal 43',
            'penetapan_penolakan_izin_penyitaan' => 'Penetapan Penolakan Izin Penyitaan'
        );
    }

    public function jenisGeledah() {
        return array(
            'penetapan_izin_penggeledahan' => 'Penetapan Izin Penggeledahan',
            'penetapan_persetujuan_penggeledahan' => 'Penetapan Persetujuan Penggeledahan',
            'penetapan_penolakan_izin_penggeledahan' => 'Penetapan Penolakan Izin Penggeledahan'
        );
    }

    public function targetGeledah() {
        return array(
            'badan_dan_atau_pakaian' => 'Badan dan atau Pakaian',
            'rumah_tempat_tinggal' => 'Rumah Tempat Tinggal',
            'tempat_tertutup_lainnya' => 'Tempat Tertutup Lainnya',
            'lain_lain' => 'Lain-lain'
        );
    }

    public function getValuePenyitaan($key) {
        $list = $this->jenisPenyitaan();
        foreach($list as $x => $val) {
            if($x == $key) {
                return $val;
            }
        }
        return null;
    }

    public function getValueGeledah($key) {
        $list = $this->jenisGeledah();
        foreach($list as $x => $val) {
            if($x == $key) {
                return $val;
            }
        }
        return null;
    }
    
    public function jenisPihak() {
        return array(
            'tersangka' => 'Tersangka',
            'korban' => 'Korban',
            'pihak_lain' => 'Pihak Lain'
        );
    }

}
