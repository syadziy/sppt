<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiversiModel extends Model
{
    public $table = 'trx_diversi';

    public $timestamps = false;

    public $primaryKey = 'diversiid';

    public function pengaju() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}
