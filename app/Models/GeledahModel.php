<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeledahModel extends Model {
    public $table = 'trx_geledah';

    public $timestamps = false;

    public $primaryKey = 'geledahid';

    public function pemohon() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}