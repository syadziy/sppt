<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InboxModel extends Model
{
    public $table = 'trx_inbox';

    public $timestamps = false;

    public $primaryKey = 'inboxid';

    public function userSender() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'sender');
    }
    
    public function userReceiver() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'receiver');
    }
}
