<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IzinBesukModel extends Model
{
    public $table = 'trx_izinbesuk';

    public $timestamps = false;

    public $primaryKey = 'izinbesukid';

    public function pengaju() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }
}
