<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenahananModel extends Model
{
    public $table = 'trx_penahanan';

    public $timestamps = false;

    public $primaryKey = 'penahananid';

    public function pengaju() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}
