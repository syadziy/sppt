<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    public $table = 'ref_role';

    public $timestamps = false;

    public $primaryKey = 'roleid';
}
