<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SatuanModel extends Model
{
    public $table = 'ref_satuan';

    public $timestamps = false;

    public $primaryKey = 'satuanid';
}
