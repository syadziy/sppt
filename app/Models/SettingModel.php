<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingModel extends Model
{
    public $table = 'ref_setting';

    public $timestamps = false;

    public $primaryKey = 'settingid';
}
