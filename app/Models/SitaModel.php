<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SitaModel extends Model {
    public $table = 'trx_sita';

    public $timestamps = false;

    public $primaryKey = 'sitaid';

    public function pemohon() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}