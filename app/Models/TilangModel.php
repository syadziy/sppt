<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TilangModel extends Model
{
    public $table = 'trx_tilang';

    public $timestamps = false;

    public $primaryKey = 'tilangid';

    public function pengaju() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}
