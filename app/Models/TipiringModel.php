<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipiringModel extends Model
{
    public $table = 'trx_tipiring';

    public $timestamps = false;

    public $primaryKey = 'tipiringid';

    public function pengaju() {
        return $this->hasOne(\App\Models\UserModel::class, 'userid', 'createdby');
    }

    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}
