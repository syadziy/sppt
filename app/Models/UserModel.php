<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    public $table = 'ref_user';

    public $timestamps = false;

    public $primaryKey = 'userid';

    public function role() {
        return $this->hasOne(\App\Models\RoleModel::class, 'roleid', 'roleid');
    }
    
    public function satuan() {
        return $this->hasOne(\App\Models\SatuanModel::class, 'satuanid', 'satuanid');
    }
}
