<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Administrator | SPPT-v2</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/plugins/toastr/toastr.min.css') }}">

  <style>
    body {
      background: url("{{ asset('lte/dist/img/photo1.png') }}") no-repeat;
      background-size: cover;
    }
  </style>
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="row">
      <img class="profile-user-img img-fluid img-circle" style="width: 60px; height: 60px" src="{{ asset('logo/logo1pn.jpg') }}" alt="">
      <img class="profile-user-img img-fluid img-circle" style="width: 60px; height: 60px" src="{{ asset('logo/logo2kejari.png') }}" alt="">
      <img class="profile-user-img img-fluid img-circle" style="width: 60px; height: 60px" src="{{ asset('logo/logo3polri.png') }}" alt="">
      <img class="profile-user-img img-fluid img-circle" style="width: 60px; height: 60px" src="{{ asset('logo/logo4kemenkumham.png') }}" alt="">
    </div>
    <br>
    <div class="login-logo" style="background-color: rgba(255, 255, 255, 0.7);">
      <a href="../../index2.html"><b>SPPT</b>v2</a>
    </div>
    <!-- /.login-logo -->
    <div class="card" style="background-color: rgba(255, 255, 255, 0.5);">
      <div class="card-body login-card-body" style="background-color: rgba(255, 255, 255, 0.5);">
        <p class="login-box-msg">
          SELAMAT DATANG <br><span><b>SPPT</b> Regional {{ $setting == null ? 'Waikabubak' : ($setting->regional == null ? 'Waikabubak' : $setting->regional) }}</span>
        </p>

        {!! NoCaptcha::renderJs() !!}
        <form action="{{ url('/login/validation') }}" method="post">
          @csrf
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Email" name="email">
            <div class="input-group-append" style="background-color: #f5f5f5;">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          @error('email')
          <sup style="padding: 10px; color: red;">{{ $message }}</sup>
          @enderror

          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <div class="input-group-append" style="background-color: #f5f5f5;">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            <input type="password" class="form-control" placeholder="PIN" name="pin">
            <div class="input-group-append" style="background-color: #f5f5f5;">
              <div class="input-group-text">
                <span class="fas fa-key"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            @error('password')
            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
            @enderror
            @error('pin')
            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
            @enderror
          </div>

          {!! NoCaptcha::display() !!}
          @error('g-recaptcha-response')
          <sup style="padding: 10px; color: red;">{{ $message }}</sup>
          @enderror

          <br>
          <div class="col-12 p-0">
            <button type="submit" class="btn btn-info btn-block">Masuk</button>
          </div>
        </form>
      </div>
      <!-- /.login-card-body -->
    </div>
  </div>
  <!-- /.login-box -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/toastr/toastr.min.js') }}"></script>
  <script>
    $(function() {
      var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      let errorMessage = '<?php echo session()->get('error') ?>';
      let successMessage = '<?php echo session()->get('success') ?>';

      if (errorMessage != '') {
        Toast.fire({
          icon: 'error',
          title: errorMessage
        });
      }

      if (successMessage != '') {
        Toast.fire({
          icon: 'success',
          title: successMessage
        });
      }
    });
  </script>
</body>

</html>