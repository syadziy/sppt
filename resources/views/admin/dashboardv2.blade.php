@extends('admin.layouts.master')

@section('title', 'Dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content" style="padding: 20px 10px">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{ $geledah }}</h3>

              <p>Izin Geledah</p>
            </div>
            <div class="icon">
              <i class="fa fa-search-location"></i>
            </div>
            <a href="#" class="small-box-footer">Total Berkas terdokumentasi </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{ $sita }}</h3>

              <p>Izin Sita</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-lock"></i>
            </div>
            <a href="#" class="small-box-footer">Total Berkas terdokumentasi </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3>{{ $penahanan }}</h3>

              <p>Izin Penahanan</p>
            </div>
            <div class="icon">
              <i class="fa fa-trailer"></i>
            </div>
            <a href="#" class="small-box-footer">Total Berkas terdokumentasi </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>{{ $user }}</h3>

              <p>Petugas</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="#" class="small-box-footer">Total Berkas terdokumentasi </a>
          </div>
        </div>
        <!-- ./col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Data Penahanan (Dewasa vs Anak)</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button> -->
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <div class="row" style="display: flex; justify-content: center;">
                  <div class="row" style="margin: 0 10px;">
                    <span>
                      <div style="width: 107px; height: 10px; background-color: rgba(60,141,188,0.9);"></div> Penahanan Dewasa
                    </span>
                  </div>
                  <div class="row" style="margin: 0 10px;">
                    <span>
                      <div style="width: 92px; height: 10px; background-color: rgba(210, 214, 222, 1);"></div> Penahanan Anak
                    </span>
                  </div>
                </div>
                <canvas id="lineChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- BAR CHART -->
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Data Geledah vs Sita</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button> -->
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- PIE CHART -->
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Data Pengguna</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button> -->
              </div>
            </div>
            <div class="card-body">
              <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">

          <!-- AREA CHART -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Data Tilang vs BAPC Tipiring</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button> -->
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <div class="row" style="display: flex; justify-content: center;">
                  <div class="row" style="margin: 0 10px;">
                    <span>
                      <div style="width: 35px; height: 10px; background-color: rgba(60,141,188,0.9);"></div> Tilang
                    </span>
                  </div>
                  <div class="row" style="margin: 0 10px;">
                    <span>
                      <div style="width: 75px; height: 10px; background-color: rgba(210, 214, 222, 1);"></div> BAPC Tipiring
                    </span>
                  </div>
                </div>
                <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- STACKED BAR CHART -->
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title">Data Izin Besuk</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button> -->
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('lte/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('lte/dist/js/demo.js') }}"></script>
<script>
  $(function() {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------
    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

    var areaChartData = {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
          label: 'Tilang',
          backgroundColor: 'rgba(60,141,188,0.9)',
          borderColor: 'rgba(60,141,188,0.8)',
          pointRadius: false,
          pointColor: '#3b8bba',
          pointStrokeColor: 'rgba(60,141,188,1)',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data: [
            <?php echo $tilang[0] ?>,
            <?php echo $tilang[1] ?>,
            <?php echo $tilang[2] ?>,
            <?php echo $tilang[3] ?>,
            <?php echo $tilang[4] ?>,
            <?php echo $tilang[5] ?>,
            <?php echo $tilang[6] ?>,
            <?php echo $tilang[7] ?>,
            <?php echo $tilang[8] ?>,
            <?php echo $tilang[9] ?>,
            <?php echo $tilang[10] ?>,
            <?php echo $tilang[11] ?>,
          ]
        },
        {
          label: 'BAPC Tipiring',
          backgroundColor: 'rgba(210, 214, 222, 1)',
          borderColor: 'rgba(210, 214, 222, 1)',
          pointRadius: false,
          pointColor: 'rgba(210, 214, 222, 1)',
          pointStrokeColor: '#c1c7d1',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data: [
            <?php echo $tipiring[0] ?>,
            <?php echo $tipiring[1] ?>,
            <?php echo $tipiring[2] ?>,
            <?php echo $tipiring[3] ?>,
            <?php echo $tipiring[4] ?>,
            <?php echo $tipiring[5] ?>,
            <?php echo $tipiring[6] ?>,
            <?php echo $tipiring[7] ?>,
            <?php echo $tipiring[8] ?>,
            <?php echo $tipiring[9] ?>,
            <?php echo $tipiring[10] ?>,
            <?php echo $tipiring[11] ?>,
          ]
        },
      ]
    }

    var areaChartOptions = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          gridLines: {
            display: false,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    new Chart(areaChartCanvas, {
      type: 'line',
      data: areaChartData,
      options: areaChartOptions
    })

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartOptions = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          gridLines: {
            display: false,
          }
        }]
      }
    }

    var lineChartData = {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
          label: 'Penahanan Dewasa',
          backgroundColor: 'rgba(60,141,188,0.9)',
          borderColor: 'rgba(60,141,188,0.8)',
          pointRadius: false,
          pointColor: '#3b8bba',
          pointStrokeColor: 'rgba(60,141,188,1)',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data: [
            <?php echo $penahananDewasa[0] ?>,
            <?php echo $penahananDewasa[1] ?>,
            <?php echo $penahananDewasa[2] ?>,
            <?php echo $penahananDewasa[3] ?>,
            <?php echo $penahananDewasa[4] ?>,
            <?php echo $penahananDewasa[5] ?>,
            <?php echo $penahananDewasa[6] ?>,
            <?php echo $penahananDewasa[7] ?>,
            <?php echo $penahananDewasa[8] ?>,
            <?php echo $penahananDewasa[9] ?>,
            <?php echo $penahananDewasa[10] ?>,
            <?php echo $penahananDewasa[11] ?>,
          ]
        },
        {
          label: 'Penahanan Anak',
          backgroundColor: 'rgba(210, 214, 222, 1)',
          borderColor: 'rgba(210, 214, 222, 1)',
          pointRadius: false,
          pointColor: 'rgba(210, 214, 222, 1)',
          pointStrokeColor: '#c1c7d1',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data: [
            <?php echo $penahananAnak[0] ?>,
            <?php echo $penahananAnak[1] ?>,
            <?php echo $penahananAnak[2] ?>,
            <?php echo $penahananAnak[3] ?>,
            <?php echo $penahananAnak[4] ?>,
            <?php echo $penahananAnak[5] ?>,
            <?php echo $penahananAnak[6] ?>,
            <?php echo $penahananAnak[7] ?>,
            <?php echo $penahananAnak[8] ?>,
            <?php echo $penahananAnak[9] ?>,
            <?php echo $penahananAnak[10] ?>,
            <?php echo $penahananAnak[11] ?>,
          ]
        },
      ]
    }

    var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
    var lineChartOptions = $.extend(true, {}, lineChartOptions)
    var lineChartData = $.extend(true, {}, lineChartData)
    lineChartData.datasets[0].fill = false;
    lineChartData.datasets[1].fill = false;
    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, {
      type: 'line',
      data: lineChartData,
      options: lineChartOptions
    })

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData = {
      labels: [
        'Pengadilan Negeri',
        'Kejaksaan',
        'Kepolisian',
        'Rutan',
      ],
      datasets: [{
        data: ['<?php echo $userPN ?>', '<?php echo $userKJ ?>', '<?php echo $userKP ?>', '<?php echo $userRT ?>'],
        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef'],
      }]
    };
    var pieOptions = {
      maintainAspectRatio: false,
      responsive: true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

    //-------------
    //- BAR CHART -
    //-------------
    var barChartData = {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
          label: 'Sita',
          backgroundColor: 'rgba(60,141,188,0.9)',
          borderColor: 'rgba(60,141,188,0.8)',
          pointRadius: false,
          pointColor: '#3b8bba',
          pointStrokeColor: 'rgba(60,141,188,1)',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data: [
            <?php echo $sitaChart[0] ?>,
            <?php echo $sitaChart[1] ?>,
            <?php echo $sitaChart[2] ?>,
            <?php echo $sitaChart[3] ?>,
            <?php echo $sitaChart[4] ?>,
            <?php echo $sitaChart[5] ?>,
            <?php echo $sitaChart[6] ?>,
            <?php echo $sitaChart[7] ?>,
            <?php echo $sitaChart[8] ?>,
            <?php echo $sitaChart[9] ?>,
            <?php echo $sitaChart[10] ?>,
            <?php echo $sitaChart[11] ?>,
          ]
        },
        {
          label: 'Geledah',
          backgroundColor: 'rgba(210, 214, 222, 1)',
          borderColor: 'rgba(210, 214, 222, 1)',
          pointRadius: false,
          pointColor: 'rgba(210, 214, 222, 1)',
          pointStrokeColor: '#c1c7d1',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data: [
            <?php echo $geledahChart[0] ?>,
            <?php echo $geledahChart[1] ?>,
            <?php echo $geledahChart[2] ?>,
            <?php echo $geledahChart[3] ?>,
            <?php echo $geledahChart[4] ?>,
            <?php echo $geledahChart[5] ?>,
            <?php echo $geledahChart[6] ?>,
            <?php echo $geledahChart[7] ?>,
            <?php echo $geledahChart[8] ?>,
            <?php echo $geledahChart[9] ?>,
            <?php echo $geledahChart[10] ?>,
            <?php echo $geledahChart[11] ?>,
          ]
        },
      ]
    }

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, barChartData)
    var temp0 = barChartData.datasets[0]
    var temp1 = barChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

    //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedChartData = {
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      datasets: [{
        label: 'Izin Besuk',
        backgroundColor: 'rgba(60,141,188,0.9)',
        borderColor: 'rgba(60,141,188,0.8)',
        pointRadius: false,
        pointColor: '#3b8bba',
        pointStrokeColor: 'rgba(60,141,188,1)',
        pointHighlightFill: '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data: [
          <?php echo $izinBesuk[0] ?>,
          <?php echo $izinBesuk[1] ?>,
          <?php echo $izinBesuk[2] ?>,
          <?php echo $izinBesuk[3] ?>,
          <?php echo $izinBesuk[4] ?>,
          <?php echo $izinBesuk[5] ?>,
          <?php echo $izinBesuk[6] ?>,
          <?php echo $izinBesuk[7] ?>,
          <?php echo $izinBesuk[8] ?>,
          <?php echo $izinBesuk[9] ?>,
          <?php echo $izinBesuk[10] ?>,
          <?php echo $izinBesuk[11] ?>,
        ]
      }]
    }

    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = $.extend(true, {}, stackedChartData)

    var stackedBarChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }

    new Chart(stackedBarChartCanvas, {
      type: 'bar',
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  })
</script>
@endsection