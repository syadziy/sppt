@extends('admin.layouts.master')

@section('title', 'Diversi')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Diversi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $menu == 'kepolisian' ? 'Kepolisian' : ($menu == 'kejaksaan' ? 'Kejaksaan' : 'Pengadilan Negeri') }}</li>
                        <li class="breadcrumb-item active"><a href="{{ url('/' . $menu . '/diversi') }}">Diversi</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-danger row">
                        <div class="col-9">
                            <h5><i class="fas fa-info" style="font-size: 14px; padding: 0 10px 0 0;"></i> Status Diversi:</h5>
                            Proses Diversi atas nama terdakwa / tersangka: <b>{{ $diversi->namatersangka }}</b> {{ $diversi->statusdiversi == 'pending' ? 'masih menunggu konfirmasi' : ($diversi->statusdiversi == 'selesai' ? 'telah selesai' : 'dibatalkan') }}
                        </div>
                        @if($session->get('roleid') == 1)
                        <div class="col-3 p-0" style="float: right;">
                            <form action="{{ url($menu . '/diversi/update_status/' . $diversi->uuid) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>STATUS DIVERSI*</label>
                                    <select class="form-control" name="status_diversi">
                                        <option value="pending">Pilih ...</option>
                                        <?php foreach ($statusdiversi as $key => $value) { ?>
                                            <option <?php echo $diversi->statusdiversi == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                @error('status_diversi')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <button type="submit" class="btn btn-primary" style=""><i class="fa fa-save"></i> Ubah Status</button>
                            </form>
                        </div>
                        @endif
                    </div>

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Informasi</h3>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <ul class="nav nav-pills flex-column">
                                            <li class="nav-item active">
                                                <a href="#detail" data-toggle="tab" class="nav-link">
                                                    <i class="fas fa-info-circle"></i> Detail Diversi
                                                    <span class="badge bg-primary float-right"></span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#berkas" data-toggle="tab" class="nav-link">
                                                    <i class="far fa-file-pdf"></i> Berkas
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-9">
                                <div class="card card-danger card-outline">
                                    <div class="card-body p-0">
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="detail">
                                                <div class="card-body table-responsive" style="margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/diversi/update/' . $diversi->uuid) }}" method="post">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Diversi ID" name="diversiid" value="{{ $diversi->diversiid }}">

                                                            <div class="col-sm-12 row" style="padding: 0 20px">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>TANGGAL REGISTER*</label>

                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                            </div>
                                                                            <input value="{{ $diversi->registerdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_register">
                                                                        </div>
                                                                        <!-- /.input group -->
                                                                    </div>
                                                                    @error('tanggal_register')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label>PENGAJU*</label>
                                                                        <select class="form-control" name="satuan_kerja">
                                                                            <option>Pilih ...</option>
                                                                            <?php foreach ($satuan as $satuan) { ?>
                                                                                <option <?php echo $diversi->satuanid == $satuan->satuanid ? 'selected="selected"' : '' ?> value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    @error('satuan_kerja')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NOMOR REGISTER*</label>
                                                                        <input value="{{ $diversi->nomorregister }}" type="text" class="form-control" placeholder="Nama Tersangka" name="no_register">
                                                                    </div>
                                                                    @error('no_register')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NAMA TERDAKWA / TERSANGKA*</label>
                                                                        <input value="{{ $diversi->namatersangka }}" type="text" class="form-control" placeholder="Nama Terdakwa / Tersangka" name="nama_tersangka">
                                                                    </div>
                                                                    @error('nama_tersangka')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/diversi') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="berkas">
                                                <div class="card-body table-responsive" style="<?php echo substr($diversi->berkas, -4) == '.pdf' ? 'height: 790px;' : '' ?> margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/diversi/reupload_file/' . $diversi->uuid) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Diversi ID" name="diversiid" value="{{ $diversi->diversiid }}">

                                                            @if(substr($diversi->berkas, -4) == '.pdf')
                                                            <div class="card-body table-responsive" style="height: 600px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                                <iframe src="{{ asset('berkas/diversi/' . $diversi->berkas . '#toolbar=0&scrollbar=0') }}" frameBorder="0" scrolling="auto" height="100%" width="100%"></iframe>
                                                            </div>
                                                            @endif

                                                            <div class="col-12 row" style="padding: 0 20px">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">BERKAS* (DOC/DOCX/PDF)</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                                                        </div>
                                                                    </div>
                                                                    @error('berkas')
                                                                    <div style="margin-top: 15px;"></div>
                                                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/diversi') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px"><i class="fa fa-save"></i> Ubah Berkas</button>
                                                                <a href="{{ url('/' . $menu . '/diversi/download/' . $diversi->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                    Berkas Elektronik
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection