@extends('admin.layouts.master')

@section('title', 'Geledah')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Geledah</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $menu == 'kepolisian' ? 'Kepolisian' : 'Pengadilan Negeri' }}</li>
                        <li class="breadcrumb-item active"><a href="{{ url('/' . $menu . '/geledah') }}">Geledah</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-danger row">
                        <div class="col-9">
                            <h5><i class="fas fa-info" style="font-size: 14px; padding: 0 10px 0 0;"></i> Status Geledah:</h5>
                            Proses Geledah atas pemohon: <b>{{ $geledah->satuan->namasatuan }}</b> {{ $geledah->statusgeledah == 'proses' ? 'sedang dalam proses' : ($geledah->statusgeledah == 'terima' ? 'diterima' : 'ditolak') }}
                        </div>
                        @if($session->get('roleid') == 1 || $session->get('roleid') == 2)
                        <div class="col-3 p-0" style="float: right;">
                            <form action="{{ url($menu . '/geledah/update_status/' . $geledah->uuid) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>STATUS GELEDAH*</label>
                                    <select class="form-control" name="status_geledah">
                                        <option disabled>Pilih ...</option>
                                        <?php foreach ($statusgeledah as $key => $value) { ?>
                                            <option <?php echo $geledah->statusgeledah == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                @error('status_geledah')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <button type="submit" class="btn btn-primary" style=""><i class="fa fa-save"></i> Ubah Status</button>
                            </form>
                        </div>
                        @endif
                    </div>

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Informasi</h3>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <ul class="nav nav-pills flex-column">
                                            <li class="nav-item active">
                                                <a href="#detail" data-toggle="tab" class="nav-link">
                                                    <i class="fas fa-info-circle"></i> Detail Geledah
                                                    <span class="badge bg-primary float-right"></span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#berkas" data-toggle="tab" class="nav-link">
                                                    <i class="far fa-file-pdf"></i> Berkas
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-9">
                                <div class="card card-danger card-outline">
                                    <div class="card-body p-0">
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="detail">
                                                <div class="card-body table-responsive" style="margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/geledah/update/' . $geledah->uuid) }}" method="post">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Geledah ID" name="geledahid" value="{{ $geledah->geledahid }}">
                                                            <div class="row col-sm-12">
                                                                <div class="col-6">

                                                                    <div class="form-group">
                                                                        <label>TANGGAL REGISTER*</label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                            </div>
                                                                            <input value="{{ $geledah->registerdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/MM/dd HH:mm:ss" data-mask name="register_date">
                                                                        </div>
                                                                    </div>
                                                                    @error('register_date')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label>PENETAPAN PENOLAKAN IZIN PENGGELEDAHAN*</label>
                                                                        <select class="form-control" name="geledah_type" onchange=toHide(this.value)>
                                                                            <option>Pilih ...</option>
                                                                            <?php foreach ($geledah_type as $key => $value) { ?>
                                                                                <option <?php echo $geledah->geledahtype == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    @error('geledah_type')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label>SATUAN KERJA*</label>
                                                                        <select class="form-control" name="satuan_kerja">
                                                                            <option>Pilih ...</option>
                                                                            <?php foreach ($satuan as $satuan) { ?>
                                                                                <option <?php echo $geledah->satuanid == $satuan->satuanid ? 'selected="selected"' : '' ?> value="{{ $satuan->satuanid}}">{{ $satuan->namasatuan }}</option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    @error('satuan_kerja')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label style="font-size: 11px;">TANGGAL SURAT PERMOHONAN PENYIDIK*</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $geledah->permohonanpenyidikdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask name="permohonan_penyidik_date">
                                                                                </div>
                                                                            </div>
                                                                            @error('permohonan_penyidik_date')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label style="font-size: 11px;">TANGGAL SURAT PERINTAH PENGGELEDAHAN*</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $geledah->perintahgeledahdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask name="perintah_geledah_date">
                                                                                </div>
                                                                            </div>
                                                                            @error('perintah_geledah_date')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>PENGGELEDAHAN TERHADAP*</label>
                                                                                <select class="form-control" name="target_geledah">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($target_geledah_type as $key => $value) { ?>
                                                                                        <option <?php echo $geledah->targetgeledah == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('target_geledah')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div id="isHide">
                                                                                <div class="form-group" style="display: block">
                                                                                    <!-- need to hide based on geledah type -->
                                                                                    <label>TANGGAL LAPORAN PENYIDIK*</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                        </div>
                                                                                        <input value="{{ $geledah->laporanpenyidikdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask name="laporan_penyidik_date">
                                                                                    </div>
                                                                                </div>
                                                                                @error('laporan_penyidik_date')
                                                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                                @enderror
                                                                                <div class="form-group" style="display: block">
                                                                                    <label style="font-size: 11px;">TANGGAL BERITA ACARA PENGGELEDAHAN*</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                        </div>
                                                                                        <input value="{{ $geledah->bageledahdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask name="ba_geledah_date">
                                                                                    </div>
                                                                                </div>
                                                                                @error('ba_geledah_date')
                                                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                                @enderror
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>NO SURAT PERMOHONAN PENYIDIK*</label>
                                                                                <input value="{{ $geledah->nopermohonanpenyidik }}" type="text" class="form-control" name="no_permohonan_penyidik" placeholder="No. Surat Perintah Penggeledahan">
                                                                            </div>
                                                                            @error('no_permohonan_penyidik')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>NO SURAT PERINTAH PENGGELEDAHAN*</label>
                                                                                <input value="{{ $geledah->noperintahgeledah }}" type="text" class="form-control" name="no_perintah_geledah" placeholder="No. Surat Perintah Penggeledahan">
                                                                            </div>
                                                                            @error('no_perintah_geledah')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>LOKASI*</label>
                                                                                <input value="{{ $geledah->lokasi }}" type="text" class="form-control" name="lokasi" placeholder="Lokasi">
                                                                            </div>
                                                                            @error('lokasi')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div id="isHide2">
                                                                                <div class="form-group" style="display: block">
                                                                                    <label>NO SURAT LAPORAN PENYIDIK*</label>
                                                                                    <input value="{{ $geledah->nolaporanpenyidik }}" type="text" class="form-control" name="no_laporan_penyidik" placeholder="No. Surat Laporan Penyidik">
                                                                                </div>
                                                                                @error('no_laporan_penyidik')
                                                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                                @enderror
                                                                                <div class="form-group" style="display: block">
                                                                                    <label>NO BERITA ACARA PENGGELEDAHAN*</label>
                                                                                    <input value="{{ $geledah->nobageledah }}" type="text" class="form-control" name="no_ba_geledah" placeholder="No. Berita Acara Penggeledahan">
                                                                                </div>
                                                                                @error('no_ba_geledah')
                                                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                                @enderror
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6" style="">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>JENIS PIHAK*</label>
                                                                                <select class="form-control" name="jenis_pihak">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($pihak_type as $key => $value) { ?>
                                                                                        <option <?php echo $geledah->pihakgeledah == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('jenis_pihak')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>NAMA PIHAK*</label>
                                                                                <input value="{{ $geledah->namapihak }}" type="text" class="form-control" name="nama_pihak" placeholder="Nama Pihak">
                                                                            </div>
                                                                            @error('nama_pihak')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>TEMPAT TINGGAL*</label>
                                                                        <input value="{{ $geledah->alamatpihak }}" type="text" class="form-control" name="alamat" placeholder="Tempat tinggal">
                                                                    </div>
                                                                    @error('alamat')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TEMPAT LAHIR*</label>
                                                                                <input value="{{ $geledah->pihakbirthplace }}" type="text" class="form-control" name="birthplace" placeholder="Tempat lahir">
                                                                            </div>
                                                                            @error('birthplace')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>JENIS KELAMIN*</label>
                                                                                <select class="form-control" name="sex_type">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($gender as $key => $value) { ?>
                                                                                        <option <?php echo $geledah->sextype == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('sex_type')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>KEBANGSAAN*</label>
                                                                                <input value="{{ $geledah->nation }}" type="text" class="form-control" name="nation" placeholder="Kebangsaan">
                                                                            </div>
                                                                            @error('nation')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TANGGAL LAHIR*</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $geledah->pihakbirthdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy/mm/dd" data-mask name="birthdate">
                                                                                </div>
                                                                            </div>
                                                                            @error('birthdate')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>AGAMA*</label>
                                                                                <select class="form-control" name="religion">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($agama as $key => $value) { ?>
                                                                                        <option <?php echo $geledah->religion == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('religion')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                            <div class="form-group">
                                                                                <label>PEKERJAAN*</label>
                                                                                <input value="{{ $geledah->occupation }}" type="text" class="form-control" name="occupation" placeholder="Pekerjaan">
                                                                            </div>
                                                                            @error('occupation')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/geledah') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="berkas">
                                                <div class="card-body table-responsive" style="<?php echo substr($geledah->berkasurl, -4) == '.pdf' ? 'height: 790px;' : '' ?> margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/geledah/reupload_file/' . $geledah->uuid) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Geledah ID" name="geledahid" value="{{ $geledah->geledahid }}">

                                                            @if($geledah->berkasurl != null)
                                                            @if(substr($geledah->berkasurl, -4) == '.pdf')
                                                            <div class="card-body table-responsive" style="height: 600px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                                <iframe src="{{ asset('berkas/geledah/' . $geledah->berkasurl . '#toolbar=0&scrollbar=0') }}" frameBorder="0" scrolling="auto" height="100%" width="100%"></iframe>
                                                            </div>
                                                            @endif
                                                            @endif

                                                            <div class="col-12 row" style="padding: 0 20px">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">BERKAS* (PDF/DOCX)</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                                                        </div>
                                                                    </div>
                                                                    @error('berkas')
                                                                    <div style="margin-top: 15px;"></div>
                                                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/geledah') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px"><i class="fa fa-save"></i> Ubah Berkas</button>
                                                                <a href="{{ url('/' . $menu . '/geledah/download/' . $geledah->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                    Berkas Elektronik
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    setTimeout(function() {
        $(document).ready(function() {
            var location = window.location.href;
            if (location.includes('geledah/detail') || location.includes('sita/detail')) {
                console.log('test url');

                if (location.includes('geledah/detail')) {
                    var $type = '<?php echo $geledah->geledahtype ?>';

                    if ($type == 'penetapan_izin_penyitaan' || $type == 'penetapan_izin_penggeledahan') {
                        document.getElementById('isHide').style.display = 'none';
                        document.getElementById('isHide2').style.display = 'none';
                    } else {
                        document.getElementById('isHide').style.display = 'block';
                        document.getElementById('isHide2').style.display = 'block';
                    }
                }
            }
        });
    }, 500);
</script>
@endsection