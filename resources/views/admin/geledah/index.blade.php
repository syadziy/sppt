@extends('admin.layouts.master')

@section('title', 'Geledah')

@section('content')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
function hari_indo($hari)
{
    switch ($hari) {
        case "Monday":
            $hari = "Senin";
            break;
        case "Tuesday":
            $hari = "Selasa";
            break;
        case "Wednesday":
            $hari = "Rabu";
            break;
        case "Thursday":
            $hari = "Kamis";
            break;
        case "Friday":
            $hari = "Jum'at";
            break;
        case "Saturday":
            $hari = "Sabtu";
            break;
        case "Sunday":
            $hari = "Minggu";
            break;
    }

    return $hari;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $menu == 'kepolisian' ? 'Kepolisian' : ($menu == 'kejaksaan' ? 'Kejaksaan' : 'Pengadilan Negeri') }} - Geledah</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $menu == 'kepolisian' ? 'Kepolisian' : ($menu == 'kejaksaan' ? 'Kejaksaan' : 'Pengadilan Negeri') }}</li>
                        <li class="breadcrumb-item active">Geledah</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Tambah</button>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-success btn-block"><i class="fa fa-download"></i> Unduh</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Penggeledahan</h3>

                            <!-- <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="card-tools" style="padding: 0 20px">
                                <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive" style="height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                            <table class="table table-head-fixed text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Tanggal Register</th>
                                        <th>Jenis Geledah</th>
                                        <th>No. Surat Permohonan</th>
                                        <th>Pemohon</th>
                                        <th>Status</th>
                                        <th style="width: 50px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($geledah as $u) { ?>
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                {{
                                                    hari_indo(date( "l", strtotime($u->registerdate))) 
                                                    . ', ' . 
                                                    tgl_indo(date( "Y-m-d", strtotime($u->registerdate)))
                                                }}
                                            </td>
                                            <td>{{
                                                    $func->getValueGeledah($u->geledahtype)
                                                }}
                                            </td>
                                            <td>{{ $u->nopermohonanpenyidik }}</td>
                                            <td>{{ $u->pemohon->fullname}}</td>
                                            <td>
                                                <div>
                                                    @if($u->statusgeledah == 'proses')
                                                    <a class="btn bg-secondary" data-toggle="tooltip" title="Menunggu..">
                                                        <i class="fa fa-spinner"></i>
                                                    </a>
                                                    @elseif($u->statusgeledah == 'terima')
                                                    <a class="btn bg-success" data-toggle="tooltip" title="Diterima !">
                                                        <i class="fa fa-vote-yea"></i>
                                                    </a>
                                                    @elseif($u->statusgeledah == 'tolak')
                                                    <a class="btn bg-danger" data-toggle="tooltip" title="Ditolak !">
                                                        <i class="fa fa-window-close"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <a class="btn bg-warning" href="{{ url('/' . $menu . '/geledah/detail/' . $u->uuid) }}">
                                                        <i class="fas fa-search"></i>
                                                    </a>
                                                    <a class="btn bg-danger" data-toggle="modal" data-target="#modal-sm" onclick="geledahDelete('{{ $u->uuid }}', '{{ $u->namapihak }}')">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <br>
                            <p style="padding: 0 5px; margin: 0">Halaman : {{ $geledah->currentPage() }}</p>
                            <p style="padding: 0 5px; margin: 0">Jumlah Data : {{ $geledah->total() }}</p>
                            <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $geledah->perPage() }}</p>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $geledah->links() }}
                            </ul>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- /.modal -->
<div class="modal fade <?php echo count($errors) > 0 ? 'show' : '' ?>" id="modal-lg" style="display: <?php echo count($errors) > 0 ? 'block' : 'none' ?>">
    <div class="modal-dialog modal-xl">
        <form action="{{ url($menu . '/geledah/store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Geledah</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>TANGGAL REGISTER*</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="register_date">
                                        </div>
                                    </div>
                                    @error('register_date')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label style="font-size: 12px;">PENETAPAN PENOLAKAN IZIN PENGGELEDAHAN*</label>
                                        <select class="form-control" name="geledah_type" onchange=toHide(this.value)>
                                            <option>Pilih ...</option>
                                            <?php foreach ($geledah_type as $key => $value) { ?>
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    @error('geledah_type')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label>SATUAN KERJA*</label>
                                <select class="form-control" name="satuan_kerja">
                                    <option>Pilih ...</option>
                                    <?php foreach ($satuan as $satuan) { ?>
                                        <option value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                            @error('satuan_kerja')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>TANGGAL SURAT PERMOHONAN PENYIDIK*</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="permohonan_penyidik_date">
                                        </div>
                                    </div>
                                    @error('permohonan_penyidik_date')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label style="font-size: 12px;">TANGGAL SURAT PERINTAH PENGGELEDAHAN*</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="perintah_geledah_date">
                                        </div>
                                    </div>
                                    @error('perintah_geledah_date')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>PENGGELEDAHAN TERHADAP*</label>
                                        <select class="form-control" name="target_geledah">
                                            <option>Pilih ...</option>
                                            <?php foreach ($target_geledah_type as $key => $value) { ?>
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    @error('target_geledah')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div id="isHide">
                                        <div class="form-group">
                                            <label>TANGGAL LAPORAN PENYIDIK*</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="laporan_penyidik_date">
                                            </div>
                                        </div>
                                        @error('laporan_penyidik_date')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                        <div class="form-group">
                                            <label>TANGGAL BERITA ACARA PENGGELEDAHAN*</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="ba_geledah_date">
                                            </div>
                                        </div>
                                        @error('ba_geledah_date')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NO SURAT PERMOHONAN PENYIDIK*</label>
                                        <input type="text" class="form-control" name="no_permohonan_penyidik" placeholder="No. Surat Permohonan Penyidik">
                                    </div>
                                    @error('no_permohonan_penyidik')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>NO SURAT PERINTAH PENGGELEDAHAN*</label>
                                        <input type="text" class="form-control" name="no_perintah_geledah" placeholder="No. Surat Perintah Penggeledahan">
                                    </div>
                                    @error('no_perintah_geledah')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>LOKASI*</label>
                                        <input type="text" class="form-control" name="lokasi" placeholder="Lokasi">
                                    </div>
                                    @error('lokasi')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div id="isHide2">
                                        <div class="form-group">
                                            <label>NO SURAT LAPORAN PENYIDIK*</label>
                                            <input type="text" class="form-control" name="no_laporan_penyidik" placeholder="No. Surat Laporan Penyidik">
                                        </div>
                                        @error('no_laporan_penyidik')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                        <div class="form-group">
                                            <label>NO BERITA ACARA PENGGELEDAHAN*</label>
                                            <input type="text" class="form-control" name="no_ba_geledah" placeholder="No. Berita Acara Penggeledahan">
                                        </div>
                                        @error('no_ba_geledah')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>JENIS PIHAK*</label>
                                        <select class="form-control" name="jenis_pihak">
                                            <option>Pilih ...</option>
                                            <?php foreach ($pihak_type as $key => $value) { ?>
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    @error('jenis_pihak')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NAMA PIHAK*</label>
                                        <input type="text" class="form-control" name="nama_pihak" placeholder="Nama Pihak">
                                    </div>
                                    @error('nama_pihak')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label>TEMPAT TINGGAL*</label>
                                <input type="text" class="form-control" name="alamat" placeholder="Tempat Tinggal">
                            </div>
                            @error('alamat')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>TEMPAT LAHIR*</label>
                                        <input type="text" class="form-control" name="birthplace" placeholder="Tempat Lahir">
                                    </div>
                                    @error('birthplace')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>JENIS KELAMIN*</label>
                                        <select class="form-control" name="sex_type">
                                            <option>Pilih ...</option>
                                            <?php foreach ($gender as $key => $value) { ?>
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    @error('sex_type')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>KEBANGSAAN*</label>
                                        <input type="text" class="form-control" name="nation" placeholder="Kebangsaan">
                                    </div>
                                    @error('nation')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>TANGGAL LAHIR*</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="birthdate">
                                        </div>
                                    </div>
                                    @error('birthdate')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>AGAMA*</label>
                                        <select class="form-control" name="religion">
                                            <option>Pilih ...</option>
                                            <?php foreach ($agama as $key => $value) { ?>
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    @error('religion')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                    <div class="form-group">
                                        <label>PEKERJAAN*</label>
                                        <input type="text" class="form-control" name="occupation" placeholder="Pekerjaan">
                                    </div>
                                    @error('occupation')
                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">BERKAS* (PDF/DOCX)</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                        <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                    </div>
                                </div>
                            </div>
                            @error('berkas')
                            <div style="margin-top: 15px;"></div>
                            <sup style="padding: 10px; color: red">{{ $message }}</sup>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal-delete -->
<div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <form action="{{ url($menu . '/geledah/delete') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Penggeledahan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12" style="padding: 0 20px">
                            <div class="form-group">
                                <p class="text-center">Apakah anda yakin ingin menghapus data geledah atas nama</p>
                                <b>
                                    <p id="namaDeleted" class="text-center" style="font-size: 14px;"></p>
                                </b>
                                <input type="hidden" id="uuidDeleted" name="uuid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection