@extends('admin.layouts.master')

@section('title', 'Manajemen User')

@section('content')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
function hari_indo($hari)
{
    switch ($hari) {
        case "Monday":
            $hari = "Senin";
            break;
        case "Tuesday":
            $hari = "Selasa";
            break;
        case "Wednesday":
            $hari = "Rabu";
            break;
        case "Thursday":
            $hari = "Kamis";
            break;
        case "Friday":
            $hari = "Jum'at";
            break;
        case "Saturday":
            $hari = "Sabtu";
            break;
        case "Sunday":
            $hari = "Minggu";
            break;
    }

    return $hari;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Semua Pesan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Inbox</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" data-toggle="modal" data-target="#modal-lg">Buat Pesan</a>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Folders</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item active">
                                <a href="#inbox" data-toggle="tab" class="nav-link">
                                    <i class="fas fa-inbox"></i> Inbox
                                    <span class="badge bg-primary float-right">{{ count($notread) }}</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#sent" data-toggle="tab" class="nav-link">
                                    <i class="far fa-envelope"></i> Sent
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#trash" data-toggle="tab" class="nav-link">
                                    <i class="far fa-trash-alt"></i> Trash
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Labels</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-circle text-danger"></i>
                                    Important
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-circle text-warning"></i> Promotions
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="far fa-circle text-primary"></i>
                                    Social
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="inbox">
                                <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                    <table class="table table-head-fixed text-nowrap table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Pengirim</th>
                                                <th>Email</th>
                                                <th>Judul</th>
                                                <th>Status</th>
                                                <th style="width: 50px">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($inbox) > 0)
                                            <?php $no = 1; ?>
                                            <?php foreach ($inbox as $i) { ?>
                                                <tr {{ $i->isread == 1 ? 'style="font-weight: bold"' : '' }}>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $i->userSender == null ? 'Deleted User' : $i->userSender->fullname }}</td>
                                                    <td>{{ $i->userSender == null ? 'Deleted User' : $i->userSender->email }}</td>
                                                    <td>{{ $i->judul }}</td>
                                                    <td>
                                                        @if($i->isread == 1)
                                                        <i class="fa fa-envelope"></i> Belum dibaca
                                                        @else
                                                        <i class="fa fa-envelope-open-text"></i> Sudah dibaca
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <?php
                                                            $sender = $i->userSender == null ? 'Deleted User' : $i->userSender->fullname . ' (' . $i->userSender->email . ')';
                                                            $receiver = $i->userReceiver == null ? 'Deleted User' : $i->userReceiver->fullname . ' (' . $i->userReceiver->email . ')';
                                                            $formatedTime = hari_indo(date("l", strtotime($i->createdat))) . ', ' . tgl_indo(date("Y-m-d", strtotime($i->createdat)))
                                                            ?>
                                                            <a class="btn bg-warning" data-toggle="modal" data-target="#modal-sm-read" onclick="readInbox('{{ $i->uuid }}', '{{ $i->judul }}', '{{ $i->isi }}', '{{ $sender }}', '{{ $receiver }}', '{{ $formatedTime }}')">
                                                                <i class="fas fa-search"></i>
                                                            </a>
                                                            <a class="btn bg-danger" data-toggle="modal" data-target="#modal-sm" onclick="inboxDelete('{{ $i->uuid }}', '{{ $i->userSender == null ? null : $i->userSender->email }}', 'inbox')">
                                                                <i class="fas fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            @else
                                            <tr>
                                                <td colspan="6"></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                    <br>
                                    <p style="padding: 0 5px; margin: 0">Halaman : {{ $inbox->currentPage() }}</p>
                                    <p style="padding: 0 5px; margin: 0">Jumlah Data : {{ $inbox->total() }}</p>
                                    <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $inbox->perPage() }}</p>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        {{ $inbox->links() }}
                                    </ul>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="sent">
                                <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                    <table class="table table-head-fixed text-nowrap table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Nama Penerima</th>
                                                <th>Email</th>
                                                <th>Judul</th>
                                                <th>Status</th>
                                                <th style="width: 50px">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($sent) > 0)
                                            <?php $no = 1; ?>
                                            <?php foreach ($sent as $s) { ?>
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $s->userReceiver == null ? 'Deleted User' : $s->userReceiver->fullname }}</td>
                                                    <td>{{ $s->userReceiver == null ? 'Deleted User' : $s->userReceiver->email }}</td>
                                                    <td>{{ $s->judul }}</td>
                                                    <td>
                                                        <i class="fa fa-file-import"></i> Pesan Terkirim
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <?php
                                                            $sender = $s->userSender == null ? 'Deleted User' : $s->userSender->fullname . ' (' . $s->userSender->email . ')';
                                                            $receiver = $s->userReceiver == null ? 'Deleted User' : $s->userReceiver->fullname . ' (' . $s->userReceiver->email . ')';
                                                            $formatedTime = hari_indo(date("l", strtotime($s->createdat))) . ', ' . tgl_indo(date("Y-m-d", strtotime($s->createdat)))
                                                            ?>
                                                            <a class="btn bg-warning" data-toggle="modal" data-target="#modal-sm-read" onclick="readInbox('{{ $s->uuid }}', '{{ $s->judul }}', '{{ $s->isi }}', '{{ $sender }}', '{{ $receiver }}', '{{ $formatedTime }}')">
                                                                <i class="fas fa-search"></i>
                                                            </a>
                                                            <a class="btn bg-danger" data-toggle="modal" data-target="#modal-sm" onclick="inboxDelete('{{ $s->uuid }}', '{{ $s->userReceiver == null ? null : $s->userReceiver->email }}', 'sent')">
                                                                <i class="fas fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            @else
                                            <tr>
                                                <td colspan="6"></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                    <br>
                                    <p style="padding: 0 5px; margin: 0">Halaman : {{ $sent->currentPage() }}</p>
                                    <p style="padding: 0 5px; margin: 0">Jumlah Data : {{ $sent->total() }}</p>
                                    <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $sent->perPage() }}</p>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        {{ $sent->links() }}
                                    </ul>
                                </div>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="trash">
                                <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                    <table class="table table-head-fixed text-nowrap table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Type</th>
                                                <th>Pengirim/Penerima</th>
                                                <th>Judul</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($trash) > 0)
                                            <?php $no = 1; ?>
                                            <?php foreach ($trash as $t) { ?>
                                                @if(($t->statussender == 0 && $t->sender == $session->get('userid')) || ($t->statusreceiver == 0 && $t->receiver == $session->get('userid')))
                                                <tr style="color: red">
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $t->sender == $session->get('userid') ? 'Sent' : 'Inbox' }}</td>
                                                    <td>{{ $t->userSender == null ? 'Deleted User' : $t->userSender->email }}</td>
                                                    <td>{{ $t->judul }}</td>
                                                    <td>
                                                        <i class="fa fa-dumpster-fire"></i> Dihapus
                                                    </td>
                                                </tr>
                                                @endif
                                            <?php } ?>
                                            @else
                                            <tr>
                                                <td colspan="6"></td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>

                                    <br>
                                    <p style="padding: 0 5px; margin: 0">Halaman : {{ $trash->currentPage() }}</p>
                                    <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $trash->perPage() }}</p>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        {{ $trash->links() }}
                                    </ul>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- /.modal -->
<div class="modal fade <?php echo count($errors) > 0 ? 'show' : '' ?>" id="modal-lg" style="display: <?php echo count($errors) > 0 ? 'block' : 'none' ?>">
    <div class="modal-dialog modal-lg" style="width: 30%;">
        <form action="{{ url('inbox/send') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kirim Pesan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12" style="padding: 0 20px">
                            <div class="form-group">
                                <label class="col-form-label"><i class="far fa-edit"></i> PENGIRIM*</label>
                                <input type="text" class="form-control" placeholder="Pengirim" name="pengirim" value="{{ $session->get('fullname') . ' (' . $session->get('email') . ')' }}" readonly>
                            </div>
                            @error('pengirim')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror

                            <div class="form-group">
                                <label><i class="far fa-edit"></i> PENERIMA*</label>
                                <select class="form-control" name="penerima">
                                    <option>Pilih ...</option>
                                    <?php foreach ($user as $user) { ?>
                                        @if($user->userid != $session->get('userid'))
                                        <option value="{{ $user->userid }}">{{ $user->fullname . ($user->satuanid == null ? '' : ' - ' . $user->satuan->namasatuan) . ' (' . $user->email . ')' }}</option>
                                        @endif
                                    <?php } ?>
                                </select>
                            </div>
                            @error('penerima')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror

                            <div class="form-group">
                                <label class="col-form-label"><i class="far fa-edit"></i> JUDUL*</label>
                                <input type="text" class="form-control" placeholder="Judul" name="judul" maxlength="20">
                            </div>
                            @error('judul')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror

                            <div class="form-group">
                                <label><i class="far fa-edit"></i> ISI PESAN*</label>
                                <textarea class="form-control" rows="3" placeholder="Isi Pesan ..." name="isi"></textarea>
                            </div>
                            @error('isi')
                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                            @enderror

                        </div>
                    </div>
                </div>
                <div class="modal-footer pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-share-square"></i> Kirim</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <form action="{{ url('inbox/delete') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Pesan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12" style="padding: 0 20px">
                            <div class="form-group">
                                <p class="text-center">Apakah anda yakin ingin menghapus pesan dari email di bawah?</p>
                                <b>
                                    <p id="emailDeleted" class="text-center" style="font-size: 14px;"></p>
                                </b>
                                <input type="hidden" id="uuidDeleted" name="uuid">
                                <input type="hidden" id="typeDeleted" name="type">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade" id="modal-sm-read">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Pesan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row col-sm-12">
                    <div class="col-sm-12" style="padding: 0">
                        <div class="form-group">
                            <p id="judul" style="font-size: 16px; font-weight: 500; margin: 0 0 5px 0; font-weight: bold;"></p>
                            <sup id="sender"></sup><br>
                            <sup id="receiver"></sup><br>
                            <hr style="margin: 0 0 10px 0;">
                            <p id="isi"></p>
                            <sup id="time" style="font-style: italic;"></sup>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload()"><i class="fa fa-angle-left"></i> Kembali</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection