@extends('admin.layouts.master')

@section('title', 'Izin Besuk')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Izin Besuk</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $menu == 'pengadilan_negeri' ? 'Pengadilan Negeri' : 'Lapas'}}</li>
                        <li class="breadcrumb-item active"><a href="{{ url('/' . $menu . '/izinbesuk') }}">Izin Besuk</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Informasi</h3>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <ul class="nav nav-pills flex-column">
                                            <li class="nav-item active">
                                                <a href="#detail" data-toggle="tab" class="nav-link">
                                                    <i class="fas fa-info-circle"></i> Detail Izin Besuk
                                                    <span class="badge bg-primary float-right"></span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#berkaspermohonan" data-toggle="tab" class="nav-link">
                                                    <i class="far fa-file-pdf"></i> Surat Permohonan Izin Besuk
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#berkasizin" data-toggle="tab" class="nav-link">
                                                    <i class="far fa-file-pdf"></i> Surat Izin Mengunjungi Tahanan
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#penerbitan" data-toggle="tab" class="nav-link">
                                                    <i class="fa fa-print"></i> Penerbitan
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-9">
                                <div class="card card-danger card-outline">
                                    <div class="card-body p-0">
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="detail">
                                                <div class="card-body table-responsive" style="margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/izin_besuk/update/' . $izinbesuk->uuid) }}" method="post">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Izin Besuk ID" name="izinbesukid" value="{{ $izinbesuk->izinbesukid }}">

                                                            <div class="col-sm-12 row" style="padding: 0 20px">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NAMA PEMOHON*</label>
                                                                        <input value="{{ $izinbesuk->namapemohon }}" type="text" class="form-control" placeholder="Nama Pemohon" name="nama_pemohon">
                                                                    </div>
                                                                    @error('nama_pemohon')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row">
                                                                        <div class="col-6">
                                                                            <div class="form-group" style="margin-bottom: 0;">
                                                                                <label>JENIS KELAMIN*</label>
                                                                                <select class="form-control" name="jenis_kelamin">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($gender as $key => $value) { ?>
                                                                                        <option <?php echo $izinbesuk->jeniskelamin == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('jenis_kelamin')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group" style="margin-bottom: 0;">
                                                                                <label>AGAMA*</label>
                                                                                <select class="form-control" name="agama">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($religion as $key => $value) { ?>
                                                                                        <option <?php echo $izinbesuk->agama == $key ? 'selected="selected"' : '' ?>value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('agama')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> TEMPAT LAHIR*</label>
                                                                                <input value="{{ $izinbesuk->tempatlahir }}" type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir">
                                                                            </div>
                                                                            @error('tempat_lahir')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TANGGAL LAHIR*</label>

                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $izinbesuk->tanggallahir }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_lahir">
                                                                                </div>
                                                                                <!-- /.input group -->
                                                                            </div>
                                                                            @error('tanggal_lahir')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> KEBANGSAAN*</label>
                                                                                <input value="{{ $izinbesuk->kebangsaan }}" type="text" class="form-control" placeholder="Kebangsaan" name="kebangsaan">
                                                                            </div>
                                                                            @error('kebangsaan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> PEKERJAAN*</label>
                                                                                <input value="{{ $izinbesuk->pekerjaan }}" type="text" class="form-control" placeholder="Pekerjaan" name="pekerjaan">
                                                                            </div>
                                                                            @error('pekerjaan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> HUBUNGAN KELUARGA*</label>
                                                                        <input value="{{ $izinbesuk->hubungankeluarga }}" type="text" class="form-control" placeholder="Hubungan Keluarga" name="hubungan_keluarga">
                                                                    </div>
                                                                    @error('hubungan_keluarga')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> Alamat Pemohon*</label>
                                                                        <input value="{{ $izinbesuk->alamatpemohon }}" type="text" class="form-control" placeholder="Alamat Pemohon" name="alamat_pemohon">
                                                                    </div>
                                                                    @error('alamat_pemohon')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> PENGIKUT BESUK*</label>
                                                                        <input value="{{ $izinbesuk->pengikutbesuk }}" type="text" class="form-control" placeholder="Pengikut Besuk" name="pengikut_besuk">
                                                                    </div>
                                                                    @error('pengikut_besuk')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label>TANGGAL SURAT IZIN BESUK*</label>

                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                            </div>
                                                                            <input value="{{ $izinbesuk->tanggalsuratizinbesuk }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_surat_izin_besuk">
                                                                        </div>
                                                                        <!-- /.input group -->
                                                                    </div>
                                                                    @error('tanggal_surat_izin_besuk')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> HAKIM YANG MEMBERIKAN IZIN*</label>
                                                                        <input value="{{ $izinbesuk->hakim }}" type="text" class="form-control" placeholder="Nama Hakim" name="hakim">
                                                                    </div>
                                                                    @error('hakim')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NOMOR PERKARA*</label>
                                                                        <input value="{{ $izinbesuk->nomorperkara }}" type="text" class="form-control" placeholder="Nomor Perkara" name="no_perkara">
                                                                    </div>
                                                                    @error('no_perkara')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> NAMA TAHANAN / TERDAKWA*</label>
                                                                                <input value="{{ $izinbesuk->namatersangka }}" type="text" class="form-control" placeholder="Nama Tahanan / Terdakwa" name="nama_tersangka">
                                                                            </div>
                                                                            @error('nama_tersangka')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TANGGAL BERLAKU SURAT IZIN BESUK*</label>

                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $izinbesuk->tanggalberlaku }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_berlaku">
                                                                                </div>
                                                                                <!-- /.input group -->
                                                                            </div>
                                                                            @error('tanggal_berlaku')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/izin_besuk') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            @if($session->get('roleid') != 5)
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="berkaspermohonan">
                                                <div class="card-body table-responsive" style="<?php echo substr($izinbesuk->berkaspermohonan, -4) == '.pdf' ? 'height: 790px;' : '' ?> margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/izin_besuk/reupload_file_permohonan/' . $izinbesuk->uuid) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Izin Besuk ID" name="izinbesukid" value="{{ $izinbesuk->izinbesukid }}">

                                                            @if($izinbesuk->berkaspermohonan != null)
                                                            @if(substr($izinbesuk->berkaspermohonan, -4) == '.pdf')
                                                            <div class="card-body table-responsive" style="height: 600px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                                <iframe src="{{ asset('berkas/permohonan/' . $izinbesuk->berkaspermohonan . '#toolbar=0&scrollbar=0') }}" frameBorder="0" scrolling="auto" height="100%" width="100%"></iframe>
                                                            </div>
                                                            @else
                                                            <div class="col-12" style="display: flex; justify-content: center; padding: 50px">
                                                                <img style="width: 200px; height: 200px" src="{{ asset('berkas/permohonan/' . $izinbesuk->berkaspermohonan) }}">
                                                            </div>
                                                            @endif
                                                            @endif

                                                            <div class="col-12 row" style="padding: 0 20px">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">BERKAS SURAT PERMOHONAN IZIN BESUK* (JPG/PNG/PDF)</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                                                        </div>
                                                                    </div>
                                                                    @error('berkas')
                                                                    <div style="margin-top: 15px;"></div>
                                                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/izin_besuk') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                @if($session->get('roleid') != 5)
                                                                <button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px"><i class="fa fa-save"></i> Ubah Berkas</button>
                                                                @endif
                                                                <a href="{{ url('/' . $menu . '/izin_besuk/download_permohonan/' . $izinbesuk->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                    Berkas Elektronik
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="berkasizin">
                                                <div class="card-body table-responsive" style="<?php echo substr($izinbesuk->berkasizin, -4) == '.pdf' ? 'height: 790px;' : '' ?> margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/izin_besuk/reupload_file_izin/' . $izinbesuk->uuid) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Izin Besuk ID" name="izinbesukid" value="{{ $izinbesuk->izinbesukid }}">

                                                            @if($izinbesuk->berkasizin != null)
                                                            @if(substr($izinbesuk->berkasizin, -4) == '.pdf')
                                                            <div class="card-body table-responsive" style="height: 600px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                                <iframe src="{{ asset('berkas/izin/' . $izinbesuk->berkasizin . '#toolbar=0&scrollbar=0') }}" frameBorder="0" scrolling="auto" height="100%" width="100%"></iframe>
                                                            </div>
                                                            @else
                                                            <div class="col-12" style="display: flex; justify-content: center; padding: 50px">
                                                                <img style="width: 200px; height: 200px" src="{{ asset('berkas/izin/' . $izinbesuk->berkasizin) }}">
                                                            </div>
                                                            @endif
                                                            @endif

                                                            <div class="col-12 row" style="padding: 0 20px">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">BERKAS SURAT IZIN MENGUNJUNGI TAHANAN* (JPG/PNG/PDF)</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                                                        </div>
                                                                    </div>
                                                                    @error('berkas')
                                                                    <div style="margin-top: 15px;"></div>
                                                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/izin_besuk') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                @if($session->get('roleid') != 5)
                                                                <button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px"><i class="fa fa-save"></i> Ubah Berkas</button>
                                                                @endif
                                                                <a href="{{ url('/' . $menu . '/izin_besuk/download_izin/' . $izinbesuk->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                    Berkas Elektronik
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="penerbitan">
                                                <div class="card-body table-responsive" style="margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <div class="col-12 row" style="padding: 30px 20px">
                                                        <i class="fa fa-file-pdf" style="font-size: 60px;"> <span style="font-size: 20px;">Surat Izin Mengunjungi Tahanan.pdf</span></i>
                                                    </div>
                                                    <hr>
                                                    <div class="col-12 row">
                                                        <div class="col-6">
                                                            <a href="{{ url('/' . $menu . '/izin_besuk') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                        </div>
                                                        <div class="col-6">
                                                            <a href="{{ url('/' . $menu . '/izin_besuk/generate_surat/' . $izinbesuk->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                Terbitkan
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection