<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 2.0.0
    </div>
    <p>Copyright &copy; 2021 <a href="https://adminlte.io">SPPT.io</a>.</p>
</footer>