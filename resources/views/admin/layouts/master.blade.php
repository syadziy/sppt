<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SPPT-v2 | @yield('title')</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('lte/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('lte/plugins/toastr/toastr.min.css') }}">

  <style>
    body {
      font-size: small;
    }

    button {
      font-size: small;
    }

    .btn {
      font-size: small;
    }

    .sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active,
    .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
      background-color: #282728;
    }

    .page-item.active .page-link {
      background-color: #3d9970;
      border-color: #3d9970;
    }

    .page-link {
      color: #3d9970;
    }
  </style>
  </style>
</head>

<body class="hold-transition sidebar-mini layout-navbar-fixed">
  <div class="wrapper">
    <!-- HEADER -->
    @include('admin.layouts.header')

    <!-- SIDEBAR -->
    @include('admin.layouts.sidebar')

    @yield('content')

    <!-- FOOTER -->
    @include('admin.layouts.footer')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <script src="{{ asset('lte/plugins/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('lte/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('lte/dist/js/demo.js') }}"></script>
  <script src="{{ asset('lte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('lte/plugins/toastr/toastr.min.js') }}"></script>
  <script>
    $(function() {
      bsCustomFileInput.init();
    });
  </script>
  <script>
    $(function() {
      var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      let errorMessage = '<?php echo session()->get('error') ?>';
      let successMessage = '<?php echo session()->get('success') ?>';

      if (errorMessage != '') {
        Toast.fire({
          icon: 'error',
          title: errorMessage
        });
      }

      if (successMessage != '') {
        Toast.fire({
          icon: 'success',
          title: successMessage
        });
      }
    });

    function reload() {
      location.reload(true);
    }

    function readInbox($uuid, $judul, $isi, $sender, $receiver, $created) {
      $.ajax({
        type: "get",
        url: "{{ url('/inbox/read') }}" + "/" + $uuid,
        success: function(response) {
          //service.php response
          console.log(response);
          $('#judul').text($judul.toUpperCase())
          $('#sender').text('from: ' + $sender)
          $('#receiver').text('to: ' + $receiver)
          $('#isi').text($isi)
          $('#time').text($created)
        }
      });
    }

    function userDelete($uuid, $email) {
      $('#emailDeleted').text($email)
      $('#uuidDeleted').val($uuid)
    }

    function penahananDelete($uuid, $tersangka) {
      $('#tersangkaDeleted').text($tersangka)
      $('#uuidDeleted').val($uuid)
    }

    function tilangDelete($uuid, $penindak) {
      $('#penindakDeleted').text($penindak)
      $('#uuidDeleted').val($uuid)
    }

    function tipiringDelete($uuid, $tersangka) {
      $('#tersangkaDeleted').text($tersangka)
      $('#uuidDeleted').val($uuid)
    }

    function diversiDelete($uuid, $tersangka) {
      $('#tersangkaDeleted').text($tersangka)
      $('#uuidDeleted').val($uuid)
    }

    function izinbesukDelete($uuid, $tersangka) {
      $('#tersangkaDeleted').text($tersangka)
      $('#uuidDeleted').val($uuid)
    }

    function inboxDelete($uuid, $email, $type) {
      $('#emailDeleted').text($email == null ? 'User Deleted' : $email)
      $('#uuidDeleted').val($uuid)
      $('#typeDeleted').val($type)
    }

    function sitaDelete($uuid, $nama) {
      $('#namaDeleted').text($nama)
      $('#uuidDeleted').val($uuid)
    }

    function geledahDelete($uuid, $nama) {
      $('#namaDeleted').text($nama)
      $('#uuidDeleted').val($uuid)
    }

    function toHide($in) {
      if ($in == 'penetapan_izin_penyitaan' || $in == 'penetapan_izin_penggeledahan') {
        document.getElementById('isHide').style.display = 'none';
        document.getElementById('isHide2').style.display = 'none';
      } else {
        document.getElementById('isHide').style.display = 'block';
        document.getElementById('isHide2').style.display = 'block';
      }

    }
  </script>
</body>

</html>