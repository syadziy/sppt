<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link elevation-4 bg-olive">
        <img style="width: 35px; height: 35px" src="{{ $setting == null ? asset('lte/dist/img/AdminLTELogo.png') : ($setting->logo == null ? asset('lte/dist/img/AdminLTELogo.png') : asset('logo/' . $setting->logo)) }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">SPPT - V.2</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column">
                <li class="nav-header">PROFILE</li>
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img style="width: 35px; height: 35px" src="{{ $session->get('photo') == null ? asset('lte/dist/img/user1-128x128.jpg') : asset('img/' . $session->get('photo')) }}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="{{ url('/setting/usermanagement/detail/' . $session->get('uuid')) }}" class="d-block">{{ $session->get('fullname') }}</a>
                        <sup style="color: #fff"><b>Butuh bantuan?</b> <a <?php echo $session->get('phone') == null ? 'href="#" data-toggle="tooltip" title="Masukkan nomor handphone terlebih dahulu !"' : 'href="https://api.whatsapp.com/send?phone=+6282121153626&text="' ?>>Chat via WA</a></sup></a>
                    </div>
                </div>
            </ul>
        </nav>

        <!-- SidebarSearch Form -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">MENU</li>
                <li class="nav-item">
                    <a href="{{ url('/dashboard') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Beranda
                            <!-- <span class="right badge badge-danger">New</span> -->
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    @if($session->get('roleid') == 1 || $session->get('roleid') == 3)
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-shield-alt"></i>
                        <p>
                            Kepolisian
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    @endif
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/kepolisian/geledah') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Geledah</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/kepolisian/sita') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Sita</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>
                                    Penahanan
                                    <i class="fas fa-angle-left right"></i>
                                    <!-- <span class="badge badge-info right">6</span> -->
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ url('/kepolisian/penahanan/dewasa') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Dewasa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/kepolisian/penahanan/anak') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Anak</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/kepolisian/tilang') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Tilang</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/kepolisian/tipiring') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>BAPC Tipiring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/kepolisian/diversi') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Diversi</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    @if($session->get('roleid') == 1 || $session->get('roleid') == 4)
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-balance-scale-right"></i>
                        <p>
                            Kejaksaan
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    @endif
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/kejaksaan/sita') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Sita</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>
                                    Penahanan
                                    <i class="fas fa-angle-left right"></i>
                                    <!-- <span class="badge badge-info right">6</span> -->
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ url('/kejaksaan/penahanan/dewasa') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Dewasa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/kejaksaan/penahanan/anak') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Anak</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/kejaksaan/diversi') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Diversi</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    @if($session->get('roleid') == 1 || $session->get('roleid') == 2)
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Pengadilan
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    @endif
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/geledah') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Geledah</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/sita') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Sita</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/izin_besuk') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Izin Besuk</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>
                                    Penahanan
                                    <i class="fas fa-angle-left right"></i>
                                    <!-- <span class="badge badge-info right">6</span> -->
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ url('/pengadilan_negeri/penahanan/dewasa') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Dewasa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/pengadilan_negeri/penahanan/anak') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Anak</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/tilang') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Tilang</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/tipiring') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>BAPC Tipiring</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/pengadilan_negeri/diversi') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Diversi</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    @if($session->get('roleid') == 1 || $session->get('roleid') == 5)
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-landmark"></i>
                        <p>
                            Lapas
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    @endif
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/lapas/izin_besuk') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Izin Besuk</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>
                                    Penahanan
                                    <i class="fas fa-angle-left right"></i>
                                    <!-- <span class="badge badge-info right">6</span> -->
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ url('/lapas/penahanan/dewasa') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Dewasa</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('/lapas/penahanan/anak') }}" class="nav-link">
                                        <i class="fa fa-dot-circle nav-icon" style="font-size: 10px"></i>
                                        <p>Penahanan Anak</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/inbox') }}" class="nav-link">
                        <i class="nav-icon fas fa-sms"></i>
                        <p>
                            Register Pesan
                            <!-- <span class="right badge badge-danger">New</span> -->
                        </p>
                    </a>
                </li>
                @if($session->get('roleid') == 1)
                <li class="nav-header">ADMINISTRATOR</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            SPPT Administrator
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('setting/usermanagement') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Manajemen User</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Setting
                            <i class="fas fa-angle-left right"></i>
                            <!-- <span class="badge badge-info right">6</span> -->
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('setting/website') }}" class="nav-link">
                                <i class="far fa-circle nav-icon" style="font-size: 10px"></i>
                                <p>Website</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                <li class="nav-header"><a href="{{ url('/logout') }}">LOGOUT</a></li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>