@extends('admin.layouts.master')

@section('title', 'Penahanan Dewasa')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Penahanan Dewasa</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">
                            {{
                                $menu == 'kepolisian' ?
                                    'Kepolisian' 
                                    :
                                    ($menu == 'kejaksaan' ?
                                        'Kejaksaan' 
                                        : 
                                        ($menu == 'pengadilan_negeri' ? 
                                        'Pengadilan Negeri'
                                        :
                                        'Lapas')
                                    )
                            }}
                        </li>
                        <li class="breadcrumb-item active"><a href="{{ url('/' . $menu . '/penahanan/dewasa') }}">Penahanan Dewasa</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="callout callout-danger row">
                        <div class="col-9">
                            <h5><i class="fas fa-info" style="font-size: 14px; padding: 0 10px 0 0;"></i> Status Penahanan:</h5>
                            Proses Penahanan tersangka atas nama: <b>{{ $penahanan->namatersangka }}</b>
                            {{
                                $penahanan->statuspenahanan == 'proses' ? 
                                    'sedang dalam proses' 
                                    : 
                                    ($penahanan->statuspenahanan == 'terima' ? 
                                        'diterima' 
                                        : 
                                        'ditolak') 
                            }}
                        </div>
                        @if($session->get('roleid') == 1 || $session->get('roleid') == 2)
                        <div class="col-3 p-0" style="float: right;">
                            <form action="{{ url($menu . '/penahanan/dewasa/update_status/' . $penahanan->uuid) }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>STATUS PENAHANAN*</label>
                                    <select class="form-control" name="status_penahanan">
                                        <option disabled>Pilih ...</option>
                                        <?php foreach ($statuspenahanan as $key => $value) { ?>
                                            <option <?php echo $penahanan->statuspenahanan == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                @error('status_penahanan')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <button type="submit" class="btn btn-primary" style=""><i class="fa fa-save"></i> Ubah Status</button>
                            </form>
                        </div>
                        @endif
                    </div>

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Informasi</h3>

                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <ul class="nav nav-pills flex-column">
                                            <li class="nav-item active">
                                                <a href="#detail" data-toggle="tab" class="nav-link">
                                                    <i class="fas fa-info-circle"></i> Detail Penahanan
                                                    <span class="badge bg-primary float-right"></span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#berkas" data-toggle="tab" class="nav-link">
                                                    <i class="far fa-file-pdf"></i> Berkas
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-9">
                                <div class="card card-danger card-outline">
                                    <div class="card-body p-0">
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="detail">
                                                <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/penahanan/dewasa/update/' . $penahanan->uuid) }}" method="post">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Penahanan ID" name="penahananid" value="{{ $penahanan->penahananid }}">

                                                            <div class="col-sm-12 row" style="padding: 0 20px">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>TANGGAL REGISTER*</label>

                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                            </div>
                                                                            <input value="{{ $penahanan->registerdate }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_register">
                                                                        </div>
                                                                        <!-- /.input group -->
                                                                    </div>
                                                                    @error('tanggal_register')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>JENIS PERMOHONAN PENAHANAN*</label>
                                                                                <select class="form-control" name="jenis_penahanan">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($jenispermohonan as $key => $value) { ?>
                                                                                        <option <?php echo $penahanan->jenispermohonan == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('jenis_penahanan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>SATUAN KERJA*</label>
                                                                                <select class="form-control" name="satuan_kerja">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($satuan as $satuan) { ?>
                                                                                        <option <?php echo $penahanan->satuanid == $satuan->satuanid ? 'selected="selected"' : '' ?> value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('satuan_kerja')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TANGGAL SURAT PENGAJUAN*</label>

                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $penahanan->tanggalpengajuan }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_pengajaun">
                                                                                </div>
                                                                                <!-- /.input group -->
                                                                            </div>
                                                                            @error('tanggal_pengajaun')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> NO SURAT PENGAJUAN*</label>
                                                                                <input value="{{ $penahanan->nosuratpengajuan }}" type="text" class="form-control" placeholder="No Surat Pengajuan" name="no_surat_pengajuan">
                                                                            </div>
                                                                            @error('no_surat_pengajuan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>WAKTU PENAHANAN HABIS*</label>

                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $penahanan->waktupenahanan }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="waktu_penahanan">
                                                                                </div>
                                                                                <!-- /.input group -->
                                                                            </div>
                                                                            @error('waktu_penahanan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>JENIS TEMPAT TAHANAN*</label>
                                                                                <select class="form-control" name="jenis_tempat_tahanan">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($jenistahanan as $key => $value) { ?>
                                                                                        <option <?php echo $penahanan->jenistempattahanan == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('jenis_tempat_tahanan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> TINDAK PIDANA YANG DILAKUKAN TERSANGKA*</label>
                                                                        <input value="{{ $penahanan->tindakpidana }}" type="text" class="form-control" placeholder="Tindak Pidana Yang Dilakukan Tersangka" name="tindak_pidana">
                                                                    </div>
                                                                    @error('tindak_pidana')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NO SURAT PERINTAH PENAHANAN*</label>
                                                                        <input value="{{ $penahanan->nosuratpenahanan }}" type="text" class="form-control" placeholder="No Surat Perintah Penahanan" name="no_surat_penahanan">
                                                                    </div>
                                                                    @error('no_surat_penahanan')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NO SURAT PERPANJANGAN KEJAKSAAN NEGERI*</label>
                                                                        <input value="{{ $penahanan->nosuratkejaksaan }}" type="text" class="form-control" placeholder="No Surat Perpanjangan Kejaksaan Negeri" name="no_surat_perpanjangan">
                                                                    </div>
                                                                    @error('no_surat_perpanjangan')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> NAMA TERSANGKA*</label>
                                                                        <input value="{{ $penahanan->namatersangka }}" type="text" class="form-control" placeholder="Nama Tersangka" name="nama_tersangka">
                                                                    </div>
                                                                    @error('nama_tersangka')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> TEMPAT LAHIR*</label>
                                                                                <input value="{{ $penahanan->tempatlahir }}" type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir">
                                                                            </div>
                                                                            @error('tempat_lahir')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>TANGGAL LAHIR*</label>

                                                                                <div class="input-group">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                                                    </div>
                                                                                    <input value="{{ $penahanan->tanggallahir }}" type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_lahir">
                                                                                </div>
                                                                                <!-- /.input group -->
                                                                            </div>
                                                                            @error('tanggal_lahir')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label><i class="far fa-edit"></i> TEMPAT TINGGAL*</label>
                                                                        <input value="{{ $penahanan->tempattinggal }}" type="text" class="form-control" placeholder="Tempat Tinggal" name="tempat_tinggal">
                                                                    </div>
                                                                    @error('tempat_tinggal')
                                                                    <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                    @enderror

                                                                    <div class="form-group row">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>JENIS KELAMIN*</label>
                                                                                <select class="form-control" name="jenis_kelamin">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($gender as $key => $value) { ?>
                                                                                        <option <?php echo $penahanan->jeniskelamin == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('jenis_kelamin')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label>AGAMA*</label>
                                                                                <select class="form-control" name="agama">
                                                                                    <option>Pilih ...</option>
                                                                                    <?php foreach ($religion as $key => $value) { ?>
                                                                                        <option <?php echo $penahanan->agama == $key ? 'selected="selected"' : '' ?> value="{{ $key }}">{{ $value }}</option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </div>
                                                                            @error('agama')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row" style="margin-bottom: 0;">
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> KEBANGSAAN*</label>
                                                                                <input value="{{ $penahanan->kebangsaan }}" type="text" class="form-control" placeholder="Kebangsaan" name="kebangsaan">
                                                                            </div>
                                                                            @error('kebangsaan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group">
                                                                                <label><i class="far fa-edit"></i> PEKERJAAN*</label>
                                                                                <input value="{{ $penahanan->pekerjaan }}" type="text" class="form-control" placeholder="Pekerjaan" name="pekerjaan">
                                                                            </div>
                                                                            @error('pekerjaan')
                                                                            <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/' . $menu . '/penahanan/dewasa') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            @if($session->get('roleid') != 5)
                                                            <div class="col-6">
                                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->

                                            <div class="tab-pane" id="berkas">
                                                <div class="card-body table-responsive" style="height: 790px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                    <form action="{{ url($menu . '/penahanan/dewasa/reupload_file/' . $penahanan->uuid) }}" method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="col-12 p-0 row">
                                                            <input type="hidden" class="form-control" placeholder="Penahanan ID" name="penahananid" value="{{ $penahanan->penahananid }}">

                                                            <div class="card-body table-responsive" style="height: 600px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                                                                <iframe src="{{ asset('berkas/penahanan/dewasa/' . $penahanan->berkas . '#toolbar=0&scrollbar=0') }}" frameBorder="0" scrolling="auto" height="100%" width="100%"></iframe>
                                                            </div>

                                                            <div class="col-12 row" style="padding: 0 20px">
                                                                <div class="form-group">
                                                                    <label for="exampleInputFile">BERKAS* (PDF)</label>
                                                                    <div class="input-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                                                        </div>
                                                                    </div>
                                                                    @error('berkas')
                                                                    <div style="margin-top: 15px;"></div>
                                                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 row">
                                                            <div class="col-6">
                                                                <a href="{{ url('/kepolisian/penahanan/dewasa') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Kembali</a>
                                                            </div>
                                                            <div class="col-6">
                                                                @if($session->get('roleid') != 5)
                                                                <button type="submit" class="btn btn-primary" style="float: right; margin-left: 5px"><i class="fa fa-save"></i> Ubah Berkas</button>
                                                                @endif
                                                                <a href="{{ url('/kepolisian/penahanan/dewasa/download/' . $penahanan->uuid) }}" rel="noopener" target="_blank" class="btn btn-success float-right"><i class="fas fa-print"></i>
                                                                    Berkas Elektronik
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection