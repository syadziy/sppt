@extends('admin.layouts.master')

@section('title', 'Penahanan Dewasa')

@section('content')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
function hari_indo($hari)
{
    switch ($hari) {
        case "Monday":
            $hari = "Senin";
            break;
        case "Tuesday":
            $hari = "Selasa";
            break;
        case "Wednesday":
            $hari = "Rabu";
            break;
        case "Thursday":
            $hari = "Kamis";
            break;
        case "Friday":
            $hari = "Jum'at";
            break;
        case "Saturday":
            $hari = "Sabtu";
            break;
        case "Sunday":
            $hari = "Minggu";
            break;
    }

    return $hari;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        {{
                            $menu == 'kepolisian' ?
                                'Kepolisian' 
                                :
                                ($menu == 'kejaksaan' ?
                                    'Kejaksaan' 
                                    : 
                                    ($menu == 'pengadilan_negeri' ? 
                                    'Pengadilan Negeri'
                                    :
                                    'Lapas')
                                )
                        }}
                        - Penahanan Dewasa
                    </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">
                            {{
                                $menu == 'kepolisian' ?
                                    'Kepolisian' 
                                    :
                                    ($menu == 'kejaksaan' ?
                                        'Kejaksaan' 
                                        : 
                                        ($menu == 'pengadilan_negeri' ? 
                                        'Pengadilan Negeri'
                                        :
                                        'Lapas')
                                    )
                            }}
                        </li>
                        <li class="breadcrumb-item active">Penahanan Dewasa</li>
                    </ol>
                </div>
            </div>
            @if($session->get('roleid') != 5)
            <div class="row">
                <div class="col-sm-10">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Tambah</button>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-success btn-block"><i class="fa fa-download"></i> Unduh</button>
                </div>
            </div>
            @endif
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Penahanan Dewasa</h3>

                            <!-- <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="card-tools" style="padding: 0 20px">
                                <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                            <table class="table table-head-fixed text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Tanggal Register</th>
                                        <th>Tersangka</th>
                                        <th>No. Surat Permohonan</th>
                                        <th>Pengaju</th>
                                        <th>Status</th>
                                        <th style="width: 50px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($penahanan as $p) { ?>
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                {{
                                                    hari_indo(date( "l", strtotime($p->registerdate))) 
                                                    . ', ' . 
                                                    tgl_indo(date( "Y-m-d", strtotime($p->registerdate)))
                                                }}
                                            </td>
                                            <td>{{ $p->namatersangka }}</td>
                                            <td>{{ $p->nosuratpengajuan }}</td>
                                            <td>{{ $p->satuan->namasatuan }}</td>
                                            <td>
                                                <div>
                                                    @if($p->statuspenahanan == 'pending')
                                                    <a class="btn bg-secondary" data-toggle="tooltip" title="proses..">
                                                        <i class="fa fa-spinner"></i>
                                                    </a>
                                                    @elseif($p->statuspenahanan == 'terima')
                                                    <a class="btn bg-success" data-toggle="tooltip" title="Diterima !">
                                                        <i class="fa fa-vote-yea"></i>
                                                    </a>
                                                    @elseif($p->statuspenahanan == 'tolak')
                                                    <a class="btn bg-danger" data-toggle="tooltip" title="Ditolak !">
                                                        <i class="fa fa-window-close"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <a class="btn bg-warning" href="{{ url('/' . $menu . '/penahanan/dewasa/detail/' . $p->uuid) }}">
                                                        <i class="fas fa-search"></i>
                                                    </a>
                                                    @if($session->get('roleid') != 5)
                                                    <a class="btn bg-danger" data-toggle="modal" data-target="#modal-sm" onclick="penahananDelete('{{ $p->uuid }}', '{{ $p->namatersangka }}')">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <br>
                            <p style="padding: 0 5px; margin: 0">Halaman : {{ $penahanan->currentPage() }}</p>
                            <p style="padding: 0 5px; margin: 0">Jumlah Data : {{ $penahanan->total() }}</p>
                            <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $penahanan->perPage() }}</p>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $penahanan->links() }}
                            </ul>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- /.modal -->
<div class="modal fade <?php echo count($errors) > 0 ? 'show' : '' ?>" id="modal-lg" style="display: <?php echo count($errors) > 0 ? 'block' : 'none' ?>">
    <div class="modal-dialog modal-xl" style="max-width: 90%;">
        <form action="{{ url($menu . '/penahanan/dewasa/store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Penahanan Dewasa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12 row" style="padding: 0 20px">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>TANGGAL REGISTER*</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_register">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                @error('tanggal_register')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group row" style="margin-bottom: 0;">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>JENIS PERMOHONAN PENAHANAN*</label>
                                            <select class="form-control" name="jenis_penahanan">
                                                <option>Pilih ...</option>
                                                <?php foreach ($jenispermohonan as $key => $value) { ?>
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        @error('jenis_penahanan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>SATUAN KERJA*</label>
                                            <select class="form-control" name="satuan_kerja">
                                                <option>Pilih ...</option>
                                                <?php foreach ($satuan as $satuan) { ?>
                                                    <option value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        @error('satuan_kerja')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row" style="margin-bottom: 0;">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>TANGGAL SURAT PENGAJUAN*</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_pengajaun">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        @error('tanggal_pengajaun')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> NO SURAT PENGAJUAN*</label>
                                            <input type="text" class="form-control" placeholder="No Surat Pengajuan" name="no_surat_pengajuan">
                                        </div>
                                        @error('no_surat_pengajuan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>WAKTU PENAHANAN HABIS*</label>

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="waktu_penahanan">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        @error('waktu_penahanan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>JENIS TEMPAT TAHANAN*</label>
                                            <select class="form-control" name="jenis_tempat_tahanan">
                                                <option>Pilih ...</option>
                                                <?php foreach ($jenistahanan as $key => $value) { ?>
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        @error('jenis_tempat_tahanan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><i class="far fa-edit"></i> TINDAK PIDANA YANG DILAKUKAN TERSANGKA*</label>
                                    <input type="text" class="form-control" placeholder="Tindak Pidana Yang Dilakukan Tersangka" name="tindak_pidana">
                                </div>
                                @error('tindak_pidana')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> NO SURAT PERINTAH PENAHANAN*</label>
                                            <input type="text" class="form-control" placeholder="No Surat Perintah Penahanan" name="no_surat_penahanan">
                                        </div>
                                        @error('no_surat_penahanan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> NO SURAT PERPANJANGAN KEJAKSAAN NEGERI*</label>
                                            <input type="text" class="form-control" placeholder="No Surat Perpanjangan Kejaksaan Negeri" name="no_surat_perpanjangan">
                                        </div>
                                        @error('no_surat_perpanjangan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row" style="margin-bottom: 0;">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> NAMA TERSANGKA*</label>
                                            <input type="text" class="form-control" placeholder="Nama Tersangka" name="nama_tersangka">
                                        </div>
                                        @error('nama_tersangka')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> TEMPAT LAHIR*</label>
                                            <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir">
                                        </div>
                                        @error('tempat_lahir')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><i class="far fa-edit"></i> TEMPAT TINGGAL*</label>
                                    <input type="text" class="form-control" placeholder="Tempat Tinggal" name="tempat_tinggal">
                                </div>
                                @error('tempat_tinggal')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group">
                                    <label>TANGGAL LAHIR*</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_lahir">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                @error('tanggal_lahir')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>JENIS KELAMIN*</label>
                                            <select class="form-control" name="jenis_kelamin">
                                                <option>Pilih ...</option>
                                                <?php foreach ($gender as $key => $value) { ?>
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        @error('jenis_kelamin')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>AGAMA*</label>
                                            <select class="form-control" name="agama">
                                                <option>Pilih ...</option>
                                                <?php foreach ($religion as $key => $value) { ?>
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        @error('agama')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row" style="margin-bottom: 0;">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> KEBANGSAAN*</label>
                                            <input type="text" class="form-control" placeholder="Kebangsaan" name="kebangsaan">
                                        </div>
                                        @error('kebangsaan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><i class="far fa-edit"></i> PEKERJAAN*</label>
                                            <input type="text" class="form-control" placeholder="Pekerjaan" name="pekerjaan">
                                        </div>
                                        @error('pekerjaan')
                                        <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputFile">BERKAS* (PDF)</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                        </div>
                                    </div>
                                    @error('berkas')
                                    <div style="margin-top: 15px;"></div>
                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <form action="{{ url($menu . '/penahanan/dewasa/delete') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Penahanan Dewasa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12" style="padding: 0 20px">
                            <div class="form-group">
                                <p class="text-center">Apakah anda yakin ingin menghapus data tersangka atas nama</p>
                                <b>
                                    <p id="tersangkaDeleted" class="text-center" style="font-size: 14px;"></p>
                                </b>
                                <input type="hidden" id="uuidDeleted" name="uuid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection