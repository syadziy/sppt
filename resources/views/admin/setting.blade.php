@extends('admin.layouts.master')

@section('title', 'Manajemen User')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Setting Website</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Setting</li>
                        <li class="breadcrumb-item active">Website</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" style="width: 100px; height: 100px" src="{{ $setting == null ? asset('lte/dist/img/AdminLTELogo.png') : ($setting->logo == null ? asset('lte/dist/img/AdminLTELogo.png') : asset('logo/' . $setting->logo)) }}" alt="User profile picture">
                            </div>

                            <br>
                            <h3 class="profile-username text-center">{{ $setting == null ? 'SISTEM PERADILAN PIDANA TERPADU' : $setting->websitename }}</h3>

                            <p class="text-muted text-center">REGIONAL {{ $setting == null ? 'WAIKABUBAK' : $setting->regional }}</p>

                            <form action="{{ url('setting/website/upload_foto') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="photo" id="fileToUpload">
                                    </div>
                                </div>
                                @error('photo')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                <br>
                                @enderror
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Ganti Foto</button>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                            <form action="{{ url('setting/website/update') }}" method="post">
                                    @csrf
                                    <div class="active tab-pane" id="activity">
                                        <div class="col-12 p-0 row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> NAMA WEBSITE*</label>
                                                    <input type="text" class="form-control" placeholder="Nama Website" name="nama_website" value="{{ $setting == null ? '' : $setting->websitename }}">
                                                </div>
                                                @error('nama_website')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>
                                            
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> REGIONAL*</label>
                                                    <input type="text" class="form-control" placeholder="Regional" name="regional" value="{{ $setting == null ? '' : $setting->regional }}">
                                                </div>
                                                @error('regional')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-12 row">
                                            <div class="col-6">
                                                <a href="{{ url('/dashboard') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Batal</a>
                                            </div>
                                            <div class="col-6">
                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection