<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Izin Mengunjungi Tahanan</title>
</head>

<body>
    <?php
    function tgl_indo($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }
    function hari_indo($hari)
    {
        switch ($hari) {
            case "Monday":
                $hari = "Senin";
                break;
            case "Tuesday":
                $hari = "Selasa";
                break;
            case "Wednesday":
                $hari = "Rabu";
                break;
            case "Thursday":
                $hari = "Kamis";
                break;
            case "Friday":
                $hari = "Jum'at";
                break;
            case "Saturday":
                $hari = "Sabtu";
                break;
            case "Sunday":
                $hari = "Minggu";
                break;
        }

        return $hari;
    }
    ?>

        <div class="row">   
            <div class="col-1">
                <img class="profile-user-img img-fluid img-circle" style="width: 90px; height: 90px; padding: 20px" src="../public/logo/logo1pn.jpg" alt="">
            </div>
            <div class="col-10" style="margin-top: -110px;">
                <center>
                    <h4 style="font-size: 14px; padding: 2px; margin: 2px">PENGADILAN NEGERI WAIKABUBAK KELAS II</h4>
                    <p style="font-size: 12px; padding: 2px; margin: 2px">JALAN SUDIRMAN NO.10 WAIKABUBAK</p>
                    <p style="font-size: 12px; padding: 2px; margin: 2px">TELEPON : (0387) 22075, 22191, 21593 FAX : 0387 - 2l054</p>
                    <p style="font-size: 12px; padding: 2px; margin: 2px">WEBSITE : <a href="#">www.pn-waikabubak.go.id</a>, E-MAIL : <a href="#">pn_wkb@yahoo.co.id</a></p>
                    <p style="font-size: 12px; padding: 2px; margin: 2px"><b>WAIKABUBAK 87211</b></p>
                </center>
            </div>
            <div class="col-1"></div>
        </div>

        <br>
        <hr>

        <div>
            <center>
                <p style="font-size: 12px; padding: 2px; margin: 2px; text-decoration: underline;">Surat Ijin Mengunjungi Tahanan</p>
                <p style="font-size: 12px; padding: 2px; margin: 2px; text-decoration: underline;">Nomor Perkara : <?php echo $izinbesuk->nomorperkara ?></p>
                <p style="font-size: 12px; padding: 2px; margin: 2px; text-decoration: underline;">Atas perkara terdakwa : <?php echo $izinbesuk->izinbesukid ?></p>
            </center>
        </div>

        <br><br><br><br>

        <div class="col-12" style="padding: 0 20px">
            <table style="width: 100%; font-size: 13px;">
                <tbody>
                    <tr>
                        <td style="width: 20%;">Nama</td>
                        <td>: <?php echo $izinbesuk->namapemohon ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Tempat Tinggal</td>
                        <td>: <?php echo $izinbesuk->alamatpemohon ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Maksud Kunjungan</td>
                        <td>: Mengunjungi dan membawa makanan untuk Para Terdakwa</td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Hubungan Keluarga</td>
                        <td>: <?php echo $izinbesuk->hubungankeluarga ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Pengikut Besuk</td>
                        <td>: <?php echo $izinbesuk->pengikutbesuk ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Yang Dikunjungi</td>
                        <td>: <?php echo $izinbesuk->hubungankeluarga ?></td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Berlaku Sampi Tanggal</td>
                        <td>:
                            <?php echo
                            hari_indo(date("l", strtotime($izinbesuk->tanggalberlaku)))
                                . ', ' .
                                tgl_indo(date("Y-m-d", strtotime($izinbesuk->tanggalberlaku)))
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>

            <br><br><br><br>

            <table style="width: 30%; font-size: 13px; float: right">
                <tbody>
                    <tr>
                        <td>
                            <center>
                                Waikabubak,
                                <br>
                                <?php echo
                                hari_indo(date("l", strtotime($today)))
                                    . ', ' .
                                    tgl_indo(date("Y-m-d", strtotime($today)))
                                ?>
                                <br><br><br><br><br><br><br><br>
                                <?php echo $izinbesuk->hakim ?>
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</body>

</html>