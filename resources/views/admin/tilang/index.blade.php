@extends('admin.layouts.master')

@section('title', 'Tilang')

@section('content')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
function hari_indo($hari)
{
    switch ($hari) {
        case "Monday":
            $hari = "Senin";
            break;
        case "Tuesday":
            $hari = "Selasa";
            break;
        case "Wednesday":
            $hari = "Rabu";
            break;
        case "Thursday":
            $hari = "Kamis";
            break;
        case "Friday":
            $hari = "Jum'at";
            break;
        case "Saturday":
            $hari = "Sabtu";
            break;
        case "Sunday":
            $hari = "Minggu";
            break;
    }

    return $hari;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $menu == 'kepolisian' ? 'Kepolisian' : 'Pengadilan Negeri' }} - Tilang</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $menu == 'kepolisian' ? 'Kepolisian' : 'Pengadilan Negeri' }}</li>
                        <li class="breadcrumb-item active">Tilang</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10">
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-lg"><i class="fa fa-plus"></i> Tambah</button>
                </div>
                <div class="col-sm-1">
                    <button type="button" class="btn btn-success btn-block"><i class="fa fa-download"></i> Unduh</button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Tilang</h3>

                            <!-- <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="card-tools" style="padding: 0 20px">
                                <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="widgets.html" data-source-selector="#card-refresh-content">
                                    <i class="fas fa-sync-alt"></i>
                                </button>
                            </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive" style="min-height: 300px; margin: 20px 0 20px 0; padding: 0 20px" id="card-refresh-content">
                            <table class="table table-head-fixed text-nowrap table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Tanggal Register</th>
                                        <th>Tanggal Penindakan</th>
                                        <th>Penindak</th>
                                        <th>Tanggal Sidang</th>
                                        <th style="width: 50px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; ?>
                                    <?php foreach ($tilang as $t) { ?>
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                {{
                                                    hari_indo(date( "l", strtotime($t->registerdate))) 
                                                    . ', ' . 
                                                    tgl_indo(date( "Y-m-d", strtotime($t->registerdate)))
                                                }}
                                            </td>
                                            <td>
                                                {{
                                                    hari_indo(date( "l", strtotime($t->tanggalpenindakan))) 
                                                    . ', ' . 
                                                    tgl_indo(date( "Y-m-d", strtotime($t->tanggalpenindakan)))
                                                }}
                                            </td>
                                            <td>{{ $t->satuan->namasatuan }}</td>
                                            <td>
                                                {{
                                                    hari_indo(date( "l", strtotime($t->tanggalsidang))) 
                                                    . ', ' . 
                                                    tgl_indo(date( "Y-m-d", strtotime($t->tanggalsidang)))
                                                }}
                                            </td>
                                            <td>
                                                <div>
                                                    <a class="btn bg-warning" href="{{ url('/' . $menu . '/tilang/detail/' . $t->uuid) }}">
                                                        <i class="fas fa-search"></i>
                                                    </a>
                                                    <a class="btn bg-danger" data-toggle="modal" data-target="#modal-sm" onclick="tilangDelete('{{ $t->uuid }}', '{{ $t->satuan->namasatuan }}')">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <br>
                            <p style="padding: 0 5px; margin: 0">Halaman : {{ $tilang->currentPage() }}</p>
                            <p style="padding: 0 5px; margin: 0">Jumlah Data : {{ $tilang->total() }}</p>
                            <p style="padding: 0 5px; margin: 0">Data Per Halaman : {{ $tilang->perPage() }}</p>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $tilang->links() }}
                            </ul>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- /.modal -->
<div class="modal fade <?php echo count($errors) > 0 ? 'show' : '' ?>" id="modal-lg" style="display: <?php echo count($errors) > 0 ? 'block' : 'none' ?>">
    <div class="modal-dialog modal-lg">
        <form action="{{ url($menu . '/tilang/store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Tilang</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12 row" style="padding: 0 20px">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>TANGGAL REGISTER*</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_register">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                @error('tanggal_register')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group">
                                    <label>PENINDAK*</label>
                                    <select class="form-control" name="satuan_kerja">
                                        <option>Pilih ...</option>
                                        <?php foreach ($satuan as $satuan) { ?>
                                            <option value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                @error('satuan_kerja')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>TANGGAL PENINDAKAN*</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_penindakan">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                @error('tanggal_penindakan')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group">
                                    <label>TANGGAL SIDANG*</label>

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask name="tanggal_sidang">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                @error('tanggal_sidang')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                @enderror

                                <div class="form-group">
                                    <label for="exampleInputFile">BERKAS* (XLS/XLSX)</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="berkas">
                                            <label class="custom-file-label" for="exampleInputFile">Pilih Berkas</label>
                                        </div>
                                    </div>
                                    @error('berkas')
                                    <div style="margin-top: 15px;"></div>
                                    <sup style="padding: 10px; color: red">{{ $message }}</sup>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="<?php echo count($errors) > 0 ? 'reload()' : '' ?>"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade" id="modal-sm">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <form action="{{ url($menu . '/tilang/delete') }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hapus Tilang</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row col-sm-12">
                        <div class="col-sm-12" style="padding: 0 20px">
                            <div class="form-group">
                                <p class="text-center">Apakah anda yakin ingin menghapus data tilang atas penindak</p>
                                <b>
                                    <p id="penindakDeleted" class="text-center" style="font-size: 14px;"></p>
                                </b>
                                <input type="hidden" id="uuidDeleted" name="uuid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection