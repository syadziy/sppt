@extends('admin.layouts.master')

@section('title', 'Manajemen User')

@section('content')
<?php
function tgl_indo($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}
function hari_indo($hari)
{
    switch ($hari) {
        case "Monday":
            $hari = "Senin";
            break;
        case "Tuesday":
            $hari = "Selasa";
            break;
        case "Wednesday":
            $hari = "Rabu";
            break;
        case "Thursday":
            $hari = "Kamis";
            break;
        case "Friday":
            $hari = "Jum'at";
            break;
        case "Saturday":
            $hari = "Sabtu";
            break;
        case "Sunday":
            $hari = "Minggu";
            break;
    }

    return $hari;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Petugas</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Setting</li>
                        <li class="breadcrumb-item active"><a href="{{ url('/setting/usermanagement') }}">Manajemen User</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" style="width: 100px; height: 100px" src="{{ $user->pathphoto == null ? asset('lte/dist/img/user1-128x128.jpg') : asset('img/' . $user->pathphoto) }}" alt="User profile picture">
                            </div>

                            <br>
                            <h3 class="profile-username text-center">{{ $user->fullname }}</h3>

                            <p class="text-muted text-center">{{ $user->role->rolename . ($user->satuanid == null ? '' : ' - ' . $user->satuan->namasatuan) }}</p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Email</b> <a class="float-right">{{ $user->email }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Status</b> <a class="float-right">{{ $user->banned == 0 ? 'Aktif' : 'Tidak Aktif' }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Waktu Daftar</b> <a class="float-right">
                                        {{
                                            hari_indo(date( "l", strtotime($user->createdat))) 
                                            . ', ' . 
                                            tgl_indo(date( "Y-m-d", strtotime($user->createdat)))
                                        }}
                                    </a>
                                    <br>
                                    <a class="float-right">
                                        {{
                                            date( "H:i:s", strtotime($user->createdat)) 
                                            . ' WIB'
                                        }}
                                    </a>
                                </li>
                            </ul>

                            <form action="{{ url('setting/usermanagement/upload_foto/' . $user->uuid) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="photo" id="fileToUpload">
                                    </div>
                                </div>
                                @error('photo')
                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                <br>
                                @enderror
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Ganti Foto</button>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <form action="{{ url('setting/usermanagement/update/' . $user->uuid) }}" method="post">
                                    @csrf
                                    <div class="active tab-pane" id="activity">
                                        <div class="col-12 p-0 row">
                                            <input type="hidden" class="form-control" placeholder="User ID" name="userid" value="{{ $user->userid }}">

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>JENIS SATUAN KERJA*</label>
                                                    <select class="form-control" name="jenis_satuan" disabled="disabled">
                                                        <option disabled="disabled">Pilih ...</option>
                                                        @if($user->roleid != 1)
                                                        <?php foreach ($role as $role) { ?>
                                                            <option <?php echo $user->roleid == $role->roleid ? 'selected="selected"' : '' ?> value="{{ $role->roleid }}">{{ $role->rolename }}</option>
                                                        <?php } ?>
                                                        @else
                                                        <option>Superadmin</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                @error('jenis_satuan')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> EMAIL*</label>
                                                    <input type="text" class="form-control" placeholder="Email" name="email" value="{{ $user->email }}">
                                                </div>
                                                @error('email')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> NAMA SATUAN KERJA*</label>
                                                    <select class="form-control" name="nama_satuan" <?php echo $user->roleid != 1 ? '' : 'disabled="disabled"' ?>>
                                                        <option disabled="disabled">Pilih ...</option>
                                                        @if($user->roleid != 1)
                                                        <?php foreach ($satuan as $satuan) { ?>
                                                            <option <?php echo $user->satuanid == $satuan->satuanid ? 'selected="selected"' : '' ?> value="{{ $satuan->satuanid }}">{{ $satuan->namasatuan }}</option>
                                                        <?php } ?>
                                                        @else
                                                        <option>Superadmin</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                @error('nama_satuan')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> NAMA PETUGAS*</label>
                                                    <input type="text" class="form-control" placeholder="Nama Petugas" name="nama_petugas" value="{{ $user->fullname }}">
                                                </div>
                                                @error('nama_petugas')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label><i class="far fa-edit"></i> STATUS*</label>
                                                    <select class="form-control" name="banned">
                                                        <option disabled="disabled">Pilih ...</option>
                                                        <option <?php echo $user->banned == 0 ? 'selected="selected"' : '' ?> value="0">Aktif</option>
                                                        <option <?php echo $user->banned == 1 ? 'selected="selected"' : '' ?> value="1">Tidak Aktif</option>
                                                    </select>
                                                </div>
                                                @error('status')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="col-form-label"><i class="far fa-edit"></i> PASSWORD*</label>
                                                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ $user->password }}">
                                                </div>
                                                @error('password')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label class="col-form-label"><i class="far fa-edit"></i> NO HANDPHONE (OPTIONAL)</label>
                                                    <input type="text" class="form-control" placeholder="No Handphone" name="phone" value="{{ $user->phone }}">
                                                </div>
                                                @error('phone')
                                                <sup style="padding: 10px; color: red;">{{ $message }}</sup>
                                                @enderror
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="col-12 row">
                                            <div class="col-6">
                                                <a href="{{ url('/setting/usermanagement') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Batal</a>
                                            </div>
                                            <div class="col-6">
                                                <button type="submit" class="btn btn-primary" style="float: right"><i class="fa fa-save"></i> Ubah Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection