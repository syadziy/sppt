<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\GeledahController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\InboxController;
use App\Http\Controllers\Admin\SitaController;
use App\Http\Controllers\Admin\PenahananDewasaController;
use App\Http\Controllers\Admin\PenahananAnakController;
use App\Http\Controllers\Admin\TilangController;
use App\Http\Controllers\Admin\TipiringController;
use App\Http\Controllers\Admin\DiversiController;
use App\Http\Controllers\Admin\IzinBesukController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', [AuthController::class, 'login']);
Route::post('/login/validation', [AuthController::class, 'validation']);
Route::get('/reload-captcha', [AuthController::class, 'reloadCaptcha']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::group([
    'prefix' => '/'
], function () {
    Route::get('dashboard', [DashboardController::class, 'index']);

    // KEPOLISIAN
    Route::group([
        'prefix' => 'kepolisian'
    ], function () {
        // GELEDAH
        Route::group([
            'prefix' => 'geledah'
        ], function () {
            Route::get('', [GeledahController::class, 'index']);
            Route::post('/store', [GeledahController::class, 'store']);
            Route::post('/delete', [GeledahController::class, 'delete']);
            Route::get('/detail/{uuid}', [GeledahController::class, 'detail']);
            Route::post('/update/{uuid}', [GeledahController::class, 'update']);
            Route::post('/update_status/{uuid}', [GeledahController::class, 'updateStatus']);
            Route::post('/reupload_file/{uuid}', [GeledahController::class, 'reupload']);
            Route::get('/download/{uuid}', [GeledahController::class, 'download']);
        });

        // SITA
        Route::group([
            'prefix' => 'sita'
        ], function () {
            Route::get('', [SitaController::class, 'index']);
            Route::post('/store', [SitaController::class, 'store']);
            Route::post('/delete', [SitaController::class, 'delete']);
            Route::get('/detail/{uuid}', [SitaController::class, 'detail']);
            Route::post('/update/{uuid}', [SitaController::class, 'update']);
            Route::post('/update_status/{uuid}', [SitaController::class, 'updateStatus']);
            Route::post('/reupload_file/{uuid}', [SitaController::class, 'reupload']);
            Route::get('/download/{uuid}', [SitaController::class, 'download']);
        });

        // PENAHANAN
        Route::group([
            'prefix' => 'penahanan'
        ], function () {
            // DEWASA
            Route::group([
                'prefix' => 'dewasa'
            ], function () {
                Route::get('', [PenahananDewasaController::class, 'index']);
                Route::post('/store', [PenahananDewasaController::class, 'store']);
                Route::post('/delete', [PenahananDewasaController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananDewasaController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananDewasaController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananDewasaController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananDewasaController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananDewasaController::class, 'updateStatus']);
            });

            // ANAK
            Route::group([
                'prefix' => 'anak'
            ], function () {
                Route::get('', [PenahananAnakController::class, 'index']);
                Route::post('/store', [PenahananAnakController::class, 'store']);
                Route::post('/delete', [PenahananAnakController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananAnakController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananAnakController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananAnakController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananAnakController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananAnakController::class, 'updateStatus']);
            });
        });

        // TILANG
        Route::group([
            'prefix' => 'tilang'
        ], function () {
            Route::get('', [TilangController::class, 'index']);
            Route::post('/store', [TilangController::class, 'store']);
            Route::post('/delete', [TilangController::class, 'delete']);
            Route::get('/detail/{uuid}', [TilangController::class, 'detail']);
            Route::post('/update/{uuid}', [TilangController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [TilangController::class, 'reupload']);
            Route::get('/download/{uuid}', [TilangController::class, 'download']);
            Route::post('/update_status/{uuid}', [TilangController::class, 'updateStatus']);
        });

        // TIPIRING
        Route::group([
            'prefix' => 'tipiring'
        ], function () {
            Route::get('', [TipiringController::class, 'index']);
            Route::post('/store', [TipiringController::class, 'store']);
            Route::post('/delete', [TipiringController::class, 'delete']);
            Route::get('/detail/{uuid}', [TipiringController::class, 'detail']);
            Route::post('/update/{uuid}', [TipiringController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [TipiringController::class, 'reupload']);
            Route::get('/download/{uuid}', [TipiringController::class, 'download']);
            Route::post('/update_status/{uuid}', [TipiringController::class, 'updateStatus']);
        });

        // DIVERSI
        Route::group([
            'prefix' => 'diversi'
        ], function () {
            Route::get('', [DiversiController::class, 'index']);
            Route::post('/store', [DiversiController::class, 'store']);
            Route::post('/delete', [DiversiController::class, 'delete']);
            Route::get('/detail/{uuid}', [DiversiController::class, 'detail']);
            Route::post('/update/{uuid}', [DiversiController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [DiversiController::class, 'reupload']);
            Route::get('/download/{uuid}', [DiversiController::class, 'download']);
            Route::post('/update_status/{uuid}', [DiversiController::class, 'updateStatus']);
        });
    });

    // KEJAKSAAN
    Route::group([
        'prefix' => 'kejaksaan'
    ], function () {
        // PENAHANAN
        Route::group([
            'prefix' => 'penahanan'
        ], function () {
            // DEWASA
            Route::group([
                'prefix' => 'dewasa'
            ], function () {
                Route::get('', [PenahananDewasaController::class, 'index']);
                Route::post('/store', [PenahananDewasaController::class, 'store']);
                Route::post('/delete', [PenahananDewasaController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananDewasaController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananDewasaController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananDewasaController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananDewasaController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananDewasaController::class, 'updateStatus']);
            });

            // ANAK
            Route::group([
                'prefix' => 'anak'
            ], function () {
                Route::get('', [PenahananAnakController::class, 'index']);
                Route::post('/store', [PenahananAnakController::class, 'store']);
                Route::post('/delete', [PenahananAnakController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananAnakController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananAnakController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananAnakController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananAnakController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananAnakController::class, 'updateStatus']);
            });
        });

        // DIVERSI
        Route::group([
            'prefix' => 'diversi'
        ], function () {
            Route::get('', [DiversiController::class, 'index']);
            Route::post('/store', [DiversiController::class, 'store']);
            Route::post('/delete', [DiversiController::class, 'delete']);
            Route::get('/detail/{uuid}', [DiversiController::class, 'detail']);
            Route::post('/update/{uuid}', [DiversiController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [DiversiController::class, 'reupload']);
            Route::get('/download/{uuid}', [DiversiController::class, 'download']);
            Route::post('/update_status/{uuid}', [DiversiController::class, 'updateStatus']);
        });

        // SITA
        Route::group([
            'prefix' => 'sita'
        ], function () {
            Route::get('', [SitaController::class, 'index']);
            Route::post('/store', [SitaController::class, 'store']);
            Route::post('/delete', [SitaController::class, 'delete']);
            Route::get('/detail/{uuid}', [SitaController::class, 'detail']);
            Route::post('/update/{uuid}', [SitaController::class, 'update']);
            Route::post('/update_status/{uuid}', [SitaController::class, 'updateStatus']);
            Route::post('/reupload_file/{uuid}', [SitaController::class, 'reupload']);
            Route::get('/download/{uuid}', [SitaController::class, 'download']);
        });
    });

    // PENGADILAN NEGERI
    Route::group([
        'prefix' => 'pengadilan_negeri'
    ], function () {
        // GELEDAH
        Route::group([
            'prefix' => 'geledah'
        ], function () {
            Route::get('', [GeledahController::class, 'index']);
            Route::post('/store', [GeledahController::class, 'store']);
            Route::post('/delete', [GeledahController::class, 'delete']);
            Route::get('/detail/{uuid}', [GeledahController::class, 'detail']);
            Route::post('/update/{uuid}', [GeledahController::class, 'update']);
            Route::post('/update_status/{uuid}', [GeledahController::class, 'updateStatus']);
            Route::post('/reupload_file/{uuid}', [GeledahController::class, 'reupload']);
            Route::get('/download/{uuid}', [GeledahController::class, 'download']);
        });
        
        // PENAHANAN
        Route::group([
            'prefix' => 'penahanan'
        ], function () {
            // DEWASA
            Route::group([
                'prefix' => 'dewasa'
            ], function () {
                Route::get('', [PenahananDewasaController::class, 'index']);
                Route::post('/store', [PenahananDewasaController::class, 'store']);
                Route::post('/delete', [PenahananDewasaController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananDewasaController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananDewasaController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananDewasaController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananDewasaController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananDewasaController::class, 'updateStatus']);
            });

            // ANAK
            Route::group([
                'prefix' => 'anak'
            ], function () {
                Route::get('', [PenahananAnakController::class, 'index']);
                Route::post('/store', [PenahananAnakController::class, 'store']);
                Route::post('/delete', [PenahananAnakController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananAnakController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananAnakController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananAnakController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananAnakController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananAnakController::class, 'updateStatus']);
            });
        });

        // TILANG
        Route::group([
            'prefix' => 'tilang'
        ], function () {
            Route::get('', [TilangController::class, 'index']);
            Route::post('/store', [TilangController::class, 'store']);
            Route::post('/delete', [TilangController::class, 'delete']);
            Route::get('/detail/{uuid}', [TilangController::class, 'detail']);
            Route::post('/update/{uuid}', [TilangController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [TilangController::class, 'reupload']);
            Route::get('/download/{uuid}', [TilangController::class, 'download']);
            Route::post('/update_status/{uuid}', [TilangController::class, 'updateStatus']);
        });

        // TIPIRING
        Route::group([
            'prefix' => 'tipiring'
        ], function () {
            Route::get('', [TipiringController::class, 'index']);
            Route::post('/store', [TipiringController::class, 'store']);
            Route::post('/delete', [TipiringController::class, 'delete']);
            Route::get('/detail/{uuid}', [TipiringController::class, 'detail']);
            Route::post('/update/{uuid}', [TipiringController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [TipiringController::class, 'reupload']);
            Route::get('/download/{uuid}', [TipiringController::class, 'download']);
            Route::post('/update_status/{uuid}', [TipiringController::class, 'updateStatus']);
        });

        // DIVERSI
        Route::group([
            'prefix' => 'diversi'
        ], function () {
            Route::get('', [DiversiController::class, 'index']);
            Route::post('/store', [DiversiController::class, 'store']);
            Route::post('/delete', [DiversiController::class, 'delete']);
            Route::get('/detail/{uuid}', [DiversiController::class, 'detail']);
            Route::post('/update/{uuid}', [DiversiController::class, 'update']);
            Route::post('/reupload_file/{uuid}', [DiversiController::class, 'reupload']);
            Route::get('/download/{uuid}', [DiversiController::class, 'download']);
            Route::post('/update_status/{uuid}', [DiversiController::class, 'updateStatus']);
        });
        
        // IZIN BESUK
        Route::group([
            'prefix' => 'izin_besuk'
        ], function () {
            Route::get('', [IzinBesukController::class, 'index']);
            Route::post('/store', [IzinBesukController::class, 'store']);
            Route::post('/delete', [IzinBesukController::class, 'delete']);
            Route::get('/detail/{uuid}', [IzinBesukController::class, 'detail']);
            Route::post('/update/{uuid}', [IzinBesukController::class, 'update']);
            Route::post('/reupload_file_permohonan/{uuid}', [IzinBesukController::class, 'reuploadPermohonan']);
            Route::post('/reupload_file_izin/{uuid}', [IzinBesukController::class, 'reuploadIzin']);
            Route::get('/download_permohonan/{uuid}', [IzinBesukController::class, 'downloadPermohonan']);
            Route::get('/download_izin/{uuid}', [IzinBesukController::class, 'downloadIzin']);
            Route::get('/generate_surat/{uuid}', [IzinBesukController::class, 'generateSurat']);
        });

        // SITA
        Route::group([
            'prefix' => 'sita'
        ], function () {
            Route::get('', [SitaController::class, 'index']);
            Route::post('/store', [SitaController::class, 'store']);
            Route::post('/delete', [SitaController::class, 'delete']);
            Route::get('/detail/{uuid}', [SitaController::class, 'detail']);
            Route::post('/update/{uuid}', [SitaController::class, 'update']);
            Route::post('/update_status/{uuid}', [SitaController::class, 'updateStatus']);
            Route::post('/reupload_file/{uuid}', [SitaController::class, 'reupload']);
            Route::get('/download/{uuid}', [SitaController::class, 'download']);
        });
    });
    
    // LAPAS
    Route::group([
        'prefix' => 'lapas'
    ], function () {
        // PENAHANAN
        Route::group([
            'prefix' => 'penahanan'
        ], function () {
            // DEWASA
            Route::group([
                'prefix' => 'dewasa'
            ], function () {
                Route::get('', [PenahananDewasaController::class, 'index']);
                Route::post('/store', [PenahananDewasaController::class, 'store']);
                Route::post('/delete', [PenahananDewasaController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananDewasaController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananDewasaController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananDewasaController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananDewasaController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananDewasaController::class, 'updateStatus']);
            });

            // ANAK
            Route::group([
                'prefix' => 'anak'
            ], function () {
                Route::get('', [PenahananAnakController::class, 'index']);
                Route::post('/store', [PenahananAnakController::class, 'store']);
                Route::post('/delete', [PenahananAnakController::class, 'delete']);
                Route::get('/detail/{uuid}', [PenahananAnakController::class, 'detail']);
                Route::post('/update/{uuid}', [PenahananAnakController::class, 'update']);
                Route::post('/reupload_file/{uuid}', [PenahananAnakController::class, 'reupload']);
                Route::get('/download/{uuid}', [PenahananAnakController::class, 'download']);
                Route::post('/update_status/{uuid}', [PenahananAnakController::class, 'updateStatus']);
            });
        });
        
        // IZIN BESUK
        Route::group([
            'prefix' => 'izin_besuk'
        ], function () {
            Route::get('', [IzinBesukController::class, 'index']);
            Route::post('/store', [IzinBesukController::class, 'store']);
            Route::post('/delete', [IzinBesukController::class, 'delete']);
            Route::get('/detail/{uuid}', [IzinBesukController::class, 'detail']);
            Route::post('/update/{uuid}', [IzinBesukController::class, 'update']);
            Route::post('/reupload_file_permohonan/{uuid}', [IzinBesukController::class, 'reuploadPermohonan']);
            Route::post('/reupload_file_izin/{uuid}', [IzinBesukController::class, 'reuploadIzin']);
            Route::get('/download_permohonan/{uuid}', [IzinBesukController::class, 'downloadPermohonan']);
            Route::get('/download_izin/{uuid}', [IzinBesukController::class, 'downloadIzin']);
            Route::get('/generate_surat/{uuid}', [IzinBesukController::class, 'generateSurat']);
        });
    });

    // INBOX
    Route::group([
        'prefix' => 'inbox'
    ], function () {
        Route::get('', [InboxController::class, 'index']);
        Route::post('/send', [InboxController::class, 'send']);
        Route::post('/delete', [InboxController::class, 'delete']);
        Route::get('/read/{uuid}', [InboxController::class, 'read']);
    });

    // SETTING
    Route::group([
        'prefix' => 'setting'
    ], function () {
        // USERMANAGEMENT
        Route::group([
            'prefix' => 'usermanagement'
        ], function () {
            Route::get('', [UserController::class, 'index']);
            Route::post('/store', [UserController::class, 'store']);
            Route::post('/delete', [UserController::class, 'delete']);
            Route::get('/detail/{uuid}', [UserController::class, 'detail']);
            Route::post('/update/{uuid}', [UserController::class, 'update']);
            Route::post('/upload_foto/{uuid}', [UserController::class, 'uploadFoto']);
        });

        // SETTING WEBSITE
        Route::group([
            'prefix' => 'website'
        ], function () {
            Route::get('', [SettingController::class, 'index']);
            Route::post('/update', [SettingController::class, 'update']);
            Route::post('/upload_foto', [SettingController::class, 'uploadFoto']);
        });
    });
});
